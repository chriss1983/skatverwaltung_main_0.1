package testclasses;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class ActionAdapterJbxMonate implements ActionListener {

	MeinFenster meinFenster = new MeinFenster();

	public ActionAdapterJbxMonate(MeinFenster meinFenster) {
		this.meinFenster.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		// this will triggert when press the button
		this.meinFenster.setText("This is a compledly different text");
	}

	class MeinFenster extends JFrame {
		private static final long	serialVersionUID	= 1L;

		private JLabel				label;
		private JButton				btn;

		public MeinFenster() {
			this.setTitle("ActionListener Test");
			this.setSize(new Dimension(600, 800));
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			this.setLayout(new BorderLayout());
			btn = new JButton("Press to change the Label Text");

			label = new JLabel("This is a Label");

			this.add(label, BorderLayout.CENTER);
			this.add(btn, BorderLayout.NORTH);

			this.setVisible(true);
		}

		public void setText(String newText) {
			label.setText(newText);
		}

		public void addActionListener(ActionListener actionListener) {
			btn.addActionListener(actionListener);
		}
	}

	public static void main(String[] args) {
		new ActionAdapterJbxMonate(null);
	}
}