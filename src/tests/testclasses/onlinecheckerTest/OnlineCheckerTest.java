package testclasses.onlinecheckerTest;

import com.backend.utils.connection.OnlineChecker;
import com.backend.utils.connection.OnlineStateChecker;

public class OnlineCheckerTest implements OnlineChecker {
	OnlineStateChecker checker;

	public OnlineCheckerTest() {
		checker = new OnlineStateChecker();
		checker.addOnlineListener(this);
	}

	public static void main(String[] args) {
		new OnlineCheckerTest();
	}

	@Override
	public void onlineListener(String state) {
		System.out.println(state);
	}

}
