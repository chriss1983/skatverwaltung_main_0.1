package testclasses;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class TestActivationInpup extends JFrame {
	public static void main(String[] args) {
		new TestActivationInpup();
	}

	public TestActivationInpup() {
		super("Test Window");
		this.setSize(new Dimension(200, 100));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout());

		this.add(component());
		this.setVisible(true);
	}

	private JPanel component() {
		JPanel content = new JPanel(new BorderLayout());

		JTextField name = new JTextField(20);
		JTextField lastName = new JTextField(20);
		JTextField activationCode = new JTextField(20);

		Box horizontalBox = Box.createHorizontalBox();
		horizontalBox.add(Box.createHorizontalStrut(20));
		horizontalBox.add(name);
		horizontalBox.add(Box.createHorizontalGlue());
		horizontalBox.add(lastName);
		horizontalBox.add(Box.createHorizontalStrut(20));

		Box verticalBox = Box.createVerticalBox();
		// verticalBox.add(Box.createVerticalStrut(50));
		verticalBox.add(horizontalBox);
		verticalBox.add(activationCode);
		verticalBox.add(Box.createVerticalStrut(50));

		content.add(verticalBox);

		return content;
	}

}
