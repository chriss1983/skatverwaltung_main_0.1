package testclasses;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.backend.model.Player;

public class MyModel {
	private List<Player>		data	= new ArrayList();
	static DefaultTableModel	dtm;
	//
	// // public Object getValueAt(int row, int col) {return ((User)
	// // data.get(row))[col];}
	// public int getColumnCount() {
	// return 6;
	// }
	//
	// public int getRowCount() {
	// return data.size();
	// }
	//
	// public void addRow(User u) {
	// int row = data.size();
	// data.add(u);
	// fireTableRowsInserted(row, row);
	// }

	public static void main(String[] args) {
		// final MyModel model = new MyModel();
		// JTable table = new JTable(dm)
		// create object of table and table model
		JTable tbl = new JTable();
		dtm = new DefaultTableModel(0, 0);

		// add header of the table
		String header[] = new String[] { "Prority", "Task Title", "Start", "Pause", "Stop", "Statulses" };

		// add header in table model
		dtm.setColumnIdentifiers(header);
		tbl.setModel(dtm);

		// add row dynamically into the table
		for (int count = 1; count <= 30; count++) {
			dtm.addRow(new Object[] { "data", "data", "data", "data", "data", "data" });
		}

		JButton button = new JButton("add row");
		button.addActionListener(new ActionListener() {
			int		count;
			Player	u	= new Player();

			@Override
			public void actionPerformed(ActionEvent e) {
				Player u = new Player();
				u.setFirstName("Peter");
				u.setLastName("Maier");

				dtm.addRow(new Object[] { u.getFirstName(), u.getLastName() });
				System.out.println(dtm.getRowCount());
			}

		});
		JFrame f = new JFrame("MyModel");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container cp = f.getContentPane();
		cp.add(new JScrollPane(new JTable(dtm)), BorderLayout.CENTER);
		cp.add(button, BorderLayout.SOUTH);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
	}
}