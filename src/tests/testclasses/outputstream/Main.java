package testclasses.outputstream;

import java.awt.Dimension;
import java.io.PrintStream;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import testclasses.outputstream.JComponentOutputStream.JComponentHandler;

public class Main extends JFrame {
	private final Logger	logger	= LoggerFactory.getLogger(Main.class);

	private int				width	= 600;
	private int				height	= 400;
	private static String	title	= "Console Test";

	public Main() {
		super(title);
		initFrame();

		showLog();
	}

	private void showLog() {
		for (int i = 0; i < 50; i++)
			logger.debug("hello from logger " + i);

	}

	private void initFrame() {
		this.setTitle(title);
		this.setSize(new Dimension(width, height));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.add(test());

		this.setVisible(true);

	}

	private JScrollPane test() {
		JLabel console = new JLabel();
		JComponentOutputStream consoleOutputStream = new JComponentOutputStream(console, new JComponentHandler() {
			private StringBuilder sb = new StringBuilder();

			@Override
			public void setText(JComponent swingComponent, String text) {
				sb.delete(0, sb.length());
				append(swingComponent, text);
			}

			@Override
			public void replaceRange(JComponent swingComponent, String text, int start, int end) {
				sb.replace(start, end, text);
				redrawTextOf(swingComponent);
			}

			@Override
			public void append(JComponent swingComponent, String text) {
				sb.append(text);
				redrawTextOf(swingComponent);
			}

			private void redrawTextOf(JComponent swingComponent) {
				((JLabel) swingComponent).setText("<html><pre>" + sb.toString() + "</pre></html>");
			}
		});

		PrintStream con = new PrintStream(consoleOutputStream);
		System.setOut(con);
		System.setErr(con);

		// Optional: add a scrollpane around the console for having scrolling
		// bars
		JScrollPane sp = new JScrollPane( //
				console, //
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, //
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED //
		);
		return sp;
	}

	public static void main(String args[]) {
		new Main();
	}
}
