package testclasses.outputstream;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;
import javax.swing.table.AbstractTableModel;

import org.apache.log4j.Logger;

public class Log4j2Example {
	class LogModel extends AbstractTableModel {

		@Override
		public int getColumnCount() {
			// TODO Auto-generated method stub
			return 1;
		}

		@Override
		public int getRowCount() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			// TODO Auto-generated method stub
			switch (columnIndex) {
				case 0:
					return null;
				default:
					return null;
			}
		}

	}

	private final JTextArea	textarea	= new JTextArea();
	private final LogModel	model		= new LogModel();
	private final JTable	table		= new JTable(model);
	static Log4j2Example	INSTANCE	= new Log4j2Example();
	JFrame					frame		= new JFrame();

	void run() {
		frame.setLayout(new BorderLayout());
		table.setBorder(new TitledBorder("Table"));
		textarea.setBorder(new TitledBorder("Text Area"));
		table.setPreferredSize(new Dimension(100, 300));
		textarea.setEditable(false);

		frame.add(table, BorderLayout.NORTH);
		frame.add(new JScrollPane(textarea));

		frame.setVisible(true);
		frame.setSize(400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public JTextArea getJTextArea() {
		return this.textarea;
	}

	static final Logger logger = Logger.getLogger(Log4j2Example.class.getName());

	public static void main(String[] args) {
		INSTANCE.run();
		// PropertyConfigurator.configure("resources/log4j.properties");

		JTextAreaAppender jtaa = new JTextAreaAppender(INSTANCE.getJTextArea());
		logger.addAppender(jtaa);

		logger.warn("first");
		logger.warn("second");
		logger.warn("third");
		logger.debug("Sample debug message");
		logger.info("Sample info message");
		logger.warn("Sample warn message");
		logger.fatal("Sample fatal message");

		logger.trace("fourth shouldn't be printed");

		Hello hello = new Hello();
		if (!hello.callMe()) {
			logger.error("Ohh!Failed!");
		}

	}
}