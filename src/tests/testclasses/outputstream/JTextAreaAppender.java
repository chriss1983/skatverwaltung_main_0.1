package testclasses.outputstream;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.LoggingEvent;

public class JTextAreaAppender extends AppenderSkeleton implements DocumentListener {
	// --------------------------------------------------------------------------
	// Fields
	// --------------------------------------------------------------------------

	/**
	 * Text area that logging statements are directed to.
	 */
	private JTextArea		textArea_;

	/**
	 * Layout for logging statements.
	 */
	private PatternLayout	layout_;

	// --------------------------------------------------------------------------
	// Constructors
	// --------------------------------------------------------------------------
	public JTextAreaAppender() {

	}

	/**
	 * Creates a new text area appender.
	 *
	 * @param textArea
	 *            Text area to connect the appender to.
	 */
	public JTextAreaAppender(JTextArea textArea) {
		textArea_ = textArea;
		textArea_.getDocument().addDocumentListener(this);
		layout_ = new PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1} - %m%n");
	}

	// --------------------------------------------------------------------------
	// Public
	// --------------------------------------------------------------------------

	/**
	 * Returns the text area.
	 *
	 * @return JTextArea
	 */
	public JTextArea getTextArea() {
		return textArea_;
	}

	// --------------------------------------------------------------------------
	// Overrides org.apache.log4j.AppenderSkeleton
	// --------------------------------------------------------------------------

	/**
	 * @see org.apache.log4j.AppenderSkeleton#append(
	 *      org.apache.log4j.spi.LoggingEvent)
	 */
	@Override
	public void append(LoggingEvent loggingEvent) {
		textArea_.append(layout_.format(loggingEvent));
	}

	/**
	 * @see org.apache.log4j.Appender#requiresLayout()
	 */
	@Override
	public boolean requiresLayout() {
		return false;
	}

	/**
	 * @see org.apache.log4j.Appender#close()
	 */
	@Override
	public void close() {
	}

	// --------------------------------------------------------------------------
	// javax.swing.event.DocumentListener Interface
	// --------------------------------------------------------------------------

	/**
	 * @see javax.swing.event.DocumentListener#changedUpdate(
	 *      javax.swing.event.DocumentEvent)
	 */
	@Override
	public void changedUpdate(DocumentEvent event) {
	}

	/**
	 * @see javax.swing.event.DocumentListener#removeUpdate(
	 *      javax.swing.event.DocumentEvent)
	 */
	@Override
	public void removeUpdate(DocumentEvent event) {
	}

	/**
	 * Sets the caret position to the end of the text in the text component
	 * whenever it is updated.
	 *
	 * @see javax.swing.event.DocumentListener#insertUpdate(
	 *      javax.swing.event.DocumentEvent)
	 */
	@Override
	public void insertUpdate(DocumentEvent event) {
		textArea_.setCaretPosition(textArea_.getDocument().getLength());
	}
}