package testclasses.outputstream;

import org.apache.log4j.Logger;

public class Hello {
	// static final Logger logger = LogManager.getLogger(Hello.class.getName());
	static final Logger logger = Logger.getLogger(Hello.class.getName());

	public boolean callMe() {
		// logger.entry();
		logger.error("Inside Hello Logger!");
		return false; // logger.exit(false);
	}
}