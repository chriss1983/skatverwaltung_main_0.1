package testclasses;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;

public class JTableTest extends JFrame {

	public static void main(String[] args) {
		new JTableTest();
	}

	public JTableTest() {
		this.setTitle("SSCCE");

		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();

		c.gridx = 0;
		c.gridy = 0;

		c.insets = new Insets(10, 10, 10, 10);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;
		JTable testTable = new JTable(10, 2);
		panel.add(testTable, c);

		this.add(panel);
		this.pack();
		this.setVisible(true);
	}
}