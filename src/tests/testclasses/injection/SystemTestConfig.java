package testclasses.injection;

// import test.injection.SystemTestConfig.DefaultRepositoryConfig;
// import test.injection.SystemTestConfig.ServiceConfig;

/**
 * {@link http://stackoverflow.com/questions/12782000/how-to-autowire-an-object-without-spring-xml-context-file}
 * https://www.tutorialspoint.com/spring/spring_java_based_configuration.htm
 * 
 * @author chriss1983
 */

/*
 * public class SystemTestConfig {
 * @Configuration
 * public class ServiceConfig {
 * private @Autowired RepositoryConfig repositoryConfig;
 * public @Bean TransferService transferService() {
 * return new TransferServiceImpl(repositoryConfig.accountRepository());
 * }
 * }
 * @Configuration
 * public interface RepositoryConfig {
 * @Bean
 * AccountRepository accountRepository();
 * }
 * @Configuration
 * public class DefaultRepositoryConfig implements RepositoryConfig {
 * public @Bean AccountRepository accountRepository() {
 * return new JdbcAccountRepository(...);
 * }
 * }
 * @Configuration
 * @Import({ ServiceConfig.class, DefaultRepositoryConfig.class }) // import
 * // the
 * // concrete
 * // config!
 * public class SystemTestConfig {
 * public @Bean DataSource dataSource() {
 * // return DataSource
 * }
 * }
 * public static void main(String[] args) {
 * ApplicationContext ctx = new
 * AnnotationConfigApplicationContext(SystemTestConfig.class);
 * TransferService transferService = ctx.getBean(TransferService.class);
 * transferService.transfer(100.00, "A123", "C456");
 * }
 * }
 */
