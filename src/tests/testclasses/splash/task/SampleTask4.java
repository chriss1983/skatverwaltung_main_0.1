package testclasses.splash.task;

public class SampleTask4 implements TaskExecuter {

	@Override
	public void runTask() {
		System.out.println("Task 4 is executed");
	}

	@Override
	public String getTaskName() {
		return "Hello from Task 4";
	}

}
