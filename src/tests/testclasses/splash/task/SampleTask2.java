package testclasses.splash.task;

public class SampleTask2 implements TaskExecuter {

	@Override
	public void runTask() {
		System.out.println("Task 2 is executed");
	}

	@Override
	public String getTaskName() {
		return "Hello from Task 2";
	}

}
