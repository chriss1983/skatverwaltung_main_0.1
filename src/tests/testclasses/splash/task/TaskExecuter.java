package testclasses.splash.task;

public interface TaskExecuter {
	void runTask();

	String getTaskName();
}
