package testclasses.splash.task;

public class SampleTask5 implements TaskExecuter {

	@Override
	public void runTask() {
		System.out.println("Task 5 is executed");
	}

	@Override
	public String getTaskName() {
		return "Hello from Task 5";
	}

}
