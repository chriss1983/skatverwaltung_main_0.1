package testclasses.splash.task;

public class SampleTask1 implements TaskExecuter {

	@Override
	public void runTask() {
		System.out.println("Task 1 is executed");
	}

	@Override
	public String getTaskName() {
		return "Hello from Task 1";
	}

}
