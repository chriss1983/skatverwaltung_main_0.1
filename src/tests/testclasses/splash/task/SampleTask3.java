package testclasses.splash.task;

public class SampleTask3 implements TaskExecuter {

	@Override
	public void runTask() {
		System.out.println("Task 3 is executed");
	}

	@Override
	public String getTaskName() {
		return "Hello from Task 3";
	}

}
