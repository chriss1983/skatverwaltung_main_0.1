package testclasses.jtable;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;

import com.backend.model.Player;

public class TreeWithTableRenderer extends JFrame {

	private static final long	serialVersionUID	= 1L;
	private JTree				tree;

	public TreeWithTableRenderer() {
		Player p = new Player();

		p.setFirstName("Peter");
		p.setLastName("Maier");
		p.setCity("Mettmann");
		p.setZipCode("40822");

		DefaultMutableTreeNode root = new DefaultMutableTreeNode("root");
		DefaultMutableTreeNode AA1 = new DefaultMutableTreeNode("Group One");

		DefaultMutableTreeNode AA2 = new DefaultMutableTreeNode("Two");
		// DefaultMutableTreeNode AA3 = new DefaultMutableTreeNode("Points");
		// DefaultMutableTreeNode AA4 = new DefaultMutableTreeNode("City");
		//
		// DefaultMutableTreeNode BB2 = new DefaultMutableTreeNode("BB2");
		// DefaultMutableTreeNode B = new DefaultMutableTreeNode("B");
		//
		DefaultMutableTreeNode CC1 = new DefaultMutableTreeNode(p);
		// DefaultMutableTreeNode CC2 = new DefaultMutableTreeNode("CC2");
		// DefaultMutableTreeNode C = new DefaultMutableTreeNode("C");

		root.add(AA1);
		root.add(AA2);
		// root.add(AA3);
		// root.add(AA4);
		// root.add(C);
		// AA1.add(BB2);
		root.add(CC1);
		// AA1.add(CC2);
		tree = new JTree(root);
		tree.setCellRenderer(new MyTableInTreeCellRenderer());
		tree.setRowHeight(0);
		JScrollPane jsp = new JScrollPane(tree);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(jsp, BorderLayout.CENTER);
		pack();
		setLocationRelativeTo(null);
	}

	class MyTableInTreeCellRenderer extends JPanel implements TreeCellRenderer {

		private static final long	serialVersionUID	= 1L;
		private JTable				table;

		public MyTableInTreeCellRenderer() {
			super(new BorderLayout());
			table = new JTable();
			JScrollPane scrollPane = new JScrollPane(table);
			add(scrollPane);
		}

		@Override
		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			final String v = (String) ((DefaultMutableTreeNode) value).getUserObject();
			table.setModel(new DefaultTableModel() {

				private static final long serialVersionUID = 1L;

				@Override
				public int getRowCount() {
					return 4;
				}

				@Override
				public int getColumnCount() {
					return 6;
				}

				@Override
				public Object getValueAt(int row, int column) {
					return v + ":" + row + ":" + column;
				}
			});
			table.setPreferredScrollableViewportSize(table.getPreferredSize());
			return this;
		}
	}

	public static void main(String[] args) throws Exception {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new TreeWithTableRenderer().setVisible(true);
			}
		});
	}
}