package testclasses.jtable;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.backend.model.Player;

public class VstTableItemModel extends AbstractTableModel {

	private List<Player> users;

	public VstTableItemModel() {
	}

	public void setUser(List<Player> user) {
		this.users = new ArrayList<Player>(users);
	}

	@Override
	public int getRowCount() {
		return users.size();
	}

	@Override
	public int getColumnCount() {
		return 6;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		Object value = "??";
		Player user = users.get(rowIndex);
		switch (columnIndex) {
			case 0:
				value = user.getFirstName();
				break;
			case 1:
				value = user.getLastName();
				break;
			case 2:
				value = user.getStreet();
				break;
			case 3:
				value = user.getHouseNumber();
				break;
			case 4:
				value = user.getZipCode();
				break;
			case 5:
				value = user.getEMail();
				break;
		}

		return value;

	}

	@Override
	public Class<?> getColumnClass(int rowIndex) {
		// User user = tiModel.getUserAt(rowIndex);
		return null;
	}

	/*
	 * Override this if you want the values to be editable...
	 * public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
	 * //....
	 * }
	 */

	/**
	 * This will return the user at the specified row...
	 * 
	 * @param row
	 * @return
	 */
	public Player getUserAt(int row) {
		return users.get(row);
	}

}