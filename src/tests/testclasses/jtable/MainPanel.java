package testclasses.jtable;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DragSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Vector;

import javax.activation.ActivationDataFlavor;
import javax.activation.DataHandler;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;

import com.backend.model.IModel;
import com.backend.model.Player;

public final class MainPanel extends JPanel {
	private final TransferHandler	handler		= new TableRowTransferHandler();
	private final String[]			columnNames	= { "String", "Integer", "Boolean" };
	private Player					user		= new Player();
	DefaultMutableTreeNode			root		= new DefaultMutableTreeNode("root");

	protected static List<String>	rowNames;

	public List<IModel>				data;

	public static void main(String... args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
			ex.printStackTrace();
		}
		JFrame frame = new JFrame("DragRowsAnotherTable");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().add(new MainPanel());
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}

class TableRowTransferHandler extends TransferHandler {
	private final DataFlavor	localObjectFlavor;
	private int[]				indices;
	private int					addIndex	= -1;	// Location where items were
													// added
	private int					addCount;			// Number of items added.
	private JComponent			source;

	protected TableRowTransferHandler() {
		super();
		localObjectFlavor = new ActivationDataFlavor(Object[].class, DataFlavor.javaJVMLocalObjectMimeType, "Array of items");
	}

	@Override
	protected Transferable createTransferable(JComponent c) {
		source = c;
		JTable table = (JTable) c;
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		List<Object> list = new ArrayList<Object>();
		indices = table.getSelectedRows();
		for (int i : indices) {
			list.add(model.getDataVector().get(i));
		}
		Object[] transferedObjects = list.toArray();
		return new DataHandler(transferedObjects, localObjectFlavor.getMimeType());
	}

	@Override
	public boolean canImport(TransferHandler.TransferSupport info) {
		JTable table = (JTable) info.getComponent();
		boolean isDropable = info.isDrop() && info.isDataFlavorSupported(localObjectFlavor);
		// XXX bug?
		table.setCursor(isDropable ? DragSource.DefaultMoveDrop : DragSource.DefaultMoveNoDrop);
		return isDropable;
	}

	@Override
	public int getSourceActions(JComponent c) {
		return TransferHandler.MOVE; // TransferHandler.COPY_OR_MOVE;
	}

	@Override
	public boolean importData(TransferHandler.TransferSupport info) {
		if (!canImport(info)) {
			return false;
		}
		TransferHandler.DropLocation tdl = info.getDropLocation();
		if (!(tdl instanceof JTable.DropLocation)) {
			return false;
		}
		JTable.DropLocation dl = (JTable.DropLocation) tdl;
		JTable target = (JTable) info.getComponent();
		DefaultTableModel model = (DefaultTableModel) target.getModel();
		int index = dl.getRow();
		// boolean insert = dl.isInsert();
		int max = model.getRowCount();
		if (index < 0 || index > max) {
			index = max;
		}
		addIndex = index;
		target.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		try {
			Object[] values = (Object[]) info.getTransferable().getTransferData(localObjectFlavor);
			if (Objects.equals(source, target)) {
				addCount = values.length;
			}
			for (int i = 0; i < values.length; i++) {
				int idx = index++;
				model.insertRow(idx, (Vector) values[i]);
				target.getSelectionModel().addSelectionInterval(idx, idx);
			}
			return true;
		} catch (UnsupportedFlavorException | IOException ex) {
			ex.printStackTrace();
		}
		return false;
	}

	@Override
	protected void exportDone(JComponent c, Transferable data, int action) {
		cleanup(c, action == TransferHandler.MOVE);
	}

	private void cleanup(JComponent c, boolean remove) {
		if (remove && indices != null) {
			c.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			DefaultTableModel model = (DefaultTableModel) ((JTable) c).getModel();
			if (addCount > 0) {
				for (int i = 0; i < indices.length; i++) {
					if (indices[i] >= addIndex) {
						indices[i] += addCount;
					}
				}
			}
			for (int i = indices.length - 1; i >= 0; i--) {
				model.removeRow(indices[i]);
			}
		}
		indices = null;
		addCount = 0;
		addIndex = -1;
	}

	class MyTableInTreeCellRenderer implements TreeCellRenderer {

		private static final long	serialVersionUID	= 1L;
		private JTable				table;

		public MyTableInTreeCellRenderer() {
			table = new JTable();
			JScrollPane scrollPane = new JScrollPane(table);
		}

		@Override
		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			final String v = (String) ((DefaultMutableTreeNode) value).getUserObject();
			table.setModel(new DefaultTableModel() {

				private static final long serialVersionUID = 1L;

				@Override
				public int getRowCount() {
					return 2;
				}

				@Override
				public int getColumnCount() {
					return 6;// MainPanel.rowNames.size();
				}

				@Override
				public Object getValueAt(int row, int column) {
					return v + ":" + row + ":" + column;
				}
			});
			table.setPreferredScrollableViewportSize(table.getPreferredSize());
			return tree;
		}
	}
}