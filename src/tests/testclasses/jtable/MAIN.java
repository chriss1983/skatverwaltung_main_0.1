package testclasses.jtable;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

public class MAIN extends JFrame {

	Button				ltor, rtol;
	JList				homelist, shoppinglist;
	DefaultListModel	homefoodlist		= new DefaultListModel();
	DefaultListModel	shoppingfoodlist	= new DefaultListModel();
	JTextField			foodlog;

	String[]			hfood				= { "Tuna", "Mayo", "Ketchup", "Sun Flower Oil", "Buscuits", "Cookies", "Turkey" };
	String[]			sfood				= { "Chocolate", "bread", "Milk", "Toast", "Beef", "Chicken" };

	public static void main(String[] args) {

		new MAIN();

	}

	private MAIN() {
		JPanel thepanel = new JPanel();
		thehandler handler = new thehandler();

		this.setLocationRelativeTo(null);
		this.setSize(400, 400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setTitle("Shopping List");
		this.add(thepanel);

		// Creating the Home List(left list)
		for (String homefood : hfood) {
			homefoodlist.addElement(homefood);
		}

		homelist = new JList(homefoodlist);
		homelist.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		thepanel.add(homelist);

		// Buttons for moving lists from left to right
		ltor = new Button(">>");
		thepanel.add(ltor);
		ltor.addActionListener(handler);

		rtol = new Button("<<");
		rtol.addActionListener(handler);
		thepanel.add(rtol);

		// Creating the Shopping list(right list)
		for (String shoppingfood : sfood) {
			shoppingfoodlist.addElement(shoppingfood);
		}
		shoppinglist = new JList(shoppingfoodlist);
		shoppinglist.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		thepanel.add(shoppinglist);

	}

	// ActionListener

	private class thehandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// The HomeList to the ShoppingList
			if (e.getSource() == ltor) {
				if (homelist.isSelectionEmpty() == false) {
					for (String x : hfood) {
						if (x == homelist.getSelectedValue()) {
							shoppingfoodlist.addElement(x);
						}

					}
				}

			}
		}
	}
}