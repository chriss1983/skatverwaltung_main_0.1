package testclasses.serial;

import java.math.BigInteger;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.logging.Logger;

public class StupidRSA {

	private static final BigInteger E = BigInteger.valueOf(3L);

	static class KeyPair {
		public final BigInteger	publicKey;
		public final BigInteger	privateKey;

		public KeyPair(final BigInteger n, final BigInteger d) {
			this.publicKey = n;
			this.privateKey = d;
		}
	}

	public KeyPair genKeyPair() {
		final Random r = new Random();

		boolean hasPrime = false;

		BigInteger p = BigInteger.ZERO;
		while (!hasPrime) {
			p = BigInteger.probablePrime(20, r);
			hasPrime = p.isProbablePrime(8);
		}

		hasPrime = false;
		BigInteger q = BigInteger.ZERO;
		while (!hasPrime) {
			q = BigInteger.probablePrime(20, r);
			hasPrime = p.isProbablePrime(8) && !(q.equals(p));
		}

		BigInteger n = p.multiply(q);

		final BigInteger eTot = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
		BigInteger d = E.modInverse(eTot);

		return new KeyPair(n, d);
	}

	public BigInteger hash(final BigInteger number) {
		return number.mod(BigInteger.valueOf(8379));
	}

	public BigInteger sign(final BigInteger number, final KeyPair kp) {
		return hash(number).modPow(kp.privateKey, kp.publicKey);
	}

	public boolean verify(final BigInteger message, final BigInteger signature, final BigInteger publicKey) {
		return hash(message).equals(signature.modPow(E, publicKey));
	}

	public static void main(String[] args) throws SocketException, UnknownHostException {

		final StupidRSA s = new StupidRSA();

		// final KeyPair kp = s.genKeyPair();
		// final KeyPair kp = new KeyPair(BigInteger.valueOf(418573),
		// BigInteger.valueOf(278187));
		final KeyPair kp = new KeyPair(BigInteger.valueOf(887870315911L), BigInteger.valueOf(591912286555L));
		final BigInteger message = BigInteger.valueOf(4534287589L);

		final BigInteger signature = s.sign(message, kp);

		final boolean isValid = s.verify(message, signature, kp.publicKey);

		Logger.getAnonymousLogger().info(String.format("private key: %1$s\npublic key: %2$s\nmessage: %3$s\nsignature: %4$s\nvalid: %5$s", kp.privateKey, kp.publicKey, message, signature, isValid));
	}

}