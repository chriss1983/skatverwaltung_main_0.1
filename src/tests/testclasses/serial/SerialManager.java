package testclasses.serial;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class SerialManager {
	private String	charset		= "ABCDEFGHJKLMNPQRSTUVWXYZ13456789";
	private char[]	charArray;
	Random			generator	= new Random();
	private byte[]	passwd;

	public String Generate(String password) {
		passwd = toByteArray(password);
		charArray = strToChar(charset);
		byte[] data = new byte[15];
		generator.nextBytes(data);
		byte[] tohash = new byte[5 + passwd.length];
		System.arraycopy(data, 0, tohash, 0, 5);
		System.arraycopy(passwd, 0, tohash, 5, passwd.length);
		try {
			byte[] hash = getHash(tohash);
			System.arraycopy(hash, 0, data, 5, 10);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		int num = 17;
		for (int i = 0; i < tohash.length; i++)
			num += tohash[i];
		String serial = Encode(data) + charArray[num & 31];
		String ret = "";
		for (int i = 0; i < 5; i++) {
			ret += serial.substring((i * 5), (i * 5) + 5);
			if (i < 4)
				ret += "-";
		}
		return ret;
	}

	public boolean Validate(String serial, String password) {
		print("Validation Start...");
		char[] x = strToChar(serial);
		passwd = toByteArray(password);
		serial = serial.toUpperCase();

		if (x.length != 29)
			return false;
		print("Step 1 Complete...");
		for (int i = 0; i < 29; i++) {
			if (i % 6 == 5 && x[i] != '-')
				return false;
		}
		print("Step 2 Complete...");

		byte[] serial2 = Decode(serial.replace("-", ""));
		byte[] tohash = new byte[5 + passwd.length];
		System.arraycopy(serial2, 0, tohash, 0, 5);
		System.arraycopy(passwd, 0, tohash, 5, passwd.length);
		int num = 17;
		for (int i = 0; i < tohash.length; i++) {
			num += tohash[i];
		}
		print("Step 3 Complete..." + x.length);
		if ((num & 31) != getPosition(x[28]))
			return false;
		print("Step 4 Complete...");
		try {
			byte[] hash = getHash(tohash);
			for (int i = 0; i < 10; i++) {
				System.out.println("Serial: " + serial2[i + 5] + " Hash: " + hash[i]);
				if (serial2[i + 5] != hash[i])
					return false;
			}
			print("Step 5 Complete...");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		print("Complete...");
		return true;
	}

	private String Encode(byte[] data) {
		String ret = "";
		for (int i = 0; i < data.length; i += 5) {
			ret += charArray[data[i] >> 3 & 0x1f];
			ret += charArray[(data[i] << 2 | data[i + 1] >> 6) & 0x1f];
			ret += charArray[(data[i + 1] >> 1) & 0x1f];
			ret += charArray[(data[i + 1] << 4 | data[i + 2] >> 4) & 0x1f];
			ret += charArray[(data[i + 2] << 1 | data[i + 3] >> 7) & 0x1f];
			ret += charArray[data[i + 3] >> 2 & 0x1f];
			ret += charArray[(data[i + 3] << 3 | data[i + 4] >> 5) & 0x1f];
			ret += charArray[data[i + 4] & 0x1f];
		}
		return ret;
	}

	public byte[] Decode(String serial) {
		char[] x = strToChar(serial);
		byte[] table = new byte[256];
		for (int i = 0; i < charArray.length; i++) {
			table[charArray[i]] = (byte) i;
		}
		byte[] ret = new byte[x.length * 5 / 8];
		int pos = 0;
		for (int i = 0; i <= x.length - 8;) {
			byte b1 = table[x[i++]];
			byte b2 = table[x[i++]];
			byte b3 = table[x[i++]];
			byte b4 = table[x[i++]];
			byte b5 = table[x[i++]];
			byte b6 = table[x[i++]];
			byte b7 = table[x[i++]];
			byte b8 = table[x[i++]];

			ret[pos++] = (byte) (b1 << 3 | b2 >> 2);
			ret[pos++] = (byte) (b2 << 6 | b3 << 1 | b4 >> 4);
			ret[pos++] = (byte) (b4 << 4 | b5 >> 1);
			ret[pos++] = (byte) (b5 << 7 | b6 << 2 | b7 >> 3);
			ret[pos++] = (byte) (b7 << 5 | b8);
		}
		return ret;
	}

	private int getPosition(char character) {
		for (int i = 0; i < 32; i++) {
			if (charArray[i] == character)
				return i;
		}
		return -1;
	}

	public int byteArrayToInt(byte[] b, int offset) {
		int value = 0;
		for (int i = 0; i < 4; i++) {
			int shift = (4 - 1 - i) * 8;
			value += (b[i + offset] & 0x000000FF) << shift;
		}
		return value;
	}

	private char[] strToChar(String str) {
		char[] c = str.toCharArray();
		return c;
	}

	public byte[] toByteArray(String p) {
		String stringToConvert = p;
		byte[] theByteArray = stringToConvert.getBytes();
		return theByteArray;
	}

	public byte[] getHash(byte[] toHash) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		digest.reset();
		return digest.digest(toHash);
	}

	public void print(String str) {
		System.out.println(">" + str);
	}

	public static void main(String[] args) {
		SerialManager sm = new SerialManager();
		char[] x = "8FF955C79FCD44BB52FF0CBD77D4B72F02DC735B".toCharArray();
		for (int i = 0; i < x.length; i++) {
			System.out.println(x[i]);
		}

		// System.out.println();
		// String pass = "HelloWorld";
		// String x = sm.Generate(pass);
		// System.out.println(">"+x);
		// if(sm.Validate(x, pass))
		// System.out.println(">VALID!");
		// else
		// System.out.println(">NOT VALID!");
	}

}