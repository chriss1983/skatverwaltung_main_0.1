package testclasses.database;

import java.sql.SQLException;
import java.util.List;

import com.backend.database.RedeyedCoreManager;
import com.backend.model.Player;

public class Snippet {
	public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, InterruptedException {

		/*
		 * Player detailsEntity = new Player();
		 * detailsEntity.setFirstName("Günter");
		 * detailsEntity.setLastName("Mielke");
		 * detailsEntity.setCity("Mettmann");
		 * Player detailsEntity2 = new Player();
		 * detailsEntity2.setFirstName("Walter");
		 * detailsEntity2.setLastName("Peter");
		 * detailsEntity2.setCity("Köln");
		 * // coreManager.saveOrUpdate(detailsEntity);
		 * // coreManager.saveOrUpdate(detailsEntity2);
		 * //
		 * List<String> conditions = new ArrayList<String>();
		 * conditions.add("FIRSTNAME = 'Günter'");
		 * List<Player> activationKeyDetails =
		 * coreManager.getAllEntities(Player.class, conditions, null, null,
		 * null);
		 * System.out.println("found " + activationKeyDetails.size() +
		 * " witch contains " + conditions.get(0));
		 * for (Player player : activationKeyDetails) {
		 * // coreManager.deleteEntity(PlayerImpl.class,
		 * player.getStartNumber());
		 * }
		 * System.out.println("print");
		 * for (Player d : activationKeyDetails) {
		 * System.out.println("Delete " + d.toString());
		 * // System.out.println(coreManager.deleteEntity(PlayerImpl.class,
		 * // d.getStartNumber()));
		 * }
		 */
		RedeyedCoreManager coreManager = RedeyedCoreManager.getDataBaseUtil();
		
		List<Player> entitys = coreManager.getAllEntities(Player.class, null, null, null, null);
		System.out.println("Entitys before delete: " + entitys.size());
		for (Player d : entitys) {

			System.out.println("Delete -> " + d.getId());
			System.out.println(coreManager.deleteEntity(Player.class, d.getId()));
			Thread.sleep(2000);

			entitys = coreManager.getAllEntities(Player.class, null, null, null, null);
		}
		System.out.println("Entitys after delete: " + entitys.size());

		// System.out.println(coreManager.deleteEntity(PlayerImpl.class, 8));

		// for (int i = 0; i < 20; i++) {
		// PlayerImpl playerImpl = coreManager.getEntity(PlayerImpl.class, i);
		// if (playerImpl != null) {
		// System.out.println("Player = " + playerImpl.getFirstName());
		// coreManager.deleteEntity(playerImpl);
		// }
		// }
		coreManager.shutdown();
	}
}
