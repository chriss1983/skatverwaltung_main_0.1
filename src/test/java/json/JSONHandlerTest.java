package json;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.backend.jsonmapper.JSONProperties;

public class JSONHandlerTest {
	final Logger	logger	= Logger.getLogger(JSONHandlerTest.class);

	String			origURL;

	@Before
	public void setup() {
		if ((origURL = JSONProperties.getJSonProperty().getDatabase().getDatabaseURL()) == null) {
			Assert.fail();
		}
	}

	@Test
	public void readFromJSON() {
		Assert.assertEquals("jdbc:mysql://localhost:3306/test", JSONProperties.getJSonProperty().getDatabase().getDatabaseURL());
	}

	@Test()
	public void writeToJSON() {
		JSONProperties.getJSonProperty().getDatabase().setDatabaseURL("Test");
		try {
			JSONProperties.saveProperties();
		} catch (IOException e) {
			logger.debug(e.getMessage());
		}
	}

	@After
	public void unDoChangesInJSON() {
		JSONProperties.getJSonProperty().getDatabase().setDatabaseURL(origURL);
		try {
			JSONProperties.saveProperties();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.debug(e.getMessage());
		}
	}
}
