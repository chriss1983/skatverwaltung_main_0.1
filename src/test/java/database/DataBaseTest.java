package database;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.backend.database.RedeyedCoreManager;
import com.backend.pojos.Player;

public class DataBaseTest {
	private static RedeyedCoreManager	coreManager;
	private Player						player;

	final Logger						logger	= Logger.getLogger(DataBaseTest.class);

	@Before
	public void runOnceBeforeClass() {
		coreManager = RedeyedCoreManager.getDataBaseUtil();

		player = getTestPlayer();
	}

	private Player getTestPlayer() {
		Player player = new Player();

		player.setFirstName("Peter");
		player.setLastName("Maier");
		player.setStreet("Haupt Strasse");
		player.setHouseNumber("50");
		player.setZipCode("40822");
		player.setCity("Köln");
		player.setInTeam(false);
		player.setMobilePhoneNumber("0152-1234567");
		player.setPhone("0221-123456");
		player.seteMail("peter.müller@koeln.de");
		player.setPaid(true);
		player.setPlayerInfo("This is a long text");
		player.setPointsSeriesOne(500);
		player.setPointsSeriesTwo(500);
		player.setTotalScore(player.getPointsSeriesOne() + player.getPointsSeriesTwo());
		return player;
	}

	@Before
	public void setup() {

	}

	@Test
	public void addToDataBase() {
		Assert.assertNotNull(coreManager.save(player));
	}

	@Test
	public void updateDataBase() {
		System.out.println("PLAYER NAME = " + player.getFirstName());
		player.setFirstName("Mustemann");
		int playerID = coreManager.insertEntity(player);

		List<String> conditions = new ArrayList<>();
		conditions.add("ID = '" + playerID + "'");

		Player playerFromDB = coreManager.getAllEntities(Player.class, conditions, null, null, null).get(0);
		Assert.assertNotNull(playerFromDB);

		Assert.assertEquals("Mustemann", playerFromDB.getFirstName());
		logger.debug("Found " + player.getFirstName());
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void searchForNonExistingContact() {
		List<String> conditions = new ArrayList<>();
		conditions.add("ID = '-1'");

		List<Player> playerFromDB = coreManager.getAllEntities(Player.class, conditions, null, null, null);
		
		Player p = playerFromDB.get(0);
		
		
		Assert.assertNull(p);
	}

	@Test
	public void removeFromDatabase() {
		Assert.assertEquals(true, coreManager.deleteEntity(player));
	}

}
