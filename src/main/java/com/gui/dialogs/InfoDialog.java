package com.gui.dialogs;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class InfoDialog {

	/** Used for error messages. */
	public static final int	ERROR_MESSAGE		= 0;
	/** Used for information messages. */
	public static final int	INFORMATION_MESSAGE	= 1;
	/** Used for warning messages. */
	public static final int	WARNING_MESSAGE		= 2;
	/** Used for questions. */
	public static final int	QUESTION_MESSAGE	= 3;
	/** No icon is used. */
	public static final int	PLAIN_MESSAGE		= -1;

	public static void main(String args[]) {
		showInfoDialog("TEst", JOptionPane.INFORMATION_MESSAGE);
	}

	public static void showInfoDialog(String message, int messageType) {
		JOptionPane.showMessageDialog(new JFrame(), message, "Test", messageType);
	}

	public static int showYesNoDialog(String message, String messageHeader, int dialogButton) {
		int dialogResult = JOptionPane.showConfirmDialog(null, message, messageHeader, dialogButton);
		return dialogResult;
	}
}
