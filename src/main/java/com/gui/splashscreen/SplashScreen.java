package com.gui.splashscreen;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;

import com.backend.jsonmapper.JSONProperties;
import com.backend.utils.ImageLoader;

public class SplashScreen extends JWindow {
	private static final long	serialVersionUID	= 1L;

	private JProgressBar		progressBar;
	static final int			MY_MINIMUM			= 0;
	static final int			MY_MAXIMUM			= 100;

	private int					currentPercent		= 0;
	private Task				task;
	private DoSomething			ds;
	private String				progressBarText;

	public SplashScreen() {
		progressBar = new JProgressBar();
		progressBar.setMinimum(MY_MINIMUM);
		progressBar.setMaximum(MY_MAXIMUM);
		progressBar.setStringPainted(true);
		progressBar.setAlignmentX(Component.CENTER_ALIGNMENT);
		progressBar.setStringPainted(true);
		progressBar.setValue(0);

	}

	public void updateProgressBar(int percent) {
		task.updateProgress(percent);
		System.out.println("Updated to " + percent);
	}

	public void updateProgressBarText(String info) {
		this.progressBarText = info;
	}

	// A simple little method to show a title screen in the center
	// of the screen for the amount of time given in the constructor
	public void showSplash() {
		JPanel content = (JPanel) getContentPane();
		content.setBackground(Color.white);

		// Set the window's bounds, centering the window
		int width = 600;
		int height = 400;
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int x = screen.width / 2;
		int y = screen.height / 2;
		this.setBounds(x, y, width, height);
		this.setLocationRelativeTo(null);

		setBackground(new Color(0, 0, 0, 0));
		// setOpacity(0.5f);

		// Build the splash screen
		JLabel label = new JLabel(new ImageIcon(ImageLoader.getImage(ImageLoader.ImageTyp.LOGO, "skat")), SwingConstants.CENTER);
		JLabel info = new JLabel(JSONProperties.getJSonProperty().getGlobal().getAppName() + " \nVersion " + JSONProperties.getJSonProperty().getGlobal().getVersion(), SwingConstants.CENTER);
		info.setFont(new Font("Sans-Serif", Font.BOLD, 16));
		info.setForeground(Color.WHITE);
		JLabel copyrt = new JLabel("Copyright 2016, Christian Richter", SwingConstants.CENTER);
		copyrt.setFont(new Font("Sans-Serif", Font.BOLD, 14));
		copyrt.setForeground(Color.BLUE);

		Box infoBox = Box.createHorizontalBox();
		infoBox.add(Box.createHorizontalGlue());
		infoBox.add(info);
		infoBox.add(Box.createHorizontalGlue());

		Box copyrtBox = Box.createHorizontalBox();
		copyrtBox.add(Box.createHorizontalGlue());
		copyrtBox.add(copyrt);
		copyrtBox.add(Box.createHorizontalGlue());

		Box verticalContendBox = Box.createVerticalBox();
		verticalContendBox.add(Box.createVerticalGlue());
		verticalContendBox.add(infoBox);
		verticalContendBox.add(copyrtBox);
		verticalContendBox.add(Box.createVerticalGlue());
		// verticalContendBox.add(progressBarBox);

		content.add(label, BorderLayout.NORTH);
		content.add(verticalContendBox, BorderLayout.CENTER);
		content.add(progressBar, BorderLayout.SOUTH);

		PropertyChangeListener listener = new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent event) {
				if ("progress".equals(event.getPropertyName())) {
					currentPercent = (int) event.getNewValue();

					progressBar.setValue(currentPercent);
					progressBar.setString(progressBarText);
				}
			}
		};

		task = new Task();
		task.addPropertyChangeListener(listener);
		task.execute();
	}

	public void setVisible() {
		setVisible(true);
	}

	public void hideSplash() {
		setVisible(false);
		dispose();
		// System.exit(0);
	}

	public static void main(String[] args) throws InterruptedException {
		final SplashScreen splashScreen = new SplashScreen();
		splashScreen.showSplash();
	}

	class Task extends SwingWorker<Void, String> {
		public Task() {
			ds = new DoSomething(this);
		}

		@Override
		public Void doInBackground() {
			// for (int i = 0; i < 100; i++) {
			ds.incrementPercent();
			// ds.updateProgressBarText("test ");
			// try {
			// Thread.sleep(1000);
			// } catch (InterruptedException e) {
			// e.printStackTrace();
			// }
			// }
			return null;
		}

		@Override
		public void done() {
			closeWindow();
		}

		public void updateProgress(int tick) {
			currentPercent += tick;
			setProgress(tick);
		}

		public void updateProgressBarText(String info) {
			progressBar.setString(info);
		}
	}

	public class DoSomething {
		Task task;

		public DoSomething(Task task) {
			this.task = task;
		}

		public void incrementPercent() {
			task.updateProgress(2 * 2);
		}
	}

	public void closeWindow() {
		WindowEvent close = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(close);
	}
}
