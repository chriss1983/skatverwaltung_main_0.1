package com.gui.view.splitpane.infopage;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagLayout;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.backend.pojos.Player;

/*
 * ToDo: CleanUp The Code
 */

public class InfoPane extends JPanel {
	private static final long	serialVersionUID	= 1L;
	private final int			labelFontsize		= 15;
	private final int			headingFontsize		= 18;
	private JPanel				panel;

	private JLabel				lbTitle, lbFirstName, lbLastName, lbAdress, lbHouseNr, lbZip, lbCity, lbEmail, lbMobil, lbPhone,
			lbInfos, lbPaid, lbStartNumber;
	private JLabel				dynamicFirstName, dynamicLastName, dynamicAdress, dynamicHouseNr, dynamicZip, dynamicCity,
			dynamicEmail, dynamicMobile, dynamicPhone, dynamicStartNumber, dynamicPaid;

	private JTextArea			taInfo;

	public InfoPane() {
		createContent();
	}

	private void createContent() {
		dynamicStartNumber = new JLabel();
		dynamicFirstName = new JLabel();
		dynamicLastName = new JLabel();
		dynamicAdress = new JLabel();
		dynamicHouseNr = new JLabel();
		dynamicZip = new JLabel();
		dynamicCity = new JLabel();
		dynamicEmail = new JLabel();
		dynamicMobile = new JLabel();
		dynamicPhone = new JLabel();
		dynamicPaid = new JLabel();

		/**
		 * TextArea f�r Infos erstellen
		 */
		taInfo = new JTextArea();
		taInfo.setSize(this.getHeight(), this.getWidth());

		new JButton("Speichern");

		/**
		 * Erstellen des Labels
		 */
		Font font = new Font("Calibri", Font.BOLD, headingFontsize);
		lbTitle = new JLabel("Spieler Informationen\n");
		lbTitle.setFont(font);

		lbStartNumber = new JLabel("Startnummer: ");
		lbFirstName = new JLabel("Name: ");
		lbLastName = new JLabel("Nachname: ");
		lbAdress = new JLabel("Adresse: ");
		lbHouseNr = new JLabel("Hausnummer: ");
		lbZip = new JLabel("Postleitzahl: ");
		lbCity = new JLabel("Ort: ");
		lbEmail = new JLabel("E-Mail Adresse: ");
		lbMobil = new JLabel("Handy Nummer: ");
		lbPhone = new JLabel("Telefon: ");
		lbInfos = new JLabel("Zusätzliche Informationen: ");
		lbPaid = new JLabel("Beitrag bezahlt");

		Font lbFont = new Font("Calibri", Font.BOLD, labelFontsize);

		lbStartNumber.setFont(lbFont);
		lbFirstName.setFont(lbFont);
		lbLastName.setFont(lbFont);
		lbAdress.setFont(lbFont);
		lbHouseNr.setFont(lbFont);
		lbZip.setFont(lbFont);
		lbCity.setFont(lbFont);
		lbEmail.setFont(lbFont);
		lbMobil.setFont(lbFont);
		lbPhone.setFont(lbFont);
		lbInfos.setFont(lbFont);
		lbPaid.setFont(lbFont);

		new JComboBox<String>();

		panel = new JPanel(new BorderLayout());
		new JPanel(new GridBagLayout());
		new JPanel();

		final int GAP = 5; // Default gap btwn components.
		GBHelper pos = new GBHelper(); // Create GridBag helper object.
		JPanel content = new JPanel(new GridBagLayout());
		// ... First row
		content.add(lbTitle, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(lbStartNumber, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(dynamicStartNumber, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(lbLastName, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(dynamicLastName, pos);
		// content.add(new Gap(GAP), pos.nextRow());
		// content.add(lbFirstName, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(dynamicFirstName, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(lbAdress, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(dynamicAdress, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(dynamicHouseNr, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(new Gap(GAP), pos.nextCol());
		content.add(dynamicZip, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(dynamicCity, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(lbEmail, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(dynamicEmail, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(lbPaid, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(dynamicPaid, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(lbPhone, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(dynamicPhone, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(lbMobil, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(dynamicMobile, pos);
		content.add(new Gap(GAP), pos.nextRow());
		// content.add(lbInfos, pos);
		// content.add(new Gap(2*GAP), pos.nextCol());
		// content.add(taInfo, pos);
		// content.add(new Gap(2*GAP), pos.nextRow());

		content.add(new Gap(GAP), pos.nextRow()); // Add a gap below

		int dynamicFontsize = 12;

		dynamicStartNumber.setFont(new Font("Calibri", Font.BOLD, dynamicFontsize));
		dynamicLastName.setFont(new Font("Calibri", Font.BOLD, dynamicFontsize));
		dynamicFirstName.setFont(new Font("Calibri", Font.BOLD, dynamicFontsize));
		dynamicAdress.setFont(new Font("Calibri", Font.BOLD, dynamicFontsize));
		dynamicAdress.setFont(new Font("Calibri", Font.BOLD, dynamicFontsize));
		dynamicHouseNr.setFont(new Font("Calibri", Font.BOLD, dynamicFontsize));
		dynamicZip.setFont(new Font("Calibri", Font.BOLD, dynamicFontsize));
		dynamicCity.setFont(new Font("Calibri", Font.BOLD, dynamicFontsize));
		dynamicEmail.setFont(new Font("Calibri", Font.BOLD, dynamicFontsize));
		dynamicPaid.setFont(new Font("Calibri", Font.BOLD, dynamicFontsize));
		dynamicPhone.setFont(new Font("Calibri", Font.BOLD, dynamicFontsize));
		dynamicMobile.setFont(new Font("Calibri", Font.BOLD, dynamicFontsize));

		JPanel infoPanel = new JPanel(new BorderLayout());

		taInfo.setText("Das ist ein Sehr Langer TExt mit dem ich gerade arbeite, um zu" + "Zeigen mit welcher Länge ein Text sein kann");
		taInfo.setLineWrap(true);
		// taInfo.setWrapStyleWord(true);
		taInfo.setOpaque(false);
		taInfo.setEditable(false);
		taInfo.setFont(new Font("Calibri", Font.BOLD, dynamicFontsize));

		infoPanel.add(lbInfos, BorderLayout.NORTH);
		infoPanel.add(Box.createVerticalStrut(30), BorderLayout.CENTER);
		infoPanel.add(taInfo, BorderLayout.SOUTH);

		panel.add(content, BorderLayout.CENTER);
		panel.add(infoPanel, BorderLayout.SOUTH);
		this.setSize(this.getPreferredSize());

		add(panel);

	}

	public void setPlayerToShow(Player selectedPlayer) {
		dynamicStartNumber.setText(selectedPlayer.getId() + "");
		dynamicLastName.setText(selectedPlayer.getLastName() + "");
		dynamicFirstName.setText(selectedPlayer.getFirstName() + "");
		dynamicAdress.setText(selectedPlayer.getStreet() + "");
		dynamicHouseNr.setText(selectedPlayer.getHouseNumber() + "");
		dynamicZip.setText(selectedPlayer.getZipCode() + "");
		dynamicCity.setText(selectedPlayer.getCity() + "");
		dynamicEmail.setText(selectedPlayer.getEMail() + "");
		dynamicPaid.setText(selectedPlayer.isPaid() + "");
		dynamicPhone.setText(selectedPlayer.getPhone() + "");
		dynamicMobile.setText(selectedPlayer.getMobilePhoneNumber() + "");

		this.repaint();
		// this.revalidate();
	}

}
