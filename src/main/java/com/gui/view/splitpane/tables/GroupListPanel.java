package com.gui.view.splitpane.tables;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTable;

import org.apache.log4j.Logger;

import com.backend.jsonmapper.JSONLanguageManager;
import com.backend.models.ModelsHandler;
import com.backend.pojos.Player;
import com.backend.pojos.TableModelHandler;
import com.backend.pojos.Team;
import com.gui.view.MainFrame;

public class GroupListPanel extends AbstractListPanel {
	private static final long	serialVersionUID	= 1L;

	private final Logger		logger				= Logger.getLogger(GroupListPanel.class);

	public GroupListPanel(MainFrame mainFrame) {
		super(mainFrame);
	}

	@Override
	public JTable createTable() {
//		logger.debug("create Table for Team");
		dtm = TableModelHandler.getInstance().getDefaultTeamTableModel();

		Object[] rowNamesArray = JSONLanguageManager.getLanguageProperty().getTable().getTeamHeadersNamesAsList().toArray();
		dtm.setColumnIdentifiers(rowNamesArray);
		return TableModelHandler.getInstance().getTeamTable();
	}

	@Override
	public void updateRow(Player user) {

	}

	@Override
	protected void addMouseListenerToTable() {
		table.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
				System.out.println("pressed on table is " + table.getSelectedRow());
				int selectedRowCount = 0;
				int[] selectedRows = null;

				if (e.isPopupTrigger()) {
					JTable source = (JTable) e.getSource();

					int row = source.rowAtPoint(e.getPoint());
					int column = source.columnAtPoint(e.getPoint());
					selectedRows = source.getSelectedRows();

					TableModelHandler.getInstance().setSelectedRowCount(source.getSelectedRow());

					logger.debug("Row: '" + row + "' Col: '" + column + "'");

					contextMenu.show(e.getComponent(), e.getX(), e.getY());
					System.out.println("PlayerDATA = " + ModelsHandler.getInstance().getTeamList().size());

					selectedRowCount = source.getSelectedRows().length;
					System.out.println("Selected Row Count " + selectedRowCount); 
					
					for(Team team : ModelsHandler.getInstance().getTeamList()) {
						System.out.println("-----------------------------");
						System.out.println("-> Team = " + team.getTeamName() + " / " + team.getId() );
						for(Player player : team.getPlayers()) {
							System.out.println("\t |Contents : " + player.getId() + " " + player.getFirstName() + " " + player.getLastName());
						}
//						System.out.println("-----------------------------");
					}

					if (selectedRowCount == 1) {
						System.out.println("ROW = " + row + " Teamsize = " + ModelsHandler.getInstance().getTeamList().size());
						
						//TableModelHandler.getInstance().getTeamTable().convertRowIndexToModel(0);
						
						ModelsHandler.getInstance().addSelectedTeam(ModelsHandler.getInstance().getTeamList().get(row));
						System.out.println("SELECTED TEAM: " + ModelsHandler.getInstance().getTeamList().get(TableModelHandler.getInstance().getTeamTable().convertRowIndexToModel(row)));
					} else {
						for (int i = 0; i < selectedRowCount; i++) {
							System.out.println("Add TEAM: " + ModelsHandler.getInstance().getTeamList().get(selectedRows[i]));
							ModelsHandler.getInstance().addSelectedTeam(ModelsHandler.getInstance().getTeamList().get(selectedRows[i]));
						}
					}
				}

				contextMenu.pack();
				contextMenu.repaint();
				contextMenu.revalidate();
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {

			}
		});

	}

	@Override
	protected void addMouseListenerToTableHeader() {

	}
}
