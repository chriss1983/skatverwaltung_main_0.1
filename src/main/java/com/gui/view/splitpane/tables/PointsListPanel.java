package com.gui.view.splitpane.tables;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.backend.jsonmapper.JSONLanguageManager;
import com.backend.pojos.Player;
import com.gui.view.MainFrame;
import com.gui.view.splitpane.infopage.InfoPane;

public class PointsListPanel extends AbstractListPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PointsListPanel(MainFrame mainFrame, InfoPane contendSplitPane) {
		super(mainFrame);
	}

	@Override
	public JTable createTable() {
		// create object of table and table model
		table = new JTable();

		dtm = new DefaultTableModel(0, 0);

		Object[] rowNamesArray = JSONLanguageManager.getLanguageProperty().getTable().getPlayerHeadersNamesAsList().toArray();
		dtm.setColumnIdentifiers(rowNamesArray);

		table.setModel(dtm);

		return table;
	}

	@Override
	protected void addMouseListenerToTable() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void addMouseListenerToTableHeader() {
		// TODO Auto-generated method stub

	}

//	@Override
//	public void addNewRow(IModel model) {
//		// TODO Auto-generated method stub
//
//	}

	@Override
	public void updateRow(Player user) {
		// TODO Auto-generated method stub

	}

}
