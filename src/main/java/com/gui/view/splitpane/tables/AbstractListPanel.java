package com.gui.view.splitpane.tables;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.apache.log4j.Logger;

import com.backend.controller.contextmenu.CreateTeamController;
import com.backend.controller.contextmenu.DeletePlayerController;
import com.backend.controller.contextmenu.EdtiPlayerController;
import com.backend.controller.contextmenu.NewPlayerController;
import com.backend.controller.contextmenu.RemoveFromTeamController;
import com.backend.controller.table.ScrollPaneController;
import com.backend.jsonmapper.JSONLanguageManager;
import com.backend.pojos.Player;
import com.backend.utils.ImageLoader;
import com.gui.view.MainFrame;
import com.gui.view.splitpane.infopage.InfoPane;

public abstract class AbstractListPanel extends JPanel {
	private static final long			serialVersionUID	= 1L;

	private final Logger				logger				= Logger.getLogger(this.getClass());

	protected DefaultTableModel			dtm;
	protected MainFrame					mainFrame;
	protected Object[]					tableData;
	protected JTable					table;
	public static JPopupMenu			contextMenu;

	private JScrollPane					sc;
	private Box							contendHBox;
	private Box							buttonVBox;
	private Box							hBox;
	// Table Sorter
	private TableRowSorter<TableModel>	sorter;
	private JLabel						lblSearch;
	// Buttons
	private JButton						addButton;
	private JButton						deleteButton;
	// TextField
	private JTextField					filterText;

	// Table PopUp
	private JPopupMenu					popupMenu;
	public static JMenu					toTeamSub;

	protected List<JMenuItem>			teamContextMenu;

	public AbstractListPanel(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
		this.contendHBox = Box.createVerticalBox();
		this.buttonVBox = Box.createVerticalBox();
		this.hBox = Box.createHorizontalBox();

		AbstractListPanel.contextMenu = createContextMenu();
		this.addButton = new JButton(JSONLanguageManager.getLanguageProperty().getContext().getAddplayer());
		this.deleteButton = new JButton(JSONLanguageManager.getLanguageProperty().getContext().getDeleteplayer());
		this.lblSearch = new JLabel("Suchen: ");
		this.popupMenu = createContextMenuScrollPane();
		this.setLayout(new GridBagLayout());
		
		this.teamContextMenu = new ArrayList<JMenuItem>();

		prepareTable();

		addMouseListenerToTableHeader();
		addMouseListenerToScrollPane();
		// Table Layout
		int tableHeight = contendHBox.getWidth();
		int tableWidth = contendHBox.getHeight();
		table.setSize(tableHeight, tableWidth);
		addMouseListenerToTable();

		filterText = new JTextField();
		filterText.setMaximumSize(new Dimension(Integer.MAX_VALUE, filterText.getPreferredSize().height));
		this.addComponents();
	}

	private void prepareTable() {
		table = createTable();
		sorter = new TableRowSorter<TableModel>(table.getModel());
		table.setRowSorter(sorter);
		sc = new JScrollPane(table);
		initPlayerToTeamMenu();
	}

	public AbstractListPanel(MainFrame mainFrame, List<String> rowNames, final InfoPane contendSplitPane) {
		this.mainFrame = mainFrame;
		this.contendHBox = Box.createVerticalBox();
		this.buttonVBox = Box.createVerticalBox();
		this.hBox = Box.createHorizontalBox();
		this.setLayout(new GridBagLayout());
		AbstractListPanel.contextMenu = createContextMenu();
		
		this.addButton = new JButton("Add Player");
		this.deleteButton = new JButton("Delete Player");
		this.lblSearch = new JLabel("Suchen: ");
		this.popupMenu = createContextMenuScrollPane();

		addMouseListenerToTableHeader();
		addMouseListenerToScrollPane();

		// Table Layout
		int heigt = contendHBox.getWidth();
		int width = contendHBox.getHeight();
		table.setSize(heigt, width);
		addMouseListenerToTable();

		// **************** Text Search ****************
		filterText = new JTextField();
		filterText.setMaximumSize(new Dimension(Integer.MAX_VALUE, filterText.getPreferredSize().height));
		
		// TODO: Add getter, to remove the add of Listener in GUI
		filterText.getDocument().addDocumentListener(getFilterTextListener());
		
		// **************** Text Search End ****************
		addComponents();
		addMouseListenerToTable();
		this.teamContextMenu = new ArrayList<JMenuItem>();
	}

	private DocumentListener getFilterTextListener() {
		return new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				fireSearchBarChanges();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				fireSearchBarChanges();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				logger.debug("Value has Changed");
			}
		};
	}

	//TODO: Add getter, to remove the add of Listener in GUI
	protected abstract void addMouseListenerToTable();

	private void fireSearchBarChanges() {
		String text = filterText.getText();
		if (text.trim().length() == 0) {
			sorter.setRowFilter(null);
		} else {
			sorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
		}
		table.repaint();
	}

	// TODO: Add getter, to remove the add of Listener in GUI
	protected abstract void addMouseListenerToTableHeader();

	// TODO: Add getter, to remove the add of Listener in GUI
	private void addMouseListenerToScrollPane() {
		// TODO: Add getter, to remove the add of Listener in GUI
		sc.addMouseListener(new ScrollPaneController(popupMenu));
	}

	private GridBagConstraints getGridBagLayout() {
		GridBagConstraints c = new GridBagConstraints();
		// Layout

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(10, 10, 10, 10);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;
		return c;
	}

	public abstract void updateRow(Player user);

	private void addComponents() {
		buttonVBox.add(addButton);
		buttonVBox.add(deleteButton);

		hBox.add(lblSearch);
		hBox.add(filterText);

		contendHBox.add(hBox);
		contendHBox.add(sc, getGridBagLayout());
		this.add(contendHBox, getGridBagLayout());
	}

	public abstract JTable createTable();

	public JPopupMenu createContextMenu() {
		// MenuItem for Context Menu
		JMenuItem newPlayer = new JMenuItem(JSONLanguageManager.getLanguageProperty().getContext().getAddplayer());
		newPlayer.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "adduser"));

		final JMenu addPlayer = new JMenu(JSONLanguageManager.getLanguageProperty().getContext().getAddplayerto());
		addPlayer.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "add"));
		// submenu
		toTeamSub = new JMenu(JSONLanguageManager.getLanguageProperty().getContext().getToteam());
		toTeamSub.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "addtogroup"));

		addPlayer.add(toTeamSub);

		JMenuItem removePlayerfromTeam = new JMenuItem(JSONLanguageManager.getLanguageProperty().getContext().getRemovefromteam());
		removePlayerfromTeam.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "removeuser"));
		JMenuItem editPlayer = new JMenuItem(JSONLanguageManager.getLanguageProperty().getContext().getEditplayer());
		editPlayer.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "edituser"));
		JMenuItem deletePlayer = new JMenuItem(JSONLanguageManager.getLanguageProperty().getContext().getDeleteplayer());
		deletePlayer.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "delete"));
		JMenuItem newTeamSub = new JMenuItem(JSONLanguageManager.getLanguageProperty().getContext().getNewteam());
		newTeamSub.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "addgroup"));
		
		// TODO Add getter, to remove the add of Listener in GUI
		newTeamSub.addActionListener(new CreateTeamController(mainFrame, toTeamSub));
		
		addPlayer.add(newTeamSub);

		
		// New Player
		// TODO: Add getter, to remove the add of Listener in GUI
		newPlayer.addActionListener(new NewPlayerController());
		// remove Player from Team
		// TODO: Add getter, to remove the add of Listener in GUI
		removePlayerfromTeam.addActionListener(new RemoveFromTeamController());
		// Edit Player
		// TODO: Add getter, to remove the add of Listener in GUI
		editPlayer.addActionListener(new EdtiPlayerController());
		// delete Player
		// TODO: Add getter, to remove the add of Listener in GUI
		deletePlayer.addActionListener(new DeletePlayerController());

		contextMenu = new JPopupMenu();
		contextMenu.add(newPlayer);
		contextMenu.add(addPlayer);
		contextMenu.add(editPlayer);
		contextMenu.add(removePlayerfromTeam);
		contextMenu.add(deletePlayer);
		return contextMenu;
	}

	protected void initPlayerToTeamMenu() {
		contextMenu.revalidate();
		contextMenu.repaint();
	}

	public JPopupMenu createContextMenuScrollPane() {
		JMenuItem newPlayer = new JMenuItem(JSONLanguageManager.getLanguageProperty().getContext().getAddplayer());
		newPlayer.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "adduser"));
		// TODO: Add getter, to remove the add of Listener in GUI
		newPlayer.addActionListener(new NewPlayerController());

		JMenuItem test = new JMenuItem("Test");
		test.setActionCommand("MENUITEM1");

		popupMenu = new JPopupMenu();
		popupMenu.add(newPlayer);
		popupMenu.add(test);
		return popupMenu;
	}
}
