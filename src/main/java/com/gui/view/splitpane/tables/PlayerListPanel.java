package com.gui.view.splitpane.tables;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;

import com.backend.controller.table.TableController;
import com.backend.jsonmapper.JSONLanguageManager;
import com.backend.jsonmapper.JSONProperties;
import com.backend.models.ModelsHandler;
import com.backend.pojos.Player;
import com.backend.pojos.TableModelHandler;
import com.gui.view.MainFrame;
import com.gui.view.splitpane.infopage.InfoPane;
import com.gui.view.splitpane.tables.model.XTableColumnModel;

public class PlayerListPanel extends AbstractListPanel {
	private static final long		serialVersionUID	= 1L;

	private List<JCheckBoxMenuItem>	menuItems;
	private XTableColumnModel		columnModel;
	private final static Logger		logger				= Logger.getLogger(PlayerListPanel.class);

	public PlayerListPanel(MainFrame mainFrame, InfoPane contendSplitPane) {
		super(mainFrame);

		menuItems = new ArrayList<>();

		Map<String, String> headerNamesMap = JSONLanguageManager.getLanguageProperty().getTable().getPlayerHeaderLabels();

		List<String> headerNames = new ArrayList<String>(headerNamesMap.values());

		Map<String, Boolean> headerMap = JSONProperties.getJSonProperty().getTable().getPlayerHeader();

		int columnCount = 0;
		columnCount = 0;
		for (Map.Entry<String, Boolean> entry : headerMap.entrySet()) {
			Boolean showCol = entry.getValue();
			String rowName = headerNames.get(columnCount);
			TableColumn column = columnModel.getColumnByModelIndex(columnCount);
			columnModel.setColumnVisible(column, showCol);

			final JCheckBoxMenuItem boxMenuItem = new JCheckBoxMenuItem(rowName);
			boxMenuItem.setSelected(showCol);
			boxMenuItem.setActionCommand(rowName);
			logger.debug("BoxName -> " + boxMenuItem.getText());

			boxMenuItem.addActionListener(new ActionListener() {
				int currentIndex = headerNames.indexOf(boxMenuItem.getActionCommand());

				@Override
				public void actionPerformed(ActionEvent e) {
					TableColumn column = columnModel.getColumnByModelIndex(currentIndex);
					boolean visible = columnModel.isColumnVisible(column);
					logger.debug("Column '" + column.getHeaderValue().toString() + "' is Visible -> " + !visible);
					columnModel.setColumnVisible(column, !visible);
				}
			});
			menuItems.add(boxMenuItem);
			columnCount++;
		}
	}

	@Override
	public void updateRow(Player user) {
		logger.debug("Update ROW : " + user.getId());
		Object[] data = new Object[] { user.getId(), user.getFirstName(), user.getLastName(), user.getStreet(), user.getHouseNumber(), user.getZipCode(), user.getCity(), user.isPaid(), user.getEMail(), user.getMobilePhoneNumber(), user.getPhone(), user.getPointsSeriesOne(), user.getPointsSeriesTwo(), user.getTotalScore() };

		logger.debug("datasize == " + data.length);

		for (int i = 0; i < dtm.getRowCount(); i++) {
			if (dtm.getValueAt(i, 0) == user.getId()) {
				TableController.getInstance().onUpdatePlayerRow(user);
				for (int j = 0; j < data.length; j++) {
					dtm.setValueAt(data[j], i, j);
				}
			}
		}

	}

	@Override
	public JTable createTable() {
		columnModel = new XTableColumnModel();

		table = TableModelHandler.getInstance().getPlayerTable();
		dtm = TableModelHandler.getInstance().getDefaultPlayerTableModel();

		table.setColumnModel(columnModel);
		table.createDefaultColumnsFromModel();

		Object[] rowNamesArray = JSONLanguageManager.getLanguageProperty().getTable().getPlayerHeadersNamesAsList().toArray();
		dtm.setColumnIdentifiers(rowNamesArray);

		return table;
	}

	@Override
	protected void addMouseListenerToTableHeader() {
		table.getTableHeader().addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (JSONProperties.getJSonProperty().getTable().getEnableColumnMenu()) {
					if (SwingUtilities.isRightMouseButton(e)) {
						JPopupMenu m = new JPopupMenu("test");
						for (JCheckBoxMenuItem item : menuItems) {
							m.add(item);
						}
						m.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}
		});
	}

	@Override
	protected void addMouseListenerToTable() {
		table.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
				System.out.println("pressed on table is " + table.getSelectedRow());
				int selectedRowCount = 0;
				int[] selectedRows = null;

				if (e.isPopupTrigger()) {
					ModelsHandler.getInstance().getSelectedPlayerList().removeAll(ModelsHandler.getInstance().getSelectedPlayerList());
					JTable source = (JTable) e.getSource();

					int row = source.rowAtPoint(e.getPoint());
					int column = source.columnAtPoint(e.getPoint());
					selectedRows = source.getSelectedRows();

					TableModelHandler.getInstance().setSelectedRowCount(source.getSelectedRow());
					// selectedPlayer =

					logger.debug("Row: '" + row + "' Col: '" + column + "'");

					contextMenu.show(e.getComponent(), e.getX(), e.getY());
					System.out.println("PlayerDATA = " + ModelsHandler.getInstance().getPlayers().size());

					// contendSplitPane.setPlayerToShow(selectedPlayer);
					selectedRowCount = source.getSelectedRows().length;

					if (selectedRowCount == 1) {
						// selectedPlayer = playerData.get(row);
						ModelsHandler.getInstance().addSelectedPlayer(ModelsHandler.getInstance().getPlayers().get(row));
					} else {
						for (int i = 0; i < selectedRowCount; i++) {
							logger.debug("Add Players: " + ModelsHandler.getInstance().getPlayers().get(selectedRows[i]));
							ModelsHandler.getInstance().addSelectedPlayer(ModelsHandler.getInstance().getPlayers().get(selectedRows[i]));
						}
					}
				}

				contextMenu.pack();
				contextMenu.repaint();
				contextMenu.revalidate();
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {

			}
		});

	}
}
