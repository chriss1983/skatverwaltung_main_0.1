package com.gui.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.WindowConstants;

import org.apache.log4j.Logger;

import com.backend.controller.contextmenu.CreateOrUpdatePlayerController;
import com.backend.controller.models.PlayerListController;
import com.backend.controller.models.TeamListController;
import com.backend.controller.settings.SettingsListenerController;
import com.backend.jsonmapper.JSONProperties;
import com.backend.models.ModelsHandler;
import com.backend.pojos.Player;
import com.backend.utils.connection.OnlineChecker;
import com.backend.utils.connection.OnlineStateChecker;
import com.gui.splashscreen.SplashScreen;
import com.gui.view.inputforms.CreateOrUpdatePlayerView;
import com.gui.view.menubar.Menubar;
import com.gui.view.settings.SettingsFrame;
import com.gui.view.splitpane.infopage.InfoPane;
import com.gui.view.splitpane.tables.GroupListPanel;
import com.gui.view.splitpane.tables.PlayerListPanel;
import com.gui.view.splitpane.tables.PointsListPanel;

public class MainFrame extends JFrame implements OnlineChecker {
	private final static Logger			logger				= Logger.getLogger(MainFrame.class);

	private static final long			serialVersionUID	= 1L;
	private CardLayout					cardLayout			= new CardLayout();
	private JPanel						framePanel;
	private InfoPane					infoPane;
	private JLabel						infoText;
	private Boolean						isActivated			= false;

	private PlayerListPanel				playerListPanel;

	private SettingsFrame				settingsFrame;
	private SplashScreen				splashScreen;
	private JPanel						tableMainPanel		= new JPanel();
	private Map<String, List<String>>	tableRowNames;
	
	private JButton testButton;

	// TODO: implement Cardlayout and replace it with infoPane

	public MainFrame(SplashScreen splashScreen) {
		setAppIcon();
		this.splashScreen = splashScreen;
		this.settingsFrame = new SettingsFrame(this);

		logger.debug("init MainFrame");
		splashScreen.showSplash();
		splashScreen.updateProgressBarText("Loading UI");
		splashScreen.updateProgressBar(12);

		OnlineStateChecker onChecker = new OnlineStateChecker();
		onChecker.addOnlineListener(this);
		infoText = new JLabel();

	}

	private void addComponents() {
		logger.info("Add Components to Splitpane");
		splashScreen.showSplash();
		splashScreen.updateProgressBarText("Load database");
		splashScreen.updateProgressBar(25);

		this.playerListPanel = new PlayerListPanel(this, infoPane);
		GroupListPanel groupListPanel = new GroupListPanel(this);
		PointsListPanel pointsListPanel = new PointsListPanel(this, infoPane);

		addListenerToTeamList();
		loadDataFromDB();

		this.tableMainPanel.setLayout(cardLayout);

		tableMainPanel.add(groupListPanel, "0");
		tableMainPanel.add(pointsListPanel, "1");

		CreateOrUpdatePlayerController addPlayerListenerController = new CreateOrUpdatePlayerController();

		CreateOrUpdatePlayerView.getInstance().addActionListener(addPlayerListenerController);

		SettingsListenerController settingsListenerController = new SettingsListenerController(this.settingsFrame);

		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, ListPanel.getInstance(playerListPanel, groupListPanel, pointsListPanel, tableRowNames), groupListPanel);

		splitPane.setResizeWeight(0.5);
		splitPane.setOneTouchExpandable(true);
		splitPane.setContinuousLayout(true);

		Menubar menubar = new Menubar();
		menubar.getMenuItemFileNewUser().addActionListener(addPlayerListenerController);
		menubar.getMenuItemFileSettings().addActionListener(settingsListenerController);

		JPanel infoArea = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.insets = new Insets(2, 2, 2, 2);

		JLabel infoLabel = new JLabel("Online state: ");

		gbc.gridx = 0; // x-Position im gedachten Gitter
		gbc.gridy = 1; // y-Position im gedachten Gitter
		gbc.gridheight = 2; // zwei Gitter-Felder hoch
		infoArea.add(infoLabel, gbc);
		gbc.gridx = 2; // x-Position im gedachten Gitter
		gbc.gridy = 1; // y-Position im gedachten Gitter
		gbc.gridheight = 2; // zwei Gitter-Felder hoch

		Box buttonBox = Box.createHorizontalBox();

		buttonBox.add(Box.createHorizontalStrut(5));
		buttonBox.add(infoLabel);
		buttonBox.add(Box.createHorizontalStrut(5));
		buttonBox.add(infoText);

		testButton = new JButton("Add testPlayer");
		buttonBox.add(Box.createHorizontalGlue());
		
		buttonBox.add(testButton);
		testButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				createTestPlayer();
				
			}
		});
		
		
		buttonBox.add(Box.createHorizontalStrut(5));
		buttonBox.add(new JButton("Test Button"));
		buttonBox.add(Box.createHorizontalStrut(5));

		framePanel.add(menubar, BorderLayout.NORTH);
		framePanel.add(splitPane, BorderLayout.CENTER);
		framePanel.add(buttonBox, BorderLayout.SOUTH);

		this.getContentPane().add(framePanel);
		splashScreen.updateProgressBar(80);
	}
	
	private void createTestPlayer() {
		Player player = new Player();
		
		player.setId(55);
		player.setFirstName("Peter Maier");
		player.setLastName("Maier");
		player.setStreet("MusterStr");
		player.setHouseNumber("55");
		player.setZipCode("45251");
		player.setCity("Musterstadt");
		
		ModelsHandler.getInstance().addPlayer(player);
		
	}

	private void addListenerToTeamList() {
		ModelsHandler.getInstance().addMapChangeListenerToPlayer(new PlayerListController());
		ModelsHandler.getInstance().addMapChangeListenerToTeam(new TeamListController());
	}

	public boolean getActivation() {
		return isActivated;
	}

	public InfoPane getContentSplitPane() {
		return this.infoPane;
	}

	public PlayerListPanel getPlayerListPanel() {
		return playerListPanel;
	}

	private void loadDataFromDB() {
//		ModelsHandler.getInstance().addPlayerList(RedeyedCoreManager.getDataBaseUtil().getAllEntities(Player.class, null, null, null, null));
//		ModelsHandler.getInstance().addTeamList(RedeyedCoreManager.getDataBaseUtil().getAllEntities(Team.class, null, null, null, null));
	}

	@Override
	public void onlineListener(String state) {
		this.infoText.setText(state);
	}

	public void setActivation(boolean activationState) {
		this.isActivated = activationState;
	}

	private void setAppIcon() {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(MainFrame.class.getResource("/Java-icon.png")));
	}

	public void showDialog() {
		JDialog dialog = new JDialog(this, Dialog.ModalityType.APPLICATION_MODAL);
		dialog.setBounds(350, 350, 200, 200);
		dialog.setVisible(true);
	}

	public void showFrame() {
		splashScreen.updateProgressBarText("add components");
		splashScreen.updateProgressBar(25);
		logger.info("Init MainFrame");
		Box.createHorizontalBox();
		Box.createVerticalBox();
		Box.createVerticalBox();
		this.infoPane = new InfoPane();

		splashScreen.updateProgressBar(20);
		if (JSONProperties.getJSonProperty().getGlobal().getFullScreen()) {
			GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
			this.setMaximizedBounds(env.getMaximumWindowBounds());
			this.setExtendedState(this.getExtendedState() | Frame.MAXIMIZED_BOTH);
		}
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		framePanel = new JPanel(new BorderLayout());

		addComponents();
		this.setVisible(true);
		splashScreen.updateProgressBar(100);
		splashScreen.hideSplash();

	}

	@SuppressWarnings("unused")
	private void test() {
		int i = 0;
		CardLayout cardLayout = (CardLayout) tableMainPanel.getLayout();
		System.out.println("test");
		if (i < 2) {
			i = 0;
		}
		cardLayout.show(tableMainPanel, i++ + "");
	}
}
