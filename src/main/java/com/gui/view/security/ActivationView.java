package com.gui.view.security;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import com.backend.jsonmapper.JSONLanguageManager;
import com.gui.view.splitpane.infopage.GBHelper;
import com.gui.view.splitpane.infopage.Gap;

public class ActivationView extends JFrame implements WindowListener {
	private static final long	serialVersionUID	= 1L;

	private JTextArea			infoText1;
	private JTextField			activationCode;
	private JButton				saveButton;
	private JTextField			firstName;
	private JTextField			lastName;

	public ActivationView() {
		initGUI();

	}

	private void initGUI() {
		super.setTitle("Activation");
		// this.setSize(new Dimension);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);

		infoText1 = new JTextArea(JSONLanguageManager.getLanguageProperty().getActivator().getInfotext());
		infoText1.setLineWrap(true);
		infoText1.setWrapStyleWord(true);
		infoText1.setOpaque(false);
		infoText1.setEditable(false);
		infoText1.setFont(new Font("Calibri", Font.BOLD, 12));

		activationCode = new JTextField(10);
		saveButton = new JButton(JSONLanguageManager.getLanguageProperty().getActivator().getButtonSave());
		firstName = new JTextField(10);
		lastName = new JTextField(40);

		addWindowListener(this);

		this.add(getContend());
		this.setSize(new Dimension(300, 175));
		// this.setResizable(false);
		this.setAlwaysOnTop(true);
	}

	private Component getContend() {
		final int GAP = 2;
		GBHelper pos = new GBHelper();
		JPanel content = new JPanel(new GridBagLayout());
		content.add(infoText1, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(new Gap(GAP), pos.nextRow());
		content.add(firstName, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(lastName, pos);
		content.add(new Gap(GAP * 2), pos.nextRow());
		content.add(activationCode, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(Box.createVerticalStrut(20), pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(saveButton, pos);

		return content;
	}

	public String getActivationCode() {
		return activationCode.getText();
	}

	public void addActionListener(ActionListener listener) {
		saveButton.addActionListener(listener);
	}

	public static void main(String[] args) {
		new ActivationView().setVisible(true);
	}

	public void showOrHide(String val) {
		if (val.equals("show")) {
			this.setVisible(true);
		} else if (val.equals("hide")) {
			this.dispose();
		}
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		System.exit(0);

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

}
