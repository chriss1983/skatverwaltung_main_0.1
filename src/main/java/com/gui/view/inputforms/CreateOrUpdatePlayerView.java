package com.gui.view.inputforms;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import com.backend.pojos.Player;

@SuppressWarnings("serial")
public class CreateOrUpdatePlayerView extends JFrame {

	private static CreateOrUpdatePlayerView	instance	= new CreateOrUpdatePlayerView();

	private JComboBox<String>				combobox;
	private JButton							saveButton, cancelButton;
	private JPanel							panel, formPanel, buttonPanel;

	private JLabel							lbTitle, lbFirstName, lbLastName, lbAdress, lbHouseNr, lbZip, lbCity, lbEmail, lbMobil, lbPhone,
			lbInfos, lbDuty, lbPaid;
	private JTextField						tfFirstName, tfLastName, tfAdress, tfHouseNr, tfZip, tfCity, tfEmail, tfMobile, tfPhone, tbStartNumber;

	private JCheckBox						cbPaid;
	private TextArea						taInfo;

	private Player							player;

	private CreateOrUpdatePlayerView() {
		this.setSize(300, 300);
		setTitle("Neuer Spieler");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.createUI();
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

	}

	private void createUI() {
		/**
		 * Erstellen der TextFields und setzen der Gr��e
		 */
		tbStartNumber = new JTextField();
		tbStartNumber.setColumns(10);

		tfFirstName = new JTextField();
		tfFirstName.setColumns(10);

		tfLastName = new JTextField();
		tfLastName.setColumns(10);

		tfAdress = new JTextField();
		tfAdress.setColumns(10);

		tfHouseNr = new JTextField();
		tfHouseNr.setColumns(10);

		tfZip = new JTextField();
		tfZip.setColumns(10);

		tfCity = new JTextField();
		tfCity.setColumns(10);

		tfEmail = new JTextField();
		tfEmail.setColumns(10);

		tfMobile = new JTextField();
		tfMobile.setColumns(10);

		tfPhone = new JTextField();
		tfPhone.setColumns(10);

		cbPaid = new JCheckBox();

		/**
		 * TextArea für Infos erstellen
		 */
		taInfo = new TextArea(5, 20);
		taInfo.setSize(this.getHeight(), this.getWidth());

		/**
		 * Erstellen der Buttons
		 */
		saveButton = new JButton("Speichern");
		saveButton.setActionCommand("save");

		cancelButton = new JButton("Schließen");

		/**
		 * Erstellen des Labels
		 */
		Font font = new Font("Calibri", Font.BOLD, 15);
		lbTitle = new JLabel("Bitte vervollständigen Sie die Daten\n");
		lbTitle.setFont(font);

		new JLabel("Startnummer");
		lbFirstName = new JLabel("Vorname*");
		lbLastName = new JLabel("Nachname*");
		lbAdress = new JLabel("Strasse*");
		lbHouseNr = new JLabel("Hausnummer* ");
		lbZip = new JLabel("Postleitzahl*");
		lbCity = new JLabel("Ort*");
		lbEmail = new JLabel("E-Mail Adresse*");
		lbMobil = new JLabel("Handy Nummer");
		lbPhone = new JLabel("Telefon");
		lbInfos = new JLabel("Zusätzliche Informationen");
		lbDuty = new JLabel("* = Pflichtangaben!");
		lbPaid = new JLabel("Beitrag bezahlt");

		combobox = new JComboBox<String>();

		panel = new JPanel(new BorderLayout());
		formPanel = new JPanel(new GridBagLayout());
		buttonPanel = new JPanel();

		// buttonPanel.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();

		/**
		 * Ausrichter und Hinzufügen der Buttons zum JLabel
		 */
		c.insets = new Insets(2, 2, 2, 2); // 5, 5, 9, 5
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 0.5;

		formPanel.add(lbLastName, c);

		c.gridx = 1;
		c.gridy = 0;
		formPanel.add(tfLastName, c);

		c.gridx = 2;
		c.gridy = 0;
		formPanel.add(lbFirstName, c);

		c.gridx = 3;
		c.gridy = 0;
		formPanel.add(tfFirstName, c);

		c.gridx = 0;
		c.gridy = 1;
		formPanel.add(lbAdress, c);

		c.gridx = 1;
		c.gridy = 1;
		formPanel.add(tfAdress, c);

		c.gridx = 2;
		c.gridy = 1;
		formPanel.add(lbHouseNr, c);

		c.gridx = 3;
		c.gridy = 1;
		formPanel.add(tfHouseNr, c);

		c.gridx = 0;
		c.gridy = 2;
		formPanel.add(lbZip, c);

		c.gridx = 1;
		c.gridy = 2;
		formPanel.add(tfZip, c);

		c.gridx = 2;
		c.gridy = 2;
		formPanel.add(lbCity, c);

		c.gridx = 3;
		c.gridy = 2;
		formPanel.add(tfCity, c);

		c.gridx = 0;
		c.gridy = 3;
		formPanel.add(lbEmail, c);

		c.gridx = 1;
		c.gridy = 3;
		formPanel.add(tfEmail, c);

		c.gridx = 2;
		c.gridy = 3;
		formPanel.add(lbPaid, c);

		c.gridx = 3;
		c.gridy = 3;
		formPanel.add(cbPaid, c);

		c.gridx = 0;
		c.gridy = 4;
		formPanel.add(lbMobil, c);

		c.gridx = 1;
		c.gridy = 4;
		formPanel.add(tfMobile, c);

		c.gridx = 2;
		c.gridy = 4;
		formPanel.add(lbPhone, c);

		c.gridx = 3;
		c.gridy = 4;
		formPanel.add(tfPhone, c);

		c.gridx = 3;
		c.gridy = 5;
		formPanel.add(lbDuty, c);

		c.gridx = 0;
		c.gridy = 7;
		formPanel.add(lbInfos, c);

		c.gridx = 0;
		c.gridy = 8;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = formPanel.getWidth();
		formPanel.add(taInfo, c);

		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

		panel.add(combobox);

		buttonPanel.add(saveButton);

		buttonPanel.add(cancelButton);

		panel.add(lbTitle, BorderLayout.NORTH);
		panel.add(formPanel, BorderLayout.CENTER);
		panel.add(buttonPanel, BorderLayout.SOUTH);

		getContentPane().add(panel, BorderLayout.CENTER);

		/**
		 * Fenster auf die Kleinstmögliche Position bringen
		 */
		pack();
	}

	public void open() {
		this.setVisible(true);
	}

	public void close() {
		this.dispose();
	}

	public void addActionListener(ActionListener a) {
		this.saveButton.addActionListener(a);
	}

	/**
	 * @return the tfVorname
	 */
	public String getTfFirstName() {
		return tfFirstName.getText();
	}

	/**
	 * @return the tfNachname
	 */
	public String getTfLastName() {
		return tfLastName.getText();
	}

	/**
	 * @return the tfAdresse
	 */
	public String getTfAdress() {
		return tfAdress.getText();
	}

	/**
	 * @return the tfHausNr
	 */
	public String getTfHouseNr() {
		return tfHouseNr.getText();
	}

	/**
	 * @return the tfPlz
	 */
	public String getTfZip() {
		return tfZip.getText();
	}

	/**
	 * @return the tfOrt
	 */
	public String getTfCity() {
		return tfCity.getText();
	}

	/**
	 * @return the tfEmail
	 */
	public String getTfEmail() {
		return tfEmail.getText();
	}

	/**
	 * @return the tfMobile
	 */
	public String getTfMobile() {
		return tfMobile.getText();
	}

	/**
	 * @return the tfTelefon
	 */
	public String getTfPhone() {
		return tfPhone.getText();
	}

	/**
	 * @return the cbPaid
	 */
	public Boolean getCbPaid() {
		return cbPaid.isSelected();
	}

	/**
	 * @return the taInfo
	 */
	public String getTaInfo() {
		return taInfo.getText();
	}

	public void cleanFields() {
		tfFirstName.setText("");
		tfLastName.setText("");
		tfAdress.setText("");
		tfHouseNr.setText("");
		tfZip.setText("");
		tfCity.setText("");
		tfEmail.setText("");
		tfMobile.setText("");
		tfPhone.setText("");
		tbStartNumber.setText("");
		cbPaid.setSelected(false);
		taInfo.setText("");
	}

	public void editPlayer(Player player) {
		System.out.println("Open Edit dialog with Player : " + player.toString());

		this.player = player;
		cleanFields();
		// tbStartNumber.setText(player.getId() + "");
		tfFirstName.setText(player.getFirstName());
		tfLastName.setText(player.getLastName());
		tfAdress.setText(player.getStreet());
		tfHouseNr.setText(player.getHouseNumber());
		tfZip.setText(player.getZipCode());
		tfCity.setText(player.getCity());
		tfEmail.setText(player.getEMail());
		tfMobile.setText(player.getMobilePhoneNumber());
		tfPhone.setText(player.getPhone());
		cbPaid.setSelected(player.isPaid());
		taInfo.setText(player.getPlayerInfo());

		open();
	}

	public Player getPlayer() {
		Player player = new Player();

		player.setFirstName(getTfFirstName());
		player.setLastName(getTfLastName());
		player.setStreet(getTfAdress());
		player.setHouseNumber(getTfHouseNr());
		player.setZipCode(getTfZip());
		player.setCity(getTfCity());
		player.setMobilePhoneNumber(getTfMobile());
		player.setPhone(getTfPhone());
		player.setEMail(getTfEmail());
		player.setPaid(getCbPaid());
		player.setPlayerInfo(getTaInfo());

		return player;
	}

	public Integer getStartNumber() {
		return player.getId();
	}

	public void setButtonAction(String cmd) {
		if (cmd.equals("update")) {
			saveButton.setText("Update");
		} else if (cmd.equals("save")) {
			saveButton.setText("Save");
		}
		this.saveButton.setActionCommand(cmd);
	}

	public static CreateOrUpdatePlayerView getInstance() {
		return instance;
	}
}
