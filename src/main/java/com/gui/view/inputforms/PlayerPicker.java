package com.gui.view.inputforms;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import com.backend.pojos.Player;
import com.backend.pojos.Team;
import com.gui.view.MainFrame;

public class PlayerPicker extends JDialog {
	private static final long			serialVersionUID	= 1L;
	private DefaultListModel<JCheckBox>	playerModel;
	private DefaultListModel<JCheckBox>	teamModel;

	private JCheckBoxList				playerCheckBoxList;
	private JScrollPane					playerScrollPane;

	private JCheckBoxList				teamCheckBoxList;

	private JButton						addToTeam;
	private JButton						removeFromTeam;

	JCheckBox							check1				= new JCheckBox("CheckBox1");
	JCheckBox							check2				= new JCheckBox("CheckBox2");
	JCheckBox							check3				= new JCheckBox("CheckBox3");
	JCheckBox							check4				= new JCheckBox("CheckBox4");
	JCheckBox							check5				= new JCheckBox("CheckBox5");
	JCheckBox							check6				= new JCheckBox("CheckBox6");
	JCheckBox							check7				= new JCheckBox("CheckBox7");
	JCheckBox							check8				= new JCheckBox("CheckBox8");

	public PlayerPicker(MainFrame mainFrame) {

		// super(mainFrame, Dialog.ModalityType.APPLICATION_MODAL);
		this.setTitle("Player Picker");
		this.setSize(new Dimension(300, 200));
		this.setAlwaysOnTop(true);
		this.requestFocus(true);
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		addToTeam = new JButton(">");
		removeFromTeam = new JButton("<");

		playerModel = new DefaultListModel<JCheckBox>();
		playerCheckBoxList = new JCheckBoxList(playerModel);
		this.playerScrollPane = new JScrollPane(playerCheckBoxList);

		teamModel = new DefaultListModel<JCheckBox>();
		teamCheckBoxList = new JCheckBoxList(teamModel);

		Box vButtonBox = Box.createVerticalBox();
		Box hButtonBox = Box.createHorizontalBox();

		vButtonBox.add(Box.createVerticalGlue());
		vButtonBox.add(addToTeam);
		vButtonBox.add(Box.createVerticalStrut(5));
		vButtonBox.add(removeFromTeam);
		vButtonBox.add(Box.createVerticalGlue());

		hButtonBox.add(Box.createVerticalStrut(5));
		hButtonBox.add(vButtonBox);

		// playerModel.addElement(check1);
		// playerModel.addElement(check2);
		// playerModel.addElement(check3);
		// playerModel.addElement(check4);
		// playerModel.addElement(check5);
		// playerModel.addElement(check6);
		// playerModel.addElement(check7);
		// playerModel.addElement(check8);

		Box vPlayerBox = Box.createVerticalBox();
		// vPlayerBox.add(Box.createHorizontalStrut(5));
		vPlayerBox.add(playerCheckBoxList);
		// vPlayerBox.add(Box.createHorizontalGlue());

		Box vTeamBox = Box.createVerticalBox();
		// vTeamBox.add(Box.createHorizontalStrut(100));
		vTeamBox.add(playerScrollPane);
		// vTeamBox.add(Box.createHorizontalGlue());

		this.add(playerCheckBoxList, BorderLayout.WEST);
		this.add(hButtonBox, BorderLayout.CENTER);
		this.add(teamCheckBoxList, BorderLayout.EAST);

		addToTeam.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (check1.isSelected()) {
					teamModel.addElement(check1);
					playerModel.removeElement(check1);
				}
				if (check2.isSelected()) {
					teamModel.addElement(check2);
					playerModel.removeElement(check2);
				}
				if (check3.isSelected()) {
					teamModel.addElement(check3);
					playerModel.removeElement(check3);
				}
				if (check4.isSelected()) {
					teamModel.addElement(check4);
					playerModel.removeElement(check4);
				}
				if (check5.isSelected()) {
					teamModel.addElement(check5);
					playerModel.removeElement(check5);
				}
				if (check6.isSelected()) {
					teamModel.addElement(check6);
					playerModel.removeElement(check6);
				}
				if (check7.isSelected()) {
					teamModel.addElement(check6);
					playerModel.removeElement(check7);
				}
				if (check7.isSelected()) {
					teamModel.addElement(check8);
					playerModel.removeElement(check8);
				}

			}

		});
		removeFromTeam.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (check1.isSelected()) {
					teamModel.removeElement(check1);
					playerModel.addElement(check1);
				}
				if (check2.isSelected()) {
					teamModel.removeElement(check2);
					playerModel.addElement(check2);
				}
				if (check3.isSelected()) {
					teamModel.removeElement(check3);
					playerModel.addElement(check3);
				}
				if (check4.isSelected()) {
					teamModel.removeElement(check4);
					playerModel.addElement(check4);
				}
				if (check5.isSelected()) {
					teamModel.removeElement(check5);
					playerModel.addElement(check5);
				}
				if (check6.isSelected()) {
					teamModel.removeElement(check6);
					playerModel.addElement(check6);
				}
				if (check7.isSelected()) {
					teamModel.removeElement(check7);
					playerModel.addElement(check7);
				}
				if (check7.isSelected()) {
					teamModel.removeElement(check8);
					playerModel.addElement(check8);
				}

			}

		});

	}

	public void open() {
		this.setVisible(true);
	}

	public void close() {
		this.dispose();
	}

	public void setTeams(List<Team> teams) {
		for (Team team : teams) {

			// Team tmp = (Team) team;

			final JCheckBox checkbox = new JCheckBox(team.getId() + " " + team.getTeamName());
			checkbox.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					System.out.println(e.getID() == ActionEvent.ACTION_PERFORMED ? "ACTION_PERFORMED" : e.getID());
				}
			});
			checkbox.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					System.out.println(e.getStateChange() == ItemEvent.SELECTED ? "SELECTED" : "DESELECTED");
				}
			});

			checkbox.addKeyListener(new KeyListener() {

				@Override
				public void keyTyped(KeyEvent e) {
				}

				@Override
				public void keyReleased(KeyEvent e) {
				}

				@Override
				public void keyPressed(KeyEvent e) {
					System.out.println("Space selected");
					if (e.getKeyCode() == KeyEvent.VK_SPACE) {
						if (checkbox.isSelected()) {
							checkbox.setSelected(false);
						} else {
							checkbox.setSelected(true);
						}
					}
				}
			});

			teamModel.addElement(checkbox);
		}
	}

	public void setPlayers(List<Player> players) {
		for (Player player : players) {

			// Team tmp = (Team) team;

			final JCheckBox checkbox = new JCheckBox(player.getId() + " " + player.getFirstName() + " " + player.getLastName());
			checkbox.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					System.out.println(e.getID() == ActionEvent.ACTION_PERFORMED ? "ACTION_PERFORMED" : e.getID());
				}
			});
			checkbox.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					System.out.println(e.getStateChange() == ItemEvent.SELECTED ? "SELECTED" : "DESELECTED");
				}
			});

			checkbox.addKeyListener(new KeyListener() {

				@Override
				public void keyTyped(KeyEvent e) {
				}

				@Override
				public void keyReleased(KeyEvent e) {
				}

				@Override
				public void keyPressed(KeyEvent e) {
					System.out.println("Space selected");
					if (e.getKeyCode() == KeyEvent.VK_SPACE) {
						if (checkbox.isSelected()) {
							checkbox.setSelected(false);
						} else {
							checkbox.setSelected(true);
						}
					}
				}
			});

			playerModel.addElement(checkbox);
		}
	}

	public JScrollPane getCheckbox() {
		return playerScrollPane;
	}

	private static List<Team> getDummyTeam() {
		List<Team> teams = new ArrayList<Team>();
		Team t = new Team();
		t.setTeamName("Team One");
		t.setPointsSeriesOne(464);
		t.setPointsSeriesTwo(464);
		t.setTotalScore(465151);

		Team t2 = new Team();
		t2.setTeamName("Team Two");
		t2.setPointsSeriesOne(464);
		t2.setPointsSeriesTwo(464);
		t2.setTotalScore(465151);

		teams.add(t);
		teams.add(t2);

		return teams;
	}

	private static List<Player> getDummyPlayer() {
		List<Player> players = new ArrayList<Player>();

		Player p = new Player();
		p.setPlayerInfo("0");
		p.setFirstName("Peter");
		p.setLastName("Maier");

		Player p1 = new Player();
		p1.setPlayerInfo("1");
		p1.setFirstName("Ulrich");
		p1.setLastName("Mustermann");

		players.add(p);
		players.add(p1);

		return players;

	}

	public static void main(String[] args) {
		PlayerPicker picker = new PlayerPicker(null);
		picker.setTeams(getDummyTeam());
		picker.setPlayers(getDummyPlayer());
		picker.open();
	}
}
