package com.gui.view.menubar;

import java.awt.FlowLayout;

import javax.swing.Box;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import com.backend.jsonmapper.JSONLanguageManager;
import com.backend.utils.ImageLoader;

public class Menubar extends JPanel {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private JMenuBar			menuBar;
	private JMenu				menuFile, menuFileNew, menuEdit, menuHelp;
	private JMenuItem			menuItemFileNewUser, menuItemFileNewgroup, menuItemFileSave, menuItemFileSettings,
			menuItemFileExit;
	private JMenuItem			menuItemEditCut, menuItemEditCopy, menuItemEditPaste;
	private JMenuItem			menuItemHelpHelp, menuItemAbout;

	public Menubar() {
		// setBackground(Color.red);
		setLayout(new FlowLayout(FlowLayout.LEFT));
		menuBar = new JMenuBar();

		menuFile = new JMenu(JSONLanguageManager.getLanguageProperty().getMenubar().getFileMenu());
		menuEdit = new JMenu(JSONLanguageManager.getLanguageProperty().getMenubar().getEditMenu());

		menuHelp = new JMenu(JSONLanguageManager.getLanguageProperty().getMenubar().getHelpMenu());

		menuBar.add(menuFile);
		menuBar.add(menuEdit);
		menuBar.add(menuHelp);

		menuFileNew = new JMenu(JSONLanguageManager.getLanguageProperty().getMenubar().getFileNew());
		menuFileNew.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "new"));

		menuFile.add(menuFileNew);

		menuItemFileNewUser = new JMenuItem(JSONLanguageManager.getLanguageProperty().getMenubar().getAddUser());
		menuItemFileNewgroup = new JMenuItem(JSONLanguageManager.getLanguageProperty().getMenubar().getAddGroup());

		menuItemFileNewUser.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "adduser"));
		menuItemFileNewgroup.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "addgroup"));

		menuItemFileSave = new JMenuItem(JSONLanguageManager.getLanguageProperty().getMenubar().getSave());
		menuItemFileSettings = new JMenuItem(JSONLanguageManager.getLanguageProperty().getMenubar().getSettings());
		menuItemFileExit = new JMenuItem(JSONLanguageManager.getLanguageProperty().getMenubar().getExit());

		menuItemFileSave.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "save"));
		menuItemFileSettings.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "settings"));
		menuItemFileExit.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "exit"));

		menuFileNew.add(menuItemFileNewUser);
		menuFileNew.add(menuItemFileNewgroup);
		menuFile.add(menuItemFileSave);
		menuFile.addSeparator();
		menuFile.add(menuItemFileSettings);
		menuFile.addSeparator();
		menuFile.add(menuItemFileExit);

		menuItemEditCut = new JMenuItem(JSONLanguageManager.getLanguageProperty().getMenubar().getCut());
		menuItemEditCopy = new JMenuItem(JSONLanguageManager.getLanguageProperty().getMenubar().getCopy());
		menuItemEditPaste = new JMenuItem(JSONLanguageManager.getLanguageProperty().getMenubar().getPaste());

		menuEdit.add(menuItemEditCut);
		menuEdit.add(menuItemEditCopy);
		menuEdit.add(menuItemEditPaste);

		menuItemHelpHelp = new JMenuItem(JSONLanguageManager.getLanguageProperty().getMenubar().getHelp());
		menuItemAbout = new JMenuItem(JSONLanguageManager.getLanguageProperty().getMenubar().getAbout());

		menuItemHelpHelp.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "help"));
		menuItemAbout.setIcon(ImageLoader.getImageIcon(ImageLoader.ImageTyp.MENUBAR, "about"));

		menuHelp.add(menuItemHelpHelp);
		menuHelp.add(menuItemAbout);

		Box hBox = Box.createHorizontalBox();

		hBox.add(menuBar);
		add(hBox);

	}

	public JMenuItem getMenuItemFileNewUser() {
		return menuItemFileNewUser;
	}

	public JMenuItem getMenuItemFileNewGroup() {
		return menuItemFileNewgroup;
	}

	public JMenuItem getMenuItemFileSave() {
		return menuItemFileSave;
	}

	public JMenuItem getMenuItemFileSettings() {
		return menuItemFileSettings;
	}

	public JMenuItem getMenuItemFileExit() {
		return menuItemFileExit;
	}

	public JMenuItem getMenuItemEditCut() {
		return menuItemEditCut;
	}

	public JMenuItem getMenuItemEditCopy() {
		return menuItemEditCopy;
	}

	public JMenuItem getMenuItemEditPaste() {
		return menuItemEditPaste;
	}

	public JMenuItem getMenuItemHelpHelp() {
		return menuItemHelpHelp;
	}

	public JMenuItem getMenuItemAbout() {
		return menuItemAbout;
	}

}
