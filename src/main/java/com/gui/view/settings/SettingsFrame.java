package com.gui.view.settings;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonProcessingException;

import com.backend.jsonmapper.JSONProperties;
import com.gui.dialogs.ExceptionDialog;
import com.gui.utils.ThemeUtils;
import com.gui.view.MainFrame;
import com.gui.view.splitpane.infopage.GBHelper;
import com.gui.view.splitpane.infopage.Gap;

public class SettingsFrame extends JFrame {
	private static final long		serialVersionUID	= -7900843317561365222L;
	final static Logger				logger				= Logger.getLogger(SettingsFrame.class);
	private JTabbedPane				tabbedPane;
	public static String			title				= "Settings";
	private static SettingsFrame	settingsFrame;

	private JPanel					mainSettingsPanel;
	private GlobalSettings			globalSettings;
	private DesignSettings			designSettings;
	private DatabaseSettings		databaseSettings;
	private LanguageSettings		languageSettings;
	private TableSettings			tableSettings;

	public SettingsFrame(MainFrame mainFrame) {
		this.mainSettingsPanel = new JPanel(new GridLayout());
		this.globalSettings = new GlobalSettings();
		this.designSettings = new DesignSettings(mainFrame);
		this.databaseSettings = new DatabaseSettings();
		this.languageSettings = new LanguageSettings(mainFrame);
		this.tableSettings = new TableSettings();
		SettingsFrame.settingsFrame = this;

		Font font = new Font("Calibri", Font.BOLD, 15);

		tabbedPane = new JTabbedPane(SwingConstants.LEFT);
		tabbedPane.add("Global Setting", globalSettings);
		tabbedPane.add("Theme", designSettings);
		tabbedPane.add("Database Settings", databaseSettings);
		tabbedPane.add("Languange Settings", languageSettings);
		tabbedPane.add("Table Settings", tableSettings);

		tabbedPane.setFont(font);

		GridBagConstraints c = new GridBagConstraints();

		c.gridx = 0;
		c.gridy = 0;

		c.insets = new Insets(10, 10, 10, 10);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;

		mainSettingsPanel.add(tabbedPane, c);
		this.getContentPane().add(mainSettingsPanel);
		initSettingsFrame();
	}

	private void initSettingsFrame() {
		super.setTitle(SettingsFrame.title);

		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setSize(new Dimension(600, 300));
	}

	public void showSettings() {
		this.setVisible(true);
	}

	public static void main(String[] args) {

		new SettingsFrame(null).setVisible(true);
		;
	}

	public static void changeTitle(String title) {
		settingsFrame.setTitle(title);
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(2000);
					settingsFrame.setTitle(SettingsFrame.title);
				} catch (InterruptedException e) {
					e.printStackTrace();
					if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
						ExceptionDialog.showException(e);
					}
				}

			}
		}).start();
	}
}

/*
 * Database Settings
 */
class DatabaseSettings extends JPanel {
	private static final long	serialVersionUID	= 1L;

	private JLabel				lbUserName, lbPassword, lbDatabaseUrl, lbDialect, lbDriver;
	private JTextField			tfUserName, tfPassword, tfDatabaseUrl, tfDialect, tfDriver;
	private JButton				save;
	private JLabel				lbInfo;

	public DatabaseSettings() {

		lbUserName = new JLabel("User Name");
		lbPassword = new JLabel("Password");
		lbDatabaseUrl = new JLabel("Database URL");
		lbDialect = new JLabel("Database Dialect");
		lbDriver = new JLabel("Database Driver");

		lbInfo = new JLabel("");

		tfUserName = new JTextField(20);
		tfPassword = new JTextField(20);
		tfDatabaseUrl = new JTextField(20);
		tfDialect = new JTextField(20);
		tfDriver = new JTextField(20);

		tfUserName.setText(JSONProperties.getJSonProperty().getDatabase().getUsername());
		tfPassword.setText(JSONProperties.getJSonProperty().getDatabase().getPassword());
		tfDatabaseUrl.setText(JSONProperties.getJSonProperty().getDatabase().getDatabaseURL());
		tfDialect.setText(JSONProperties.getJSonProperty().getDatabase().getDialect());
		tfDriver.setText(JSONProperties.getJSonProperty().getDatabase().getDriver());

		save = new JButton("Save");
		save.addActionListener(saveActionListener());

		this.setLayout(new BorderLayout());

		this.add(setDatabaseSettingsUI());
		Box hBox = Box.createHorizontalBox();
		hBox.add(Box.createHorizontalGlue());
		hBox.add(lbInfo);
		hBox.add(Box.createHorizontalGlue());
		this.add(hBox, BorderLayout.SOUTH);
	}

	private JPanel setDatabaseSettingsUI() {

		final int GAP = 5; // Default gap btwn components.
		GBHelper pos = new GBHelper(); // Create GridBag helper object.
		JPanel content = new JPanel(new GridBagLayout());
		content.add(lbUserName, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(tfUserName, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(lbPassword, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(tfPassword, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(lbDatabaseUrl, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(tfDatabaseUrl, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(lbDialect, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(tfDialect, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(lbDriver, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(tfDriver, pos);
		content.add(new Gap(GAP * 2), pos.nextRow());
		content.add(save, pos);

		return content;
	}

	private ActionListener saveActionListener() {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JSONProperties.getJSonProperty().getDatabase().setUsername(tfUserName.getText());
				JSONProperties.getJSonProperty().getDatabase().setPassword(tfPassword.getText());
				JSONProperties.getJSonProperty().getDatabase().setDatabaseURL(tfDatabaseUrl.getText());
				JSONProperties.getJSonProperty().getDatabase().setDialect(tfDialect.getText());
				JSONProperties.getJSonProperty().getDatabase().setDriver(tfDriver.getText());
				try {
					JSONProperties.getJSonProperty();
					JSONProperties.saveProperties();
				} catch (JsonProcessingException e1) {
					if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
						ExceptionDialog.showException(e1);
					}
					e1.printStackTrace();
				} catch (IOException e1) {
					if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
						ExceptionDialog.showException(e1);
					}

					e1.printStackTrace();
				}
				lbInfo.setText("Gespeichert");
				SettingsFrame.changeTitle(SettingsFrame.title + " -> gespeichert");
			}
		};
	}
}

/*
 * Design Settings
 */
class DesignSettings extends JPanel {
	private static final long	serialVersionUID	= 1L;
	private ThemeUtils			uiUtils				= null;

	protected String			curLF				= "javax.swing.plaf.metal.MetalLookAndFeel";

	protected JRadioButton		previousButton;
	private MainFrame			mainFrame;

	public DesignSettings(MainFrame mainframe) {
		this.mainFrame = mainframe;
		ButtonGroup bg = new ButtonGroup();

		try {
			uiUtils = new ThemeUtils();
		} catch (ParseException e) {
			e.printStackTrace();
			if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
				ExceptionDialog.showException(e);
			}
		}

		for (String s : uiUtils.getThemeList()) {
			JRadioButton bJava = new JRadioButton(s);
			if (JSONProperties.getJSonProperty().getGlobal().getTheme().equals(s)) {
				bJava.setSelected(true);
			}
			bJava.addActionListener(new LNFSetter(s, bJava));
			bg.add(bJava);
			this.add(bJava);
		}
	}

	class LNFSetter implements ActionListener {
		String			theLNFName;

		JRadioButton	thisButton;

		LNFSetter(String lnfName, JRadioButton me) {
			theLNFName = lnfName;
			thisButton = me;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				uiUtils.initLookAndFeel(theLNFName);
				JSONProperties.getJSonProperty().getGlobal().setTheme(theLNFName);
				SwingUtilities.updateComponentTreeUI(mainFrame);
				mainFrame.pack();
				JSONProperties.saveProperties();
			} catch (Exception e0) {
				if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
					ExceptionDialog.showException(e0);
				}
				previousButton.setSelected(true);
			}
			previousButton = thisButton;
			SettingsFrame.changeTitle(SettingsFrame.title + " -> gespeichert");
		}
	}
}

/*
 * Global Settings
 */
class GlobalSettings extends JPanel {
	private static final long	serialVersionUID	= 1L;

	private JCheckBox			startWithFullscreen;

	private JLabel				lbWidth, lbHeight;
	private JTextField			tfWidth, tfHeight;
	private JButton				save;

	public GlobalSettings() {
		this.startWithFullscreen = new JCheckBox("Start With Fullscreen");
		this.lbWidth = new JLabel("Window Width");
		this.lbHeight = new JLabel("Window Height");

		this.tfWidth = new JTextField(20);
		this.tfHeight = new JTextField(20);

		this.save = new JButton("Save");

		startWithFullscreen.setSelected(JSONProperties.getJSonProperty().getGlobal().getFullScreen());

		tfHeight.setEditable(JSONProperties.getJSonProperty().getGlobal().getFullScreen());
		tfWidth.setEditable(JSONProperties.getJSonProperty().getGlobal().getFullScreen());

		tfWidth.setText(JSONProperties.getJSonProperty().getGlobal().getWidth() + "");
		tfHeight.setText(JSONProperties.getJSonProperty().getGlobal().getHeight() + "");

		startWithFullscreen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AbstractButton abstractButton = (AbstractButton) e.getSource();

				if (abstractButton.getModel().isSelected()) {
					tfHeight.setEditable(false);
					tfWidth.setEditable(false);
				} else {
					tfHeight.setEditable(true);
					tfWidth.setEditable(true);
				}

			}
		});
		save.addActionListener(saveActionListener());

		this.add(setGlobalSettingsUI());
	}

	private JPanel setGlobalSettingsUI() {

		final int GAP = 5;
		GBHelper pos = new GBHelper();
		JPanel content = new JPanel(new GridBagLayout());
		content.add(startWithFullscreen, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(lbWidth, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(tfWidth, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(lbHeight, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(tfHeight, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(save, pos);

		return content;
	}

	private ActionListener saveActionListener() {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JSONProperties.getJSonProperty().getGlobal().setFullScreen(startWithFullscreen.isSelected());
				if (!tfWidth.getText().equals(""))
					JSONProperties.getJSonProperty().getGlobal().setWidth(tfWidth.getText());
				if (!tfHeight.getText().equals(""))
					JSONProperties.getJSonProperty().getGlobal().setHeight(tfHeight.getText());
				try {
					JSONProperties.saveProperties();
				} catch (JsonProcessingException e1) {
					e1.printStackTrace();
					if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
						ExceptionDialog.showException(e1);
					}
				} catch (IOException e1) {
					e1.printStackTrace();
					if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
						ExceptionDialog.showException(e1);
					}
				}
				SettingsFrame.changeTitle(SettingsFrame.title + " -> gespeichert");
			}
		};
	}
}

/*
 * Language Settings
 */
class LanguageSettings extends JPanel {
	private static final long	serialVersionUID	= 1L;

	protected JRadioButton		previousButton;
	private MainFrame			mainFrame;

	public LanguageSettings(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
		ButtonGroup bg = new ButtonGroup();

		List<String> langFiles = getAllLangFiles();

		for (String langName : langFiles) {
			langName = langName.substring(0, 2);
			JRadioButton radioLang = new JRadioButton(langName.toUpperCase());
			System.out.println("Set Lang -> " + langName);
			if (JSONProperties.getJSonProperty().getGlobal().getLang().equals(langName)) {
				radioLang.setSelected(true);
			}
			radioLang.addActionListener(new LNFSetter(langName, radioLang));
			bg.add(radioLang);
			this.add(radioLang);
		}
	}

	private List<String> getAllLangFiles() {
		List<String> files = null;
		try {
			files = IOUtils.readLines(SettingsFrame.class.getClass().getResourceAsStream("/lang/"), Charsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (String f : files) {
			System.out.println(f);
		}
		return files;

	}

	class LNFSetter implements ActionListener {
		String			langName;

		JRadioButton	thisButton;

		LNFSetter(String langName, JRadioButton me) {
			this.langName = langName;
			thisButton = me;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			previousButton = thisButton;
			try {

				JSONProperties.getJSonProperty().getGlobal().setLang(langName);
				SwingUtilities.updateComponentTreeUI(mainFrame);
				JSONProperties.saveProperties();
			} catch (Exception e0) {
				if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
					ExceptionDialog.showException(e0);
				} else {
					e0.printStackTrace();
				}
				previousButton.setSelected(true);
			}
			SettingsFrame.changeTitle(SettingsFrame.title + " -> gespeichert: " + langName);
		}
	}
}

/*
 * Database Settings
 */
class TableSettings extends JPanel {
	private static final long	serialVersionUID	= 1L;

	private JLabel				lbEnableColMenu;			// , lbPassword,
															// lbDatabaseUrl,
															// lbDialect,
															// lbDriver;
	private JCheckBox			cbEnableColMenu;			// , tfPassword,
															// tfDatabaseUrl,
															// tfDialect,
															// tfDriver;
	private JButton				save;
	private JLabel				lbInfo;

	public TableSettings() {

		this.lbEnableColMenu = new JLabel("Enable Column Menu");
		// lbPassword = new JLabel("Password");
		// lbDatabaseUrl = new JLabel("Database URL");
		// lbDialect = new JLabel("Database Dialect");
		// lbDriver = new JLabel("Database Driver");

		lbInfo = new JLabel("");

		cbEnableColMenu = new JCheckBox();
		// tfPassword = new JTextField(20);
		// tfDatabaseUrl = new JTextField(20);
		// tfDialect = new JTextField(20);
		// tfDriver = new JTextField(20);

		cbEnableColMenu.setSelected(JSONProperties.getJSonProperty().getTable().getEnableColumnMenu());

		// tfUserName.setText(JSONProperties.getJSonProperty().getDatabase().getUsername());
		// tfPassword.setText(JSONProperties.getJSonProperty().getDatabase().getPassword());
		// tfDatabaseUrl.setText(JSONProperties.getJSonProperty().getDatabase().getDatabaseURL());
		// tfDialect.setText(JSONProperties.getJSonProperty().getDatabase().getDialect());
		// tfDriver.setText(JSONProperties.getJSonProperty().getDatabase().getDriver());

		save = new JButton("Save");
		save.addActionListener(saveActionListener());

		this.setLayout(new BorderLayout());

		JTabbedPane pane = new JTabbedPane();

		// this.add(setDatabaseSettingsUI());
		Box hBox = Box.createHorizontalBox();
		hBox.add(Box.createHorizontalGlue());
		hBox.add(lbInfo);
		hBox.add(Box.createHorizontalGlue());
		// pane.add("test", hBox);
		pane.add("Context menu", setEnablePlayerTableContextMenu());
		this.add(pane, BorderLayout.CENTER);
	}

	private JPanel setEnablePlayerTableContextMenu() {

		final int GAP = 20; // Default gap btwn components.
		GBHelper pos = new GBHelper(); // Create GridBag helper object.
		JPanel content = new JPanel(new GridBagLayout());
		content.add(lbEnableColMenu, pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(new Gap(GAP), pos.nextCol());
		content.add(cbEnableColMenu, pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(new JLabel("Test"), pos);
		content.add(new Gap(GAP), pos.nextCol());
		content.add(new Gap(GAP), pos.nextCol());
		content.add(new JCheckBox(), pos);
		content.add(new Gap(GAP), pos.nextRow());
		content.add(new Gap(GAP), pos.nextRow());
		// content.add(lbDatabaseUrl, pos);
		// content.add(new Gap(GAP), pos.nextCol());
		// content.add(tfDatabaseUrl, pos);
		// content.add(new Gap(GAP), pos.nextRow());
		// content.add(lbDialect, pos);
		// content.add(new Gap(GAP), pos.nextCol());
		// content.add(tfDialect, pos);
		// content.add(new Gap(GAP), pos.nextRow());
		// content.add(lbDriver, pos);
		// content.add(new Gap(GAP), pos.nextCol());
		// content.add(tfDriver, pos);
		content.add(new Gap(GAP * 2), pos.nextRow());
		content.add(save, pos);

		return content;
	}

	private ActionListener saveActionListener() {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JSONProperties.getJSonProperty().getTable().setEnableColumnMenu(cbEnableColMenu.isSelected());
				// JSONProperties.getJSonProperty().getDatabase().setPassword(tfPassword.getText());
				// JSONProperties.getJSonProperty().getDatabase().setDatabaseURL(tfDatabaseUrl.getText());
				// JSONProperties.getJSonProperty().getDatabase().setDialect(tfDialect.getText());
				// JSONProperties.getJSonProperty().getDatabase().setDriver(tfDriver.getText());
				try {
					JSONProperties.getJSonProperty();
					JSONProperties.saveProperties();
				} catch (JsonProcessingException e1) {
					if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
						ExceptionDialog.showException(e1);
					}
					e1.printStackTrace();
				} catch (IOException e1) {
					if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
						ExceptionDialog.showException(e1);
					}

					e1.printStackTrace();
				}
				lbInfo.setText("Gespeichert");
				SettingsFrame.changeTitle(SettingsFrame.title + " -> gespeichert");
			}
		};
	}
}
