package com.gui.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import org.apache.log4j.Logger;

import com.gui.view.splitpane.tables.PlayerListPanel;
import com.gui.view.splitpane.tables.PointsListPanel;

public class ListPanel extends JPanel {
	final static Logger			logger				= Logger.getLogger(ListPanel.class);
	private static final long	serialVersionUID	= 1L;
	private JTabbedPane			tabbedPane;
	private static ListPanel	instance;

	private ListPanel(PlayerListPanel playerListPanel, JPanel groupListPanel, PointsListPanel pointsListPanel, Map<String, List<String>> tableNames) {
		this.setLayout(new GridBagLayout());

		tabbedPane = new JTabbedPane(SwingConstants.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
		tabbedPane.add("Spieler Liste", playerListPanel);
		tabbedPane.addTab("Punkte", pointsListPanel);

		GridBagConstraints c = new GridBagConstraints();

		c.gridx = 0;
		c.gridy = 0;

		c.insets = new Insets(10, 10, 10, 10);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;

		this.add(tabbedPane, c);
	}

	public static ListPanel getInstance(PlayerListPanel playerListPanel, JPanel groupListPanel, PointsListPanel pointsListPanel, Map<String, List<String>> tableNames) {
		if (instance == null) {
			instance = new ListPanel(playerListPanel, groupListPanel, pointsListPanel, tableNames);
		}
		return instance;
	}
}
