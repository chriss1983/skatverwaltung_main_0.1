package com.gui.utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.backend.jsonmapper.JSONProperties;
import com.gui.dialogs.ExceptionDialog;

import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;
import de.javasoft.plaf.synthetica.SyntheticaBlackEyeLookAndFeel;
import de.javasoft.plaf.synthetica.SyntheticaBlackMoonLookAndFeel;
import de.javasoft.plaf.synthetica.SyntheticaBlackStarLookAndFeel;
import de.javasoft.plaf.synthetica.SyntheticaClassyLookAndFeel;
import de.javasoft.plaf.synthetica.SyntheticaLookAndFeel;
import de.javasoft.plaf.synthetica.SyntheticaWhiteVisionLookAndFeel;


public class ThemeUtils {

	static Map<String, SyntheticaLookAndFeel>	themes;
	public static final String					ALUOXIDE	= "ALUOXIDE";
	public static final String					BLACKEYE	= "BLACKEYE";
	public static final String					BLACKMOON	= "BLACKMOON";
	public static final String					BLACKSTAR	= "BLACKSTAR";
	public static final String					WHITEVISION	= "WHITEVISION";
	public static final String					CLASSY		= "CLASSY";
	public static final String					METAL		= "METAL";
	public static final String					NOIRE		= "NOIRE";

	public ThemeUtils() throws ParseException {

		themes = new HashMap<String, SyntheticaLookAndFeel>();

		themes.put("ALUOXIDE", new SyntheticaAluOxideLookAndFeel());
		themes.put("BLACKEYE", new SyntheticaBlackEyeLookAndFeel());
		themes.put("BLACKMOON", new SyntheticaBlackMoonLookAndFeel());

		themes.put("BLACKSTAR", new SyntheticaBlackStarLookAndFeel());
		themes.put("WHITEVISION", new SyntheticaWhiteVisionLookAndFeel());
		themes.put("CLASSY", new SyntheticaClassyLookAndFeel());
	}

	public void initLookAndFeel(String theme) throws Exception {

		UIManager.removeAuxiliaryLookAndFeel(UIManager.getLookAndFeel());
		try {
			String key = new String(new byte[] { 67, 49, 52, 49, 48, 50, 57, 52, 45, 54, 49, 66, 54, 52, 65, 65, 67, 45, 52, 66, 55, 68, 51, 48, 51, 57, 45, 56, 51, 52, 65, 56, 50, 65, 49, 45, 51, 55, 69, 53, 68, 54, 57, 53 }, "UTF-8");
			String key1 = "F9F97345-122DDD91-51799FCC-1FBD8BFE-0AA74328";

			if (key != null) {
				String[] li = { "Licensee=PowerFolder", "LicenseRegistrationNumber=235363175", "Product=Synthetica", "LicenseType=Small Business License", "ExpireDate=--.--.----", "MaxVersion=2.999.999" };
				UIManager.put("Synthetica.license.info", li);
				UIManager.put("Synthetica.license.key", key1);
				UIManager.put("Synthetica.window.decoration", false);
				UIManager.put("Synthetica.focus.textComponents.enabled", false);
				UIManager.put("Synthetica.extendedFileChooser.rememberPreferences", Boolean.FALSE);
				UIManager.put("Synthetica.tree.line.type", "NONE");
				UIManager.put("Synthetica.window.decoration", false);
			}

			System.out.println("SyntheticaLookAndFeel Theme");
			UIManager.setLookAndFeel(themes.get(theme));

		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
			if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
				ExceptionDialog.showException(e);
			}
		}
	}

	public List<String> getThemeList() {
		List<String> listOfThemes = new ArrayList<String>(themes.keySet());
		return listOfThemes;
	}

	public static void main(String[] args) throws Exception {

		new ThemeUtils().initLookAndFeel(ThemeUtils.CLASSY);
		List<String> l = new ArrayList<String>(themes.keySet());

		for (String s : l) {
			System.out.println(s);
		}

		JFrame frame = new JFrame();

		frame.setTitle("test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(600, 300);
		frame.setVisible(true);

	}

}
