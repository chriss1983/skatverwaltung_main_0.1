package com.skat_haan_gruiten;

import java.awt.Dimension;
import java.math.BigInteger;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.backend.controller.activation.ActivationKeyController;
import com.backend.controller.listener.window.GuiManager;
import com.backend.jsonmapper.JSONProperties;
import com.backend.security.KeyHandler;
import com.gui.dialogs.ExceptionDialog;
import com.gui.splashscreen.SplashScreen;
import com.gui.utils.ThemeUtils;
import com.gui.view.MainFrame;
import com.gui.view.security.ActivationView;

// TODO: Clean project, remove all unused Variables and Classes. Create for all
// Listeners seperate classes
// Seperate Model from Controller

/**
 * @author Christian Richter
 *         Objekt zum Handeln der Table Collums
 *         TODO: klickt
 *         -> Benachrichtigen wenn Offline.
 *         -> InfoPanel für Gruppe Erstellen, Button editierbar machen
 *         -> Liste der Player in einer List und diese über den Namen der Gruppe
 *         in eienr Map speichern.
 *         -> PlayerPicker für die Gruppen erstellen, welcher aus den Playerpool
 *         die entsprechenden Player anzeigt.
 *         -> Sprache für das Kontextmenü hinzufügen
 *         -> DONE - Tabellen Problem lösen: Tabelle Gruppe und Player zeigt
 *         selben Datensatz. -> done - Settings - Info hinzufügen, wenn der User
 *         auf Speichern -> DONE - Alle Einstellungen in JSON Abspeicher bevor
 *         App beendet wird -> Done - ActivationKey in Properties überprüfen und
 *         Auscoden Zeile 96 -> Done - Eingabe von Usern Auscoden und diesen zur
 *         Tabelle hinzufügen. -> Done - Info Panel für Spieler erstellen ->
 *         Done - Aktuell gewählten eintrag in der Tabelle via
 *         -> invalid - Wenn Online, Fragen ob Daten sofort gesichert und zur
 *         Datenbank geschrieben werden sollen
 *         *
 */
public class StartSkatManager {

	private final Logger	logger				= Logger.getLogger(this.getClass());

	private final Boolean	ACTIVATION_ENABLE	= JSONProperties.getJSonProperty().getSecurity().getActivationEnable();
	public final BigInteger	PUBLIC_KEY			= new BigInteger(JSONProperties.getJSonProperty().getSecurity().getPublicKey());

	public static Boolean	isOnline			= true;
	private KeyHandler		securityHandler;
	private ActivationView	activationView;
	private MainFrame		frame;
	private SplashScreen	splashScreen;

	public StartSkatManager() {
		logger.debug("Create and init Mainframe");

		securityHandler = new KeyHandler();
		splashScreen = new SplashScreen();

		Locale.setDefault(new Locale(JSONProperties.getJSonProperty().getGlobal().getLang()));
		activationView = new ActivationView();
		activationView.addActionListener(new ActivationKeyController(this, activationView, securityHandler));

		if (ACTIVATION_ENABLE && JSONProperties.getJSonProperty().getSecurity().getActivationKey().equals("")) {
			activationView.showOrHide("show");
			boolean runn = true;
			while (runn) {
				if (JSONProperties.getJSonProperty().getSecurity().getActivationKey() != null) {
					if (checkActivationKey()) {
						runn = false;

						init();
					}
				}
			}
			logger.debug("finish enter Aktivation Key");
		} else {
			splashScreen.showSplash();

			splashScreen.updateProgressBarText("Init Properties");
			splashScreen.updateProgressBar(0);
			init();
		}
	}

	public void init() {
		logger.debug("init");
		try {
			new ThemeUtils().initLookAndFeel(JSONProperties.getJSonProperty().getGlobal().getTheme());
		} catch (Exception e) {
			if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
				ExceptionDialog.showException(e);
			}
			e.printStackTrace();
		}

		frame = new MainFrame(splashScreen);
		frame.addWindowListener(new GuiManager(frame));
		frame.showFrame();

		initAndStartMainFrame();
	}

	private void initAndStartMainFrame() {
		splashScreen.updateProgressBarText("Load properties");
		splashScreen.updateProgressBar(10);
		
		String title = JSONProperties.getJSonProperty().getGlobal().getAppName() + " - Version " + JSONProperties.getJSonProperty().getGlobal().getVersion(); 
		
		 
		if(JSONProperties.getJSonProperty().getSecurity().getActivationState().equals("DEMO")) {
			title += " " + JSONProperties.getJSonProperty().getSecurity().getActivationState();
		}
		
		frame.setTitle(title);
		frame.setSize(new Dimension(Integer.parseInt(JSONProperties.getJSonProperty().getGlobal().getWidth()), Integer.parseInt(JSONProperties.getJSonProperty().getGlobal().getHeight() + "")));

		// new GuiManager(frame);
	}

	public Boolean checkActivationKey() {
		logger.debug("checkActivation " + JSONProperties.getJSonProperty().getSecurity().getActivationKey());
		return securityHandler.verify(JSONProperties.getJSonProperty().getSecurity().getActivationKey().replaceAll("-", ""), PUBLIC_KEY);
	}

	public static void main(String[] args) {
		new StartSkatManager();
	}
}
