package com.redeyed.logger;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.Priority;

import com.backend.jsonmapper.JSONProperties;
import com.gui.dialogs.ExceptionDialog;

public class MeinLogger {
	private static final String		MEIN_LOGGER_NAME	= "logger";
	private static final String		MESSAGES_RESBUNDLE	= "messages";
	private static ResourceBundle	messagesResBundle;
	private static MeinLogger		meinLogger;
	private static Logger			log4jLogger;

	// private damit Singleton
	private MeinLogger() {
		init();
	}

	private synchronized void init() {
		PatternLayout layout = new PatternLayout();
		String conversionPattern = "%d{dd.MM.yyyy} %-5p %X{id} [%t] %X{clss}: %m%n";
		layout.setConversionPattern(conversionPattern);

		// creates console appender
		ConsoleAppender consoleAppender = new ConsoleAppender();
		consoleAppender.setLayout(layout);
		consoleAppender.activateOptions();

		// creates file appender
		FileAppender fileAppender = new FileAppender();
		fileAppender.setFile("applog3.txt");
		fileAppender.setLayout(layout);
		fileAppender.activateOptions();

		// configures the root logger
		Logger rootLogger = Logger.getRootLogger();
		rootLogger.setLevel(Level.DEBUG);
		rootLogger.addAppender(consoleAppender);
		rootLogger.addAppender(fileAppender);

		log4jLogger = Logger.getLogger(MEIN_LOGGER_NAME);
		messagesResBundle = ResourceBundle.getBundle(MESSAGES_RESBUNDLE);
		log4jLogger.setResourceBundle(messagesResBundle);
	}

	// Singleton-Instanz
	public static synchronized MeinLogger getInstance() {
		if (meinLogger == null)
			meinLogger = new MeinLogger();
		return meinLogger;
	}

	public synchronized void log(Level level, Object caller, String id, Object[] parms) {
		MDC.put("clss", caller.getClass().getSimpleName() + ".class");
		MDC.put("id", id);

		String message = id;
		if (null != messagesResBundle) {
			try {
				message = messagesResBundle.getString(id);
			} catch (MissingResourceException e) {
				/**/System.out.println("BLA");
				if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
					ExceptionDialog.showException(e);
				}
			}
		}
		if (null != parms)
			message = MessageFormat.format(message, parms);

		switch (level.toInt()) {
			case Priority.ALL_INT:
			case Priority.DEBUG_INT:
				log4jLogger.debug(message);
				break;
			case Priority.INFO_INT:
				log4jLogger.info(message);
				break;
			case Priority.WARN_INT:
				log4jLogger.warn(message);
				break;
			case Priority.ERROR_INT:
				log4jLogger.error(message);
				break;
			case Priority.FATAL_INT:
				log4jLogger.fatal(message);
				break;
		}
	}

	public boolean isEnabledFor(Level level) {
		return log4jLogger.isEnabledFor(level);
	}
}