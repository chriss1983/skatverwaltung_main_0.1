package com.redeyed.logger;

import org.apache.log4j.Level;

public class ProgrammaticLog4jExample {
	MeinLogger logger = MeinLogger.getInstance();

	public ProgrammaticLog4jExample() {
		// TODO Auto-generated constructor stub
		logger.log(Level.ERROR, this, "Find Error", null/*
														 * new String[] { "abc",
														 * "xyz" }
														 */);

		logger.log(Level.DEBUG, this, "Find Debug", null/*
														 * new String[] { "abc",
														 * "xyz" }
														 */);
	}

	public static void main(String[] args) {

		new ProgrammaticLog4jExample();
		// // creates pattern layout
		// PatternLayout layout = new PatternLayout();
		// String conversionPattern = "%d{dd.MM.yyyy} %-5p %X{id} [%t] %X{clss}:
		// %m%n";
		// layout.setConversionPattern(conversionPattern);
		//
		// // creates console appender
		// ConsoleAppender consoleAppender = new ConsoleAppender();
		// consoleAppender.setLayout(layout);
		// consoleAppender.activateOptions();
		//
		// // creates file appender
		// FileAppender fileAppender = new FileAppender();
		// fileAppender.setFile("applog3.txt");
		// fileAppender.setLayout(layout);
		// fileAppender.activateOptions();
		//
		// // configures the root logger
		// Logger rootLogger = Logger.getRootLogger();
		// rootLogger.setLevel(Level.DEBUG);
		// rootLogger.addAppender(consoleAppender);
		// rootLogger.addAppender(fileAppender);
		//
		// // creates a custom logger and log messages
		// Logger logger = Logger.getLogger(ProgrammaticLog4jExample.class);
		// logger.debug("this is a debug log message");
		// logger.info("this is a information log message");
		// logger.warn("this is a warning log message");
	}
}