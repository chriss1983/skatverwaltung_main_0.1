package com.backend.server.connector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.backend.security.CryptUtil;
import com.backend.server.model.UserDetailsEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class RedeyedServerConnector {
	//private String				json		= "{\\r\\n\\t\\\"database\\\": {\\r\\n\\t\\t\\\"username\\\": \\\"root\\\",\\r\\n\\t\\t\\\"password\\\": \\\"root\\\",\\r\\n\\t\\t\\\"databaseURL\\\": \\\"jdbc:mysql:\\/\\/localhost:3306\\/test\\\",\\r\\n\\t\\t\\\"dialect\\\": \\\"MySQLDialect\\\",\\r\\n\\t\\t\\\"driver\\\": \\\"com.mysql.jdbc.Driver\\\"\\r\\n\\t},\\r\\n\\t\\\"databaseH2\\\": {\\r\\n\\t\\t\\\"username\\\": \\\"root\\\",\\r\\n\\t\\t\\\"password\\\": \\\"admin\\\",\\r\\n\\t\\t\\\"databaseURL\\\": \\\"jdbc:hsqldb:.\\/db\\/repository\\/skatmanager\\\",\\r\\n\\t\\t\\\"dialect\\\": \\\"H2Dialect\\\",\\r\\n\\t\\t\\\"driver\\\": \\\"org.hsqldb.jdbcDriver\\\"\\r\\n\\t},\\r\\n\\t\\\"debugging\\\": {\\r\\n\\t\\t\\\"showErrorMessages\\\": false\\r\\n\\t},\\r\\n\\t\\\"security\\\": {\\r\\n\\t\\t\\\"activationEnable\\\": false,\\r\\n\\t\\t\\\"publicKey\\\": \\\"36559548014518483\\\",\\r\\n\\t\\t\\\"activationState\\\": \\\"DEMO\\\",\\r\\n\\t\\t\\\"activationKey\\\": \\\"wlwo8IIe6UaFBLmYO7WlXG9g0hH48nJzd6a0SkY6yxw=\\\",\\r\\n\\t\\t\\\"secureKey\\\": \\\"Z8LSq0wWwB5v+6YJzurcP463H3F12iZh74fDj4S74oUH4EONkiKb2FmiWUbtFh97GG\\/c\\/lbDE47mvw6j94yXxKHOpoqu6zpLKMKPcOoSppcVWb2q34qENBJkudXUh4MWcreondLmLL2UyydtFKuU9Sa5VgY\\/CzGaVGJABK2ZR94=\\\"\\r\\n\\t},\\r\\n\\t\\\"global\\\": {\\r\\n\\t\\t\\\"appName\\\": \\\"Skat Manager Haan-Gruiten\\\",\\r\\n\\t\\t\\\"version\\\": \\\"0.1 (Nightly)\\\",\\r\\n\\t\\t\\\"width\\\": \\\"1000\\\",\\r\\n\\t\\t\\\"height\\\": \\\"800\\\",\\r\\n\\t\\t\\\"fullScreen\\\": true,\\r\\n\\t\\t\\\"theme\\\": \\\"BLACKEYE\\\",\\r\\n\\t\\t\\\"lang\\\": \\\"en\\\"\\r\\n\\t},\\r\\n\\t\\\"table\\\": {\\r\\n\\t\\t\\\"enableColumnMenu\\\": true\\r\\n\\t}\\r\\n}";

	final static Logger			logger		= Logger.getLogger(RedeyedServerConnector.class);

	private final static String	ROOT_URL	= "http://localhost:8080/TestBackend";

	protected static Gson		GSON		= new GsonBuilder().create();

	public static JSONObject getData(RequestObj requestObj, String url) {
		System.out.println("send Request to " + ROOT_URL + url);
		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(ROOT_URL + url);
			StringEntity input = new StringEntity(GSON.toJson(requestObj));
			input.setContentType("application/json");
			httpPost.setEntity(input);
			CloseableHttpResponse response2 = httpclient.execute(httpPost);

			StatusLine sl = response2.getStatusLine();

			logger.debug("STATUS: " + sl.getStatusCode());
			JSONObject statusCode = new JSONObject();
			statusCode.append("statusCode", sl.getStatusCode());

			switch (sl.getStatusCode()) {

				case 403:
					return statusCode;
				case 404:
					return statusCode;
				case 200:
					return new JSONObject(getString(response2)).append("statusCode", sl.getStatusCode());
				default:
					return statusCode;

			}
		} catch (Exception ex) {
			logger.error("Error in GetData", ex);
		}
		return new JSONObject();
	}

	public static String getString(CloseableHttpResponse response1) throws UnsupportedOperationException, IOException {
		String response = "";

		System.out.println(response1);
		InputStream content = response1.getEntity().getContent();

		BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
		String s = "";
		while ((s = buffer.readLine()) != null) {
			response += s;
		}
		logger.debug("response = " + response);

		return response;
	}

	public static void main(String[] args) throws Exception {
		UserDetailsEntity user = new UserDetailsEntity();
		user.setFirstName("Hans");
		user.setLastName("Maier");
		user.setEmail("chriss1983@ish.de");

		RequestObj requestObj = new RequestObj();
		requestObj.setRequestType("RFID_VALIDATE");
		Map<String, Object> params = new HashMap<String, Object>();
		// params.put("RFID", );
		String key = "Z8LSq0wWwB5v+6YJzurcP463H3F12iZh74fDj4S74oUH4EONkiKb2FmiWUbtFh97GG/c/lbDE47mvw6j94yXxKHOpoqu6zpLKMKPcOoSppcVWb2q34qENBJkudXUh4MWcreondLmLL2UyydtFKuU9Sa5VgY/CzGaVGJABK2ZR94=";
		String crypt = CryptUtil.encrypt(GSON.toJson(user), key);
		params.put("USER", crypt);
		requestObj.setParams(params);
		JSONObject uiObj = RedeyedServerConnector.getData(requestObj, "skatmanager/dataProvider");
		System.out.println(uiObj.getString("USER_DETAILS").toString());
		System.out.println(crypt);

		System.out.println("user = " + user.getFirstName());

		user = GSON.fromJson(uiObj.getString("USER_DETAILS").toString(), UserDetailsEntity.class);

		System.out.println("user = " + user.getFirstName());
	}
}