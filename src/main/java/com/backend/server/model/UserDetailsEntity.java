package com.backend.server.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@javax.persistence.Entity(name = "USERDETAILS")
@Table(name = "USERDETAILS", uniqueConstraints = { @UniqueConstraint(columnNames = { "ID" }) })
public class UserDetailsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true, length = 11)
	private Integer	userDetailsId;

	@Column(name = "EMAIL", length = 20, nullable = true)
	private String	email;

	@Column(name = "FIRSTNAME", length = 20, nullable = true)
	private String	firstName;

	@Column(name = "LASTNAME", length = 20, nullable = true)
	private String	lastName;

	public Integer getUserDetailsId() {
		return userDetailsId;
	}

	public void setUserDetailsId(Integer userDetailsId) {
		this.userDetailsId = userDetailsId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}