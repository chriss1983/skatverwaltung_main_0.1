package com.backend.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.backend.errorhandling.EmptyStingException;
import com.backend.jsonmapper.JSONProperties;
import com.gui.dialogs.ExceptionDialog;

public class JSONUtil {
	final static Logger		logger	= Logger.getLogger(JSONUtil.class);
	private static JSONUtil	instance;

	private ObjectMapper	mapper;

	private List<String>	jsonHolder;
	JSONUtil				util;

	private JSONUtil() {
		logger.info("Init JSONUtil");

		jsonHolder = new ArrayList<String>();
		mapper = new ObjectMapper();
	}

	public String objectToJSON(Object jsonObject) {
		logger.info("objectToJSON");
		if (jsonObject != null) {
			try {
				String jsonObjectString = mapper.writeValueAsString(jsonObject);
				return jsonObjectString;
			} catch (IOException e) {
				logger.error("IO Exception : " + e.getMessage());
				if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
					ExceptionDialog.showException(e);
				}
			} catch (ClassCastException e) {
				logger.error("cannot Convert Object to String: " + e.getMessage());
				if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
					ExceptionDialog.showException(e);
				}
			} catch (NullPointerException e) {
				logger.error("jSon Object has a NullPointer: " + e.getMessage());
				if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
					ExceptionDialog.showException(e);
				}
			}
		}
		return null;
	}

	public Object jsonToObject(String jsonString, Object clazz) {
		if (!jsonString.equals("")) {
			try {
				return mapper.readValue(jsonString, clazz.getClass());
			} catch (IOException e) {
				logger.error("cannot Convert String to " + clazz.getClass().getSimpleName() + " : " + e.getMessage());
				if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
					ExceptionDialog.showException(e);
				}
			} catch (ClassCastException e) {
				logger.error("cannot Convert Object to String : " + e.getMessage());
				if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
					ExceptionDialog.showException(e);
				}
			} catch (NullPointerException e) {
				logger.error("NullpointerException : " + e.getMessage());
				if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
					ExceptionDialog.showException(e);
				}
			}
		} else {
			try {
				throw new EmptyStingException("the String cannot Empty");
			} catch (EmptyStingException e) {
				logger.error("EmptyStingException: " + e.getMessage());
				if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
					ExceptionDialog.showException(e);
				}
			}
		}

		return null;
	}

	public void toSting() {
		for (String string : jsonHolder) {
			System.out.println(string);
		}
	}

	public static JSONUtil getInstace() {
		if (instance == null) {
			instance = new JSONUtil();
		}
		return instance;
	}
}
