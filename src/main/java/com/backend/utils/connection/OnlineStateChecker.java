package com.backend.utils.connection;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class OnlineStateChecker {

	private List<OnlineChecker> observerList = new ArrayList<OnlineChecker>();

	public OnlineStateChecker() {
		startTimer();
	}

	public void notifyListener(String state) {
		for (OnlineChecker checker : observerList) {
			checker.onlineListener(state);
		}
	}

	public void addOnlineListener(OnlineChecker e) {
		observerList.add(e);
	}

	public void removeOnlineListener(OnlineChecker e) {
		observerList.remove(e);
	}

	private void startTimer() {
		Timer t = new Timer();
		t.schedule(new TimerTask() {
			@Override
			public void run() {
				if (checkState()) {
					notifyListener("ONLINE");
				} else {
					notifyListener("OFFLINE");
				}

			}
		}, 0, 2500);
	}

	private Boolean checkState() {
		Socket sock = new Socket();
		InetSocketAddress addr = new InetSocketAddress("google.de", 80);
		try {
			sock.connect(addr, 3000);
			return true;

		} catch (IOException e) {
			return false;
		} finally {
			try {
				sock.close();
			} catch (IOException e) {
			}

		}
	}
}
