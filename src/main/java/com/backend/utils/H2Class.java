package com.backend.utils;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.backend.jsonmapper.JSONProperties;
import com.gui.dialogs.ExceptionDialog;

public class H2Class {
	final static Logger logger = Logger.getLogger(H2Class.class);

	public static void main(String[] a) throws Exception {

		Connection conn = null;
		String tab = "testdb";

		try {

			Class.forName("org.h2.Driver");

			conn = DriverManager.getConnection("jdbc:hsqldb:" + tab, // filenames
					"", // username
					""); // password
			// conn =
			// DriverManager.getConnection("jdbc:h2:~/.javabeginners/h2Test",
			// "", "");

			DatabaseMetaData md = conn.getMetaData();

			String[] types = { "TABLE", "SYSTEM TABLE" };

			ResultSet metaRS = md.getTables(null, null, "%", types);

			while (metaRS.next()) {

				// Catalog
				String tableCatalog = metaRS.getString(1);
				logger.info("Catalog: " + tableCatalog);

				// Schemata
				String tableSchema = metaRS.getString(2);
				logger.info("Tabellen-Schema: " + tableSchema);

				// Tabellennamen
				String tableName = metaRS.getString(3);
				logger.info("Tabellen-Name: " + tableName);

				// Tabellentyp
				String tableType = metaRS.getString(4);
				logger.info("Tabellen-Typ: " + tableType + "\n");
			}

			Statement stmt = conn.createStatement();

			String dropQ = "DROP TABLE IF EXISTS " + tab;
			stmt.executeUpdate(dropQ);

			// String createQ = "CREATE TABLE IF NOT EXISTS " + tab
			// + "(ID INT PRIMARY KEY AUTO_INCREMENT(1,1) NOT NULL, NAME
			// VARCHAR(255))";
			// stmt.executeUpdate(createQ);

			String insertQ = "INSERT INTO " + tab + " VALUES(TRANSACTION_ID(),'Hello World!')";
			stmt.executeUpdate(insertQ);
			ResultSet selectRS = stmt.executeQuery("SELECT * FROM " + tab);
			while (selectRS.next()) {
				System.out.printf("%s, %s\n", selectRS.getString(1), selectRS.getString(2));
			}

			System.out.println("Liste Tabellen...");
			String tablesQ = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='PUBLIC'";
			ResultSet tablesRS = stmt.executeQuery(tablesQ);
			while (tablesRS.next()) {
				System.out.printf("Tabelle %s vorhanden \n", tablesRS.getString(1));
			}

			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
				ExceptionDialog.showException(e);
			}
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
						ExceptionDialog.showException(e);
					}
				}
		}
	}
}