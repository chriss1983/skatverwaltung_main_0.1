package com.backend.utils;

import java.awt.Image;

import javax.swing.ImageIcon;

import com.gui.view.splitpane.infopage.InfoPane;

public class ImageLoader {
	private static ImageLoader	instance;
	private static ImageIcon	icon;
	private static Image		img;
	private static Image		newimg;

	private ImageLoader() {
		;
	}

	public static enum ImageTyp {
		MENUBAR("/menubar/"), LOGO("/logo/");
		private final String text;

		private ImageTyp(final String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}
	}

	public static ImageIcon getImageIcon(ImageTyp imageType, String fileName) {
		if (instance == null) {
			instance = new ImageLoader();
		}


		icon = new ImageIcon(InfoPane.class.getResource("/images" + imageType + fileName + ".png"));
		img = icon.getImage();
		newimg = img.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		return new ImageIcon(newimg);
	}

	public static Image getImage(ImageTyp imageType, String fileName) {
		if (instance == null) {
			instance = new ImageLoader();
		}

		icon = new ImageIcon(InfoPane.class.getResource("/images" + imageType + fileName + ".png"));
		return icon.getImage();
	}
}
