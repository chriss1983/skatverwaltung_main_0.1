package com.backend.utils;

public final class OsUtils {
	private static String OS = null;

	public static String getOsName() {
		if (OS == null) {
			OS = System.getProperty("os.name");
		}
		return OS;
	}

	public static boolean isWindows() {
		return getOsName().startsWith("Windows");
	}

	public static boolean isUnix() {
		return getOsName().startsWith("Linux");
	}

	public static String getHomeDir() {
		return System.getProperty("user.home") + "/";
	}

	public static void main(String[] args) {
		System.out.println(System.getProperty("user.home"));
	}
}