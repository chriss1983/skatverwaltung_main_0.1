package com.backend.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.log4j.Logger;

import com.backend.jsonmapper.JSONProperties;

@Entity()
@Table(name = "TEAM", uniqueConstraints = { @UniqueConstraint(columnNames = { "ID" }) })
public class Team implements Serializable, IModel {
	final static Logger			logger				= Logger.getLogger(Team.class);

	public final static int		PLAYER_ADDED		= 0;
	public final static int		PLAYER_IS_IN_TEAM	= 1;
	public final static int		MAX_PLAYERS_REACHED	= 2;
	public final static int 	PLAYER_IS_IN_OTHER_TEAM = 3;

	private static final long	serialVersionUID	= -9033527683159348260L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true, length = 11)
	private int					id;
	@ElementCollection
	@CollectionTable(name = "PLAYERS_MAPPING")
	@Column(name = "PLAYERS", length = 20, nullable = true)
	private List<Player>		playersList;

	@Column(name = "TEAMNAME", length = 100, nullable = true)
	private String				teamName;

	@Column(name = "POINTSSERIESONE", length = 10, nullable = true)
	private int					pointsSeriesOne;
	@Column(name = "POINTSSERIESTWO", length = 10, nullable = true)
	private int					pointsSeriesTwo;
	@Column(name = "TOTALSCORE", length = 10, nullable = true)
	private int					totalScore;

	public Team() {
		playersList = new ArrayList<Player>();
	}

	@OneToMany(fetch = FetchType.LAZY)
	public List<Player> getPlayers() {
		return playersList;
	}

	public int addPlayer(Player player) {
		boolean playerAlreadyInTeam = false;
		

		if (playersList.size() <= JSONProperties.getJSonProperty().getTeam().getMaxPlayerCount()) {
			for (Player playerInList : playersList) {
				if (player.getId() == playerInList.getId()) {
					playerAlreadyInTeam = true;
					break;
				}
			}

			if (playerAlreadyInTeam) {
				return PLAYER_IS_IN_TEAM;
			} else {
				playersList.add(player);
				return PLAYER_ADDED;
			}
		} else {
			return MAX_PLAYERS_REACHED;
		}
	}

	public void addPlayer(List<Player> players) {
		System.out.println("PLAYERS LIST IN TEAM: " + players.size());
		for (Player player : players) {
			playersList.add(player);
		}
	}

	public void setNewPlayerList(List<Player> players) {
		this.playersList = players;
	}

	public Player getPlayer(int playerID) {
		for (Player player : playersList) {
			if (player.getId() == playerID) {
				return playersList.get(playerID);
			}
		}
		return null;
	}

	public int getPlayerSize() {
		return playersList.size();
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public int getPointsSeriesOne() {
		return pointsSeriesOne;
	}

	public void setPointsSeriesOne(int pointsSeriesOne) {
		this.pointsSeriesOne = pointsSeriesOne;
	}

	public int getPointsSeriesTwo() {
		return pointsSeriesTwo;
	}

	public void setPointsSeriesTwo(int pointsSeriesTwo) {
		this.pointsSeriesTwo = pointsSeriesTwo;
	}

	public int getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		if (!playersList.isEmpty()) {
			return "TeamImpl [id=" + id + ", idPlayers=" + playersList.get(0).toString() + ", teamName=" + teamName + ", pointsSeriesOne=" + pointsSeriesOne + ", pointsSeriesTwo=" + pointsSeriesTwo + ", totalScore=" + totalScore + "]";
		} else
			return "No Players in List";
	}

}
