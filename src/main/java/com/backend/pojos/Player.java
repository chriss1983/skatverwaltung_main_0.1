package com.backend.pojos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.log4j.Logger;

@Entity
@Table(name = "PLAYER", uniqueConstraints = { @UniqueConstraint(columnNames = { "ID" }) })
public class Player implements Serializable, IModel {
	final static Logger			logger				= Logger.getLogger(Player.class);

	private static final long	serialVersionUID	= -9033527683159348260L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true, length = 11)
	private Integer				id;
	@Column(name = "PAID", length = 20, nullable = true)
	private boolean				paid				= false;
	@Column(name = "FIRSTNAME", length = 20, nullable = true)
	private String				firstName;
	@Column(name = "LASTNAME", length = 20, nullable = true)
	private String				lastName;
	@Column(name = "HOUSENUMBER", length = 20, nullable = true)
	private String				houseNumber;
	@Column(name = "STREET", length = 20, nullable = true)
	private String				street;
	@Column(name = "ZIPCODE", length = 20, nullable = true)
	private String				zipCode;
	@Column(name = "CITY", length = 20, nullable = true)
	private String				city;
	@Column(name = "PLAYERINFO", length = 255, nullable = true)
	private String				playerInfo;
	@Column(name = "EMAIL", length = 55, nullable = true)
	private String				eMail;
	@Column(name = "MOBILEPHONENUMBER", length = 20, nullable = true)
	private String				mobilePhoneNumber;
	@Column(name = "PHONE", length = 20, nullable = true)
	private String				phone;

	@Column(name = "POINTSSERIESONE", length = 10, nullable = true)
	private int					pointsSeriesOne;
	@Column(name = "POINTSSERIESTWO", length = 10, nullable = true)
	private int					pointsSeriesTwo;
	@Column(name = "TOTALSCORE", length = 10, nullable = true)
	private int					totalScore;
	@Column(name = "INTEAM", length = 20, nullable = true)
	private boolean				isInTeam			= false;
	@Column(name = "CURRENTTEAMID", length = 10, nullable = true)
	private int					currentTeamId;

	/**
	 * @return the paid
	 */
	public boolean isPaid() {
		return paid;
	}

	/**
	 * @param paid
	 *            the paid to set
	 */
	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the houseNumber
	 */
	public String getHouseNumber() {
		return houseNumber;
	}

	/**
	 * @param houseNumber
	 *            the houseNumber to set
	 */
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the playerInfo
	 */
	public String getPlayerInfo() {
		return playerInfo;
	}

	/**
	 * @param playerInfo
	 *            the playerInfo to set
	 */
	public void setPlayerInfo(String playerInfo) {
		this.playerInfo = playerInfo;
	}

	/**
	 * @return the eMail
	 */
	public String getEMail() {
		return eMail;
	}

	/**
	 * @param eMail
	 *            the eMail to set
	 */
	public void setEMail(String eMail) {
		this.eMail = eMail;
	}

	/**
	 * @return the mobilePhoneNumber
	 */
	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	/**
	 * @param mobilePhoneNumber
	 *            the mobilePhoneNumber to set
	 */
	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public int getPointsSeriesOne() {
		return pointsSeriesOne;
	}

	public void setPointsSeriesOne(int pointsSeriesOne) {
		this.pointsSeriesOne = pointsSeriesOne;
	}

	public int getPointsSeriesTwo() {
		return pointsSeriesTwo;
	}

	public void setPointsSeriesTwo(int pointsSeriesTwo) {
		this.pointsSeriesTwo = pointsSeriesTwo;
	}

	public int getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}

	public boolean isInTeam() {
		return isInTeam;
	}

	public void setInTeam(boolean isInTeam) {
		this.isInTeam = isInTeam;
	}

	public int getCurrentTeamId() {
		return currentTeamId;
	}

	public void setCurrentTeamId(int currentTeamId) {
		this.currentTeamId = currentTeamId;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", paid=" + paid + ", firstName=" + firstName + ", lastName=" + lastName + ", houseNumber=" + houseNumber + ", street=" + street + ", zipCode=" + zipCode + ", city=" + city + ", playerInfo=" + playerInfo + ", eMail=" + eMail + ", mobilePhoneNumber=" + mobilePhoneNumber + ", phone=" + phone + ", pointsSeriesOne=" + pointsSeriesOne + ", pointsSeriesTwo=" + pointsSeriesTwo + ", totalScore=" + totalScore + ", isInTeam=" + isInTeam + ", currentTeamId=" + currentTeamId + "]";
	}

}
