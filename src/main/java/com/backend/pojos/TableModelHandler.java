package com.backend.pojos;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.backend.jsonmapper.JSONLanguageManager;
import com.backend.models.ModelsHandler;

public class TableModelHandler {
	private static TableModelHandler	instance				= new TableModelHandler();

	public static String				TEAM_TABLE				= "TEAM_TABLE";
	public static String				PLAYER_TABLE			= "PLAYER_TABLE";

	private DefaultTableModel			defaultTeamTableModel	= new DefaultTableModel(0, 0);
	private DefaultTableModel			defaultPlayerTableModel	= new DefaultTableModel(0, 0);

	private JTable						playerTable				= new JTable();
	private JTable						teamTable				= new JTable();

	private int							selectedRow;

	private TableModelHandler() {
		// TableController.addActionListener(new TableListener());

		playerTable.setModel(defaultPlayerTableModel);
		teamTable.setModel(defaultTeamTableModel);

		playerTable.getModel().addTableModelListener(new TableModelListener() {
			 //Called when table has been changed.
			@Override
			public void tableChanged(TableModelEvent e) {
				int row = e.getFirstRow();
				int column = e.getColumn();
				TableModel model = (TableModel) e.getSource();

				if (row >= 0 && column >= 0) {
					String newdata = (String) model.getValueAt(row, column);
					
					for(Player player : ModelsHandler.getInstance().getPlayers()) {
						if(player.getId() == model.getValueAt(row, 0)) {
							System.out.println("The Player to Update is : " + player);
						}
					}
					
					System.out.println("Change the Values : " + playerTable.getColumnName(column) + " in Row " + row + " \n" + newdata);
					
					for(String s : JSONLanguageManager.getLanguageProperty().getTable().getPlayerHeadersNamesAsList()) {
						if(s.equals(newdata)) {
							System.out.println("FOUND");
						}
					}
				}
				System.out.println("TABLE UPDATED");
			}
		});
	}

	public DefaultTableModel getDefaultTeamTableModel() {
		return defaultTeamTableModel;
	}

	public DefaultTableModel getDefaultPlayerTableModel() {
		return defaultPlayerTableModel;
	}

	public void setSelectedRowCount(int selectedRowCount) {
		this.selectedRow = selectedRowCount;
	}

	public int getSelectedRowCount() {
		return selectedRow;
	}

	public void addRowToTeam(Team team, Object[] tableData) {
		defaultTeamTableModel.addRow(tableData);
	}

	private void addRowToPlayer(Player player, Object[] tableData) {
		defaultPlayerTableModel.addRow(tableData);
	}

	public void addRowOnlyToPlayerTable(Player player) {
		defaultPlayerTableModel.addRow(new Object[] { player.getId(), player.getFirstName(), player.getLastName(), player.getStreet(), player.getHouseNumber(), player.getZipCode(), player.getCity(), player.isPaid(), player.getEMail(), player.getMobilePhoneNumber(), player.getPhone(), player.getPointsSeriesOne(), player.getPointsSeriesTwo(), player.getTotalScore() });
	}

	public void addRowOnlyToTeamTable(Team team) {
		for (Player player : team.getPlayers()) {
			defaultTeamTableModel.addRow(new Object[] { (team.getId()), team.getTeamName(), player.getId(), player.getFirstName(), player.getLastName(), team.getPointsSeriesOne(), team.getPointsSeriesTwo(), team.getPlayerSize() });
		}
	}

	public int getPlayerRowByValue(final String value) {
		for (int i = 0; i <= defaultPlayerTableModel.getRowCount(); i++) {
			if (defaultPlayerTableModel.getValueAt(i, 1).equals(value)) {
				return i;
			}
		}
		return -1;
	}

	public int getTeamRowByValue(final String value) {
		for (int i = 0; i <= defaultTeamTableModel.getRowCount(); i++) {
			if (defaultTeamTableModel.getValueAt(i, 1).equals(value)) {
				return i;
			}
		}
		return -1;
	}

	public void removeRowFromPlayer(int row) {
		defaultPlayerTableModel.removeRow(row);
	}

	public void removeRowFromTeam(int row) {
		defaultTeamTableModel.removeRow(row);
	}

	public static TableModelHandler getInstance() {
		return instance;
	}

	public JTable getPlayerTable() {
		return playerTable;
	}

	public JTable getTeamTable() {
		return teamTable;
	}
}
