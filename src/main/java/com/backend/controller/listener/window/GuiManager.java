package com.backend.controller.listener.window;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import com.backend.database.HibernateConfigUtil;
import com.backend.jsonmapper.JSONProperties;
import com.gui.dialogs.ExceptionDialog;
import com.gui.view.MainFrame;

public class GuiManager extends WindowAdapter {
	private MainFrame mainFrame;

	public GuiManager(final MainFrame mainFrame) {
		this.mainFrame = mainFrame;
	}

	@Override
	public void windowClosing(WindowEvent e) {
		System.out.println("Exit App");
		try {
			HibernateConfigUtil.getInstance().close();
		} catch (Exception e0) {
			e0.printStackTrace();
			if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
				ExceptionDialog.showException(e0);
			}
		}

		mainFrame.dispose();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
				ExceptionDialog.showException(e1);
			}
			e1.printStackTrace();
		}
		System.exit(0);
	}

	public void addNewPlayer() {

	}

}
