package com.backend.controller.settings;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.gui.view.settings.SettingsFrame;

public class SettingsListenerController implements ActionListener {

	private SettingsFrame settingsFrame;

	public SettingsListenerController(SettingsFrame settingsFrame) {
		this.settingsFrame = settingsFrame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		settingsFrame.showSettings();
	}

}
