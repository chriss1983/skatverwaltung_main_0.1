package com.backend.controller.models;

import java.util.ArrayList;

import javax.swing.JMenuItem;

import org.apache.log4j.Logger;

import com.backend.controller.contextmenu.AddPlayerToGroupController;
import com.backend.models.ModelsHandler;
import com.backend.pojos.Player;
import com.backend.pojos.TableModelHandler;
import com.backend.pojos.Team;
import com.gui.view.splitpane.tables.AbstractListPanel;

import javafx.collections.ListChangeListener;

public class TeamListController implements ListChangeListener<Team> {
	final static Logger			logger				= Logger.getLogger(TeamListController.class);

	private TableModelHandler	tableModelHandler	= TableModelHandler.getInstance();

	@Override
	public void onChanged(Change<? extends Team> change) {
		while (change.next()) {
			for (Team remitem : change.getRemoved()) {
				logger.debug("delete Team from list: " + remitem.getId());

				ArrayList<Player> list = new ArrayList<Player>();

				remitem.setNewPlayerList(list);

				if (!ModelsHandler.getInstance().getToDeleteTeams().isEmpty()) {
					int rowToDelete = tableModelHandler.getTeamRowByValue(ModelsHandler.getInstance().getToDeleteTeams().get(0).getTeamName());

					if (rowToDelete != -1)
						tableModelHandler.getDefaultTeamTableModel().removeRow(rowToDelete);
				}
			}

			for (Team addItem : change.getAddedSubList()) {
				logger.debug("change.getAddedSubList() " + addItem.getId());
				if (addItem.getPlayers().size() > 0) {
					Player player = addItem.getPlayers().get(addItem.getPlayerSize() - 1);
					tableModelHandler.getDefaultTeamTableModel().addRow(new Object[] { (addItem.getId()), addItem.getTeamName(), player.getId(), player.getFirstName(), player.getLastName(), addItem.getPointsSeriesOne(), addItem.getPointsSeriesTwo(), addItem.getPlayerSize() });
				}
				createAvaibleTeamContextMenu(addItem);
				ModelsHandler.getInstance().getSelectedTeams().removeAll(ModelsHandler.getInstance().getSelectedTeams());
			}
		}
		ModelsHandler.getInstance().getSelectedTeams().removeAll(ModelsHandler.getInstance().getSelectedTeams());
		ModelsHandler.getInstance().getSelectedPlayerList().removeAll(ModelsHandler.getInstance().getSelectedPlayerList());
	}

	private void createAvaibleTeamContextMenu(final Team team) {
		boolean exists = false;
		for (int index = 0; index < AbstractListPanel.toTeamSub.getItemCount() && !exists; index++) {
			if (team.getTeamName().equals(AbstractListPanel.toTeamSub.getItem(index).getText())) {
				exists = true;
			}
		}

		if (!exists) {
			final JMenuItem tmpItem = new JMenuItem(team.getTeamName());
			logger.debug("Add Team " + team.getTeamName() + " to context menu");

			tmpItem.addActionListener(new AddPlayerToGroupController(tmpItem));

			AbstractListPanel.toTeamSub.add(tmpItem);
			AbstractListPanel.toTeamSub.repaint();
			AbstractListPanel.toTeamSub.revalidate();
			// } else {
			logger.debug("Team MenuItem already exists in Contextmenu");
		}
	}
}
