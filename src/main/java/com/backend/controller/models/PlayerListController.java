package com.backend.controller.models;

import org.apache.log4j.Logger;

import com.backend.controller.table.TableController;
import com.backend.models.ModelsHandler;
import com.backend.pojos.Player;
import com.backend.pojos.TableModelHandler;

import javafx.collections.ListChangeListener;

public class PlayerListController implements ListChangeListener<Player> {

	final static Logger			logger				= Logger.getLogger(PlayerListController.class);

	private TableModelHandler	tableModelHandler	= TableModelHandler.getInstance();

	public PlayerListController() {
	}

	@Override
	public void onChanged(Change<? extends Player> change) {
		while (change.next()) {
			for (Player remitem : change.getRemoved()) {
				logger.debug("delete player from list: " + remitem.getId());
				// if (coreManager.deleteEntity(remitem)) {
				// tableModelHandler.getDefaultPlayerTableModel().removeRow();
				TableController.getInstance().onRemovePlayerRow(remitem);
				// tableModelHandler.removeRowFromPlayer(tableModelHandler.getSelectedRowCount());
				// ModelsHandler.getInstance().removePlayer(remitem);
				// RedeyedCoreManager.getDataBaseUtil().deleteEntity(remitem);

				// }
			}
			for (Player additem : change.getAddedSubList()) {
				if (additem != null) {

				}
				// TableController.getInstance().onAddRowToPlayer(additem);

			}
		}
		ModelsHandler.getInstance().getSelectedPlayerList().removeAll(ModelsHandler.getInstance().getSelectedPlayerList());
	}

}
