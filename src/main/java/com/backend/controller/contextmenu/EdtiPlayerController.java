package com.backend.controller.contextmenu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.backend.models.ModelsHandler;
import com.backend.pojos.Player;
import com.gui.view.inputforms.CreateOrUpdatePlayerView;

public class EdtiPlayerController implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		Player tmp = null;
		for (Player p : ModelsHandler.getInstance().getPlayers()) {
			System.out.println("ID = " + p.getId());
			if (p.getId() == ModelsHandler.getInstance().getSelectedPlayerList().get(0).getId()) {
				tmp = p;
				break;
			}
		}
		if (tmp != null) {
			CreateOrUpdatePlayerView.getInstance().setButtonAction("update");
			CreateOrUpdatePlayerView.getInstance().editPlayer(ModelsHandler.getInstance().getSelectedPlayerList().get(0));
		}
		ModelsHandler.getInstance().getSelectedPlayerList().clear();
	}
}
