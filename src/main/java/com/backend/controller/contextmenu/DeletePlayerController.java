package com.backend.controller.contextmenu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.backend.controller.table.TableController;
import com.backend.models.ModelsHandler;
import com.backend.pojos.Player;
import com.backend.pojos.Team;
import com.gui.dialogs.InfoDialog;

public class DeletePlayerController implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("delete");
		if (!ModelsHandler.getInstance().getSelectedPlayerList().isEmpty() && ModelsHandler.getInstance().getSelectedPlayerList().get(0) != null) {
			for (Player player : ModelsHandler.getInstance().getPlayers()) {
				if (player.getId() == ModelsHandler.getInstance().getSelectedPlayerList().get(0).getId()) {
					if (checkPlayerIsInTeam(player)) {
						InfoDialog.showInfoDialog("Bitte den Spieler zuerst aus dem Team entfernen", InfoDialog.WARNING_MESSAGE);
						System.out.println("Bitte Player erst aus der Grupper entfernen!");
						break;
					} else {
						System.out.println("delete Player " + player);
						TableController.getInstance().onRemovePlayerRow(player);
						break;
					}
				}
			}
		} else {
			System.out.println("NO PLAYER TO DELETE");
		}
	}

	private Boolean checkPlayerIsInTeam(Player player) {
		for (Team team : ModelsHandler.getInstance().getTeamList()) {
			for (Player teamPlayer : team.getPlayers()) {

				System.out.println(player.getId() == teamPlayer.getId());
				System.out.print(" " + team.getTeamName());
				if (player.getId() == teamPlayer.getId()) {
					return true;
				}
			}
		}
		return false;
	}
}
