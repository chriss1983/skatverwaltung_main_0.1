package com.backend.controller.contextmenu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.apache.log4j.Logger;

import com.backend.controller.table.TableController;
import com.backend.models.ModelsHandler;
import com.backend.pojos.Player;
import com.backend.pojos.Team;

public class RemoveFromTeamController implements ActionListener {
	private final static Logger	logger	= Logger.getLogger(RemoveFromTeamController.class);
			
	Team	selectedTeam	= null;
	Player	playerTmp		= null;

	@Override
	public void actionPerformed(ActionEvent e) {
		
//		if (ModelsHandler.getInstance().getSelectedTeams().get(0) != null) {
			for (Team team : ModelsHandler.getInstance().getTeamList()) {
				if (!ModelsHandler.getInstance().getSelectedTeams().isEmpty() && team.getId() == ModelsHandler.getInstance().getSelectedTeams().get(0).getId()) {
					logger.debug("Delete team " + team);

					selectedTeam = team;
					playerTmp = team.getPlayer(0);
					break;
				}
			}
			TableController.getInstance().onRemoveTeamRow(selectedTeam);
			
//		}
	}
}
