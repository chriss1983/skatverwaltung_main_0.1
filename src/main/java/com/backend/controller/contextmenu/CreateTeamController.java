package com.backend.controller.contextmenu;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.log4j.Logger;

import com.backend.models.ModelsHandler;
import com.backend.pojos.Team;
import com.gui.view.MainFrame;

public class CreateTeamController implements ActionListener {
	final static Logger	logger	= Logger.getLogger(CreateTeamController.class);

	private MainFrame	mainFrame;
	private JMenu		toTeamSub;

	public CreateTeamController(MainFrame mainframe, JMenu toTeamSub) {
		this.mainFrame = mainframe;
		this.toTeamSub = toTeamSub;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		final JDialog newPlayerDialog = new JDialog(mainFrame, Dialog.ModalityType.APPLICATION_MODAL);

		final String title = "New Team";

		newPlayerDialog.setLayout(new BorderLayout());
		newPlayerDialog.setTitle(title);
		newPlayerDialog.setSize(400, 75);
		newPlayerDialog.setResizable(false);
		newPlayerDialog.setLocationRelativeTo(null);

		JLabel enterTeamNameHint = new JLabel("Enter the team name");
		final JTextField enterTeamNameField = new JTextField();
		final JButton enterTeamNameButton = new JButton("Create Team");

		enterTeamNameButton.setEnabled(false);
		enterTeamNameField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				enterTeamNameButton.setEnabled(enterTeamNameField.getText().length() != 0 && !isTeamExists());
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				System.out.println("value has changed in IntertUpdate");
				enterTeamNameButton.setEnabled(enterTeamNameField.getText().length() != 0 && !isTeamExists());
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				System.out.println("value has changed in changeUpdate()");
				isTeamExists();

			}

			private Boolean isTeamExists() {
				for (Team team : ModelsHandler.getInstance().getTeamList()) {
					// System.out.println(entry.getKey() + "/" +
					// entry.getValue());
					if (enterTeamNameField.getText().equals(team.getTeamName())) {
						newPlayerDialog.setTitle(title + " Team already exists");
						return true;
					}
				}
				newPlayerDialog.setTitle(title);
				newPlayerDialog.repaint();
				return false;
			}
		});

		enterTeamNameButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				logger.debug(enterTeamNameField.getText());
				newPlayerDialog.dispose();

				final Team newTeam = new Team();
				newTeam.setTeamName(enterTeamNameField.getText());
				newTeam.addPlayer(ModelsHandler.getInstance().getSelectedPlayerList());

				ModelsHandler.getInstance().addPlayersToTeam(ModelsHandler.getInstance().getSelectedPlayerList(), newTeam);
//				TableController.getInstance().onAddRowToTeam(newTeam);

				final JMenuItem tmpItem = new JMenuItem();

				toTeamSub.add(tmpItem);
				toTeamSub.repaint();
				toTeamSub.revalidate();

				ModelsHandler.getInstance().getSelectedPlayerList().clear();
			}
		});

		Box contendBox = Box.createHorizontalBox();
		contendBox.add(Box.createVerticalGlue());
		contendBox.add(enterTeamNameHint);
		contendBox.add(enterTeamNameField);
		contendBox.add(enterTeamNameButton);
		contendBox.add(Box.createVerticalGlue());
		newPlayerDialog.add(contendBox);
		newPlayerDialog.setVisible(true);

	}
}
