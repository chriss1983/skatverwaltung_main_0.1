package com.backend.controller.contextmenu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.backend.models.ModelsHandler;
import com.backend.pojos.Player;
import com.gui.dialogs.InfoDialog;
import com.gui.view.inputforms.CreateOrUpdatePlayerView;

public class CreateOrUpdatePlayerController implements ActionListener {

	private Player user;

	@Override
	public void actionPerformed(ActionEvent e) {
		user = CreateOrUpdatePlayerView.getInstance().getPlayer();

		if (e.getActionCommand().equals("update")) {
			user.setId(CreateOrUpdatePlayerView.getInstance().getStartNumber());
		}

		if (user != null) {

			if (!(user.getFirstName().equals("") || user.getLastName().equals("") || user.getZipCode().equals("") || user.getCity().equals("") || user.getStreet().equals("") || user.getHouseNumber().equals("") || user.getEMail().equals(""))) {
				String cmd = e.getActionCommand();
				if (cmd.equals("update")) {
					ModelsHandler.getInstance().updatePlayer(user);
					// TableModelHandler.getInstance().

				} else if (cmd.equals("save")) {
					ModelsHandler.getInstance().addPlayer(user);
				}

				CreateOrUpdatePlayerView.getInstance().cleanFields();
				CreateOrUpdatePlayerView.getInstance().close();
			}
			
		} else {
			InfoDialog.showInfoDialog("Der Spieler konnte nicht angelegt werden", InfoDialog.ERROR_MESSAGE);
		}
	}

}
