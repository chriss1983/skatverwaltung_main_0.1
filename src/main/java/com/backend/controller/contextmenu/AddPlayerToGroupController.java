package com.backend.controller.contextmenu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JMenuItem;

import org.apache.log4j.Logger;

import com.backend.models.ModelsHandler;
import com.backend.pojos.Player;
import com.backend.pojos.Team;

public class AddPlayerToGroupController implements ActionListener {
	private final static Logger	logger	= Logger.getLogger(AddPlayerToGroupController.class);

	private JMenuItem			tmpItem;

	public AddPlayerToGroupController(JMenuItem tmpItem) {
		this.tmpItem = tmpItem;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		logger.debug("SELECTED TEAM : " + tmpItem.getText());
		Team tmpTeam = ModelsHandler.getInstance().getTeam(tmpItem.getText());

		List<Player> players = ModelsHandler.getInstance().getSelectedPlayerList();

		if (tmpTeam != null) {
			for (Player player : players) {
				player.setInTeam(true);
				player.setCurrentTeamId(tmpTeam.getId());
			}
		}

		ModelsHandler.getInstance().addPlayersToTeam(players, tmpTeam);
		ModelsHandler.getInstance().cleanSelectedPlayer();
	}
}
