package com.backend.controller.contextmenu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.backend.models.ModelsHandler;
import com.gui.view.inputforms.CreateOrUpdatePlayerView;

public class NewPlayerController implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		ModelsHandler.getInstance().getSelectedPlayerList().clear();
		CreateOrUpdatePlayerView.getInstance().setButtonAction("save");
		CreateOrUpdatePlayerView.getInstance().open();
	}
}
