package com.backend.controller.table;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.backend.models.ModelsHandler;
import com.backend.pojos.Player;
import com.backend.pojos.TableModelHandler;
import com.backend.pojos.Team;

public class TableController {
	final static Logger					logger		= Logger.getLogger(TableController.class);

	private static TableController		instance	= new TableController();

	private List<TableActionListener>	tableActionListener;

	private TableController() {
		tableActionListener = new ArrayList<>();
		tableActionListener.add(TableListener.getInstance());
	}

	public void addActionListener(TableActionListener actionListener) {
		tableActionListener.add(actionListener);
	}

	public void removeActionListener(TableActionListener actionListener) {
		tableActionListener.remove(actionListener);
	}

	public void onAddRowToTeam(Team team) {
		logger.debug("onAddRowToTeam " + team);
		for (TableActionListener actionListener : tableActionListener) {
			actionListener.onAddRowToTeam(team);
		}
		ModelsHandler.getInstance().cleanSelectedPlayer();
	}

	public void onAddRowToPlayer(Player player) {
		logger.debug("onAddRowToPlayer " + player);
		for (TableActionListener actionListener : tableActionListener) {
			actionListener.onAddRowToPlayer(player);
		}
	}

	public void onUpdatePlayerRow(Player updatedUser) {
		logger.debug("onUpdatePlayerRow " + updatedUser);
		for (TableActionListener actionListener : tableActionListener) {
			actionListener.onUpdatePlayerRow(updatedUser);
		}
	}

	public void onUpdateTeamRow(Team updatedTeam) {
		logger.debug("onUpdateTeamRow " + updatedTeam);
		for (TableActionListener actionListener : tableActionListener) {
			actionListener.onUpdateTeamRow(updatedTeam);
		}
	}

	public void onRemovePlayerRow(Player player) {
		logger.debug("onRemovePlayerRow " + player);
		tableActionListener.get(0).onRemovePlayerRow(player);
		ModelsHandler.getInstance().cleanSelectedPlayer();
		TableModelHandler.getInstance().getDefaultPlayerTableModel().fireTableDataChanged();
	}

	public void onRemoveTeamRow(Team team) {
		logger.debug("onRemovePlayerRow " + team);
		for (TableActionListener actionListener : tableActionListener) {
			actionListener.onRemoveTeamRow(team);
		}
		ModelsHandler.getInstance().cleanSelectedTeam();
	}

	public void onAddPlayersToTeam(List<Player> players, Team team) {
//		logger.debug("onAddPlayerToTeam " + player + "\n" + team);
		for (TableActionListener actionListener : tableActionListener) {
			actionListener.onAddPlayersToTeam(players, team);
		}
		ModelsHandler.getInstance().cleanSelectedTeam();
		ModelsHandler.getInstance().cleanSelectedPlayer();
	}

	public static TableController getInstance() {
		return instance;
	}

}
