package com.backend.controller.table;

import java.util.List;

import com.backend.pojos.Player;
import com.backend.pojos.Team;

public interface TableActionListener {

	public void onAddRowToTeam(Team team);

	public void onAddRowToPlayer(Player player);

	public void onUpdatePlayerRow(Player updatedUser);

	public void onUpdateTeamRow(Team updatedTeam);

	public void onRemovePlayerRow(Player player);
	
	public void onRemoveTeamRow(Team team);
	
	public void onAddPlayersToTeam(List<Player> player, Team team);

}
