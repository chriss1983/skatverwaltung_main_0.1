package com.backend.controller.table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JMenuItem;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.log4j.Logger;

import com.backend.controller.contextmenu.AddPlayerToGroupController;
import com.backend.database.RedeyedCoreManager;
import com.backend.jsonmapper.JSONLanguageManager;
import com.backend.models.ModelsHandler;
import com.backend.pojos.Player;
import com.backend.pojos.TableModelHandler;
import com.backend.pojos.Team;
import com.gui.dialogs.InfoDialog;
import com.gui.view.splitpane.tables.AbstractListPanel;

public class TableListener implements TableActionListener {

	private final static Logger		logger		= Logger.getLogger(ModelsHandler.class);

	private RedeyedCoreManager		coreManager	= RedeyedCoreManager.getDataBaseUtil();
	private static TableListener	instance	= new TableListener();

	private TableListener() {
	}

	@Override
	public void onAddRowToTeam(Team team) {
		coreManager.save(team);
		ModelsHandler.getInstance().addTeam(team);
		ModelsHandler.getInstance().cleanSelectedPlayer();
	}

	@Override
	public void onAddRowToPlayer(Player player) {
		coreManager.save(player);
		// dTableModelHandler.getInstance().addPlayer(player);
	}

	@Override
	public void onUpdatePlayerRow(Player updatedUser) {
		// TableModelHandler.getInstance().
		coreManager.update(updatedUser);
		ModelsHandler.getInstance().updatePlayer(updatedUser);
	}

	@Override
	public void onUpdateTeamRow(Team updatedTeam) {
		coreManager.update(updatedTeam);
	}

	@Override
	public void onRemovePlayerRow(Player player) {
		List<String> conditions = new ArrayList<>();
		conditions.add("ID = '" + player.getId() + "'");
		// List<Player> players =
		// RedeyedCoreManager.getDataBaseUtil().getAllEntities(Player.class,
		// conditions, null, null, null);

		// if (!players.isEmpty()) {
		// if (players.get(0).getId() == player.getId()) {
		TableModelHandler.getInstance().removeRowFromPlayer(TableModelHandler.getInstance().getSelectedRowCount());
		ModelsHandler.getInstance().removePlayer(player);
		RedeyedCoreManager.getDataBaseUtil().deleteEntity(player);
		// }
		// }
	}

	@Override
	public void onRemoveTeamRow(Team team) {
		List<String> conditions = new ArrayList<>();
		conditions.add("ID = '" + team.getId() + "'");

		List<Team> tmpTeam = RedeyedCoreManager.getDataBaseUtil().getAllEntities(Team.class, conditions, null, null, null);
		if (tmpTeam != null && tmpTeam.size() > 0) {
			coreManager.deleteEntity(team);
			TableModelHandler.getInstance().removeRowFromTeam(TableModelHandler.getInstance().getSelectedRowCount());
			ModelsHandler.getInstance().removeTeam(team);
			ModelsHandler.getInstance().cleanSelectedTeam();
		}
	}

	@Override
	public void onAddPlayersToTeam(List<Player> players, Team team) {
		Team tmpTeam = null;

		List<String> conditions = new ArrayList<>();
		conditions.add("ID = '" + team.getId() + "'");
		List<Team> dbTeams = RedeyedCoreManager.getDataBaseUtil().getAllEntities(Team.class, conditions, null, null, null);

		for (Team tmpList : ModelsHandler.getInstance().getTeamList()) {
			if (tmpList.getTeamName() != null && tmpList.getTeamName().equals(team.getTeamName())) {
				tmpTeam = tmpList;
				break;
			}
		}
		for (Player player : players) {
			logger.debug("Adding Player with ID : " + player.getId());
			player.setInTeam(true);
			player.setCurrentTeamId(team.getId());

			if (tmpTeam == null) {
				tmpTeam = new Team();
				tmpTeam.setTeamName(team.getTeamName());
			}

			boolean playerAlreadyInOtherTeam = false;

			Player playerAlreadyInTeam = null;
			Team playerfoundInTeam = null;

			for (Team teamList : ModelsHandler.getInstance().getTeamList()) {
				for (Player playerInList : teamList.getPlayers())
					if (player.getId() == playerInList.getId()) {
						playerAlreadyInTeam = playerInList;
						playerfoundInTeam = teamList;
						playerAlreadyInOtherTeam = true;
						break;
					}
			}

			int reason;

			if (!playerAlreadyInOtherTeam) {
				reason = tmpTeam.addPlayer(player);
				createAvaibleTeamContextMenu(tmpTeam);
			} else {
				reason = Team.PLAYER_IS_IN_OTHER_TEAM;
			}

			switch (reason) {
				case Team.PLAYER_ADDED:
					if (!dbTeams.isEmpty()) {
						logger.debug("Found " + dbTeams);
					}

					break;
				case Team.MAX_PLAYERS_REACHED:
					InfoDialog.showInfoDialog("Es ist bereits die Maximale Anzahl an Spielern pro Team erreicht", InfoDialog.INFORMATION_MESSAGE);
					break;
				case Team.PLAYER_IS_IN_TEAM:
					InfoDialog.showInfoDialog("Der Spieler ist bereits im Team", InfoDialog.INFORMATION_MESSAGE);
					break;
				case Team.PLAYER_IS_IN_OTHER_TEAM:
					Map<String, Object> data = new HashMap<>();
					data.put("playerFirstname", playerAlreadyInTeam.getLastName());
					data.put("playerLastname", playerAlreadyInTeam.getLastName());
					data.put("playerId", playerAlreadyInTeam.getId());
					data.put("teamName", playerfoundInTeam.getTeamName());

					InfoDialog.showInfoDialog(StrSubstitutor.replace(JSONLanguageManager.getLanguageProperty().getStringValues().getValues().get("alreadyInOtherTeam"), data), InfoDialog.INFORMATION_MESSAGE);
					break;
				default:
					InfoDialog.showInfoDialog("Ein unbekannter Fehler ist aufgetretten", InfoDialog.INFORMATION_MESSAGE);
					break;

			}
		}

		coreManager.save(tmpTeam);
		TableModelHandler.getInstance().addRowOnlyToTeamTable(tmpTeam);
		ModelsHandler.getInstance().cleanSelectedPlayer();
	}
	
	private void createAvaibleTeamContextMenu(final Team team) {
		boolean exists = false;
		for (int index = 0; index < AbstractListPanel.toTeamSub.getItemCount() && !exists; index++) {
			if (team.getTeamName().equals(AbstractListPanel.toTeamSub.getItem(index).getText())) {
				exists = true;
			}
		}

		if (!exists) {
			final JMenuItem tmpItem = new JMenuItem(team.getTeamName());
			logger.debug("Add Team " + team.getTeamName() + " to context menu");

			tmpItem.addActionListener(new AddPlayerToGroupController(tmpItem));

			AbstractListPanel.toTeamSub.add(tmpItem);
			AbstractListPanel.toTeamSub.repaint();
			AbstractListPanel.toTeamSub.revalidate();
			// } else {
			logger.debug("Team MenuItem already exists in Contextmenu");
		}
	}

	public static TableListener getInstance() {
		return instance;
	}
}
