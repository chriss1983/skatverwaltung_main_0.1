package com.backend.controller.activation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.backend.jsonmapper.JSONProperties;
import com.backend.security.KeyHandler;
import com.gui.view.security.ActivationView;
import com.skat_haan_gruiten.StartSkatManager;

public class ActivationKeyController implements ActionListener {
	private final static Logger	logger	= Logger.getLogger(ActivationKeyController.class);

	private ActivationView		activationView;
	private StartSkatManager	skatManager;
	private KeyHandler			securityHandler;

	public ActivationKeyController(StartSkatManager skatManager, ActivationView activationView, KeyHandler securityHandler) {
		this.skatManager = skatManager;
		this.activationView = activationView;
		this.securityHandler = securityHandler;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (activationView.getActivationCode() != null) {

			String key = activationView.getActivationCode();
			key = key.replaceAll("-", "");
			logger.debug("key = " + key);

			if (activationView.getActivationCode().replaceAll("-", "").length() == 16) {
				if (securityHandler.verify(activationView.getActivationCode().replaceAll("-", ""), skatManager.PUBLIC_KEY)) {
					JSONProperties.getJSonProperty().getSecurity().setActivationKey(activationView.getActivationCode());
					try {
						JSONProperties.saveProperties();
					} catch (IOException e1) {
						logger.error(e1.getStackTrace());
					}
					activationView.showOrHide("hide");
					skatManager.init();
				}
			}
		}
	}
}
