package com.backend.security;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.backend.jsonmapper.JSONProperties;
import com.backend.server.connector.RedeyedServerConnector;
import com.backend.server.connector.RequestObj;
import com.backend.server.model.UserDetailsEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SecurityManager {
	private final Logger			logger	= LoggerFactory.getLogger(SecurityManager.class);

	private RequestObj				requestObj;
	private Map<String, Object>		params;
	private static Gson				GSON;

	private static SecurityManager	securityManager;

	private SecurityManager() {
		params = new HashMap<String, Object>();
		GSON = new GsonBuilder().create();
	}

	// Test User
	private UserDetailsEntity createTestUser() {

		UserDetailsEntity user = new UserDetailsEntity();
		user.setFirstName("Christian");
		user.setLastName("Richter");
		user.setEmail("christian.richter@ish.de");

		return user;
	}

	private void sendUserToBackend(UserDetailsEntity user) throws Exception {
		RequestObj requestObj = new RequestObj();
		requestObj.setRequestType("Z8LSq0wWwB5v+6YJzurc");

		String crypt = CryptUtil.encrypt(GSON.toJson(user), JSONProperties.getJSonProperty().getSecurity().getSecureKey());

		params.clear();
		params.put("USER", crypt);
		requestObj.setParams(params);
		JSONObject uiObj = RedeyedServerConnector.getData(requestObj, "/skatmanager/getActivationCode");

		System.out.println("UIOBJ" + uiObj);
	}

	private Boolean checkActivation() {
		try {
			requestObj = new RequestObj();
			params.clear();
			params.put("GET_KEY", CryptUtil.encrypt("Z8LSq0wWwB5v33481384", JSONProperties.getJSonProperty().getSecurity().getSecureKey()));
			params.put("PUBLIC_KEY", GSON.toJson(JSONProperties.getJSonProperty().getSecurity().getPublicKey()));

			requestObj.setRequestType("Z8LSq0wWwB5v33481384");
			requestObj.setParams(params);

			JSONObject uiObj = RedeyedServerConnector.getData(requestObj, "/data/keyagreement");

			if (Boolean.parseBoolean(uiObj.get("KEY_AGREEMENT").toString())) {
				logger.debug("KeyAgreement : " + uiObj.get("KEY_AGREEMENT"));

				String activationCode = JSONProperties.getJSonProperty().getSecurity().getActivationKey();
				params.clear();
				logger.debug("Activation Code = " + activationCode);
				params.put("ACTIVATION_CODE", activationCode);
				params.put("PUBLIC_KEY", JSONProperties.getJSonProperty().getSecurity().getPublicKey());
				requestObj.setParams(params);
				uiObj = null;
				uiObj = RedeyedServerConnector.getData(requestObj, "/data/activator");

				logger.debug(uiObj.get("ACTIVATION_STATE").equals("OK") ? "AKTIVATION_STATE = OK" : "AKTIVATION_STATE = NOK");
				return uiObj.get("ACTIVATION_STATE").equals("OK");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static SecurityManager getInstance() {
		if (securityManager == null) {
			securityManager = new SecurityManager();
		}
		return securityManager;
	}

	public static void main(String args[]) throws Exception {

		String key = "Z8LSq0wWwB5v+6YJzurcP463H3F12iZh74fDj4S74oUH4EONkiKb2FmiWUbtFh97GG/c/lbDE47mvw6j94yXxKHOpoqu6zpLKMKPcOoSppcVWb2q34qENBJkudXUh4MWcreondLmLL2UyydtFKuU9Sa5VgY/CzGaVGJABK2ZR94=";
		System.out.println(CryptUtil.encrypt("2OVSS1V50ACU17D5", key));
		SecurityManager.getInstance().createTestUser();

		SecurityManager.getInstance().sendUserToBackend(SecurityManager.getInstance().createTestUser());
		System.out.print("Activation was : ");
		if (SecurityManager.getInstance().checkActivation()) {
			System.out.println("Successfull");
		} else {
			System.out.println("not Successfull");
		}
	}

}
