package com.backend.security;

import java.math.BigInteger;
import java.util.Random;

import com.backend.jsonmapper.JSONProperties;
import com.gui.dialogs.ExceptionDialog;

public class KeyHandler {
	private static final BigInteger	E	= BigInteger.valueOf(3L);

	final Random					r	= new Random();

	public KeyHandler() {}

	public static class KeyPair {

		public final BigInteger	publicKey;
		public final BigInteger	privateKey;

		public KeyPair(final BigInteger n, final BigInteger d) {
			this.publicKey = n;
			this.privateKey = d;
		}
	}

	public KeyPair genKeyPair() {

		int nTries = 0;
		do {
			try {
				boolean hasPrime = false;

				BigInteger p = BigInteger.ZERO;
				while (!hasPrime) {
					p = BigInteger.probablePrime(28, r);
					hasPrime = p.isProbablePrime(8);
				}

				hasPrime = false;
				BigInteger q = BigInteger.ZERO;
				while (!hasPrime) {
					q = BigInteger.probablePrime(28, r);
					hasPrime = p.isProbablePrime(8) && !(q.equals(p));
				}

				final BigInteger n = p.multiply(q);

				final BigInteger eTot = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
				final BigInteger d = E.modInverse(eTot);

				return new KeyPair(n, d);
			} catch (final ArithmeticException ex) {
				if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
					ExceptionDialog.showException(ex);
				}
			}
		} while (nTries++ < 20);

		throw new RuntimeException("Konnte kein Schluesselpaar erzeugen.");
	}

	public BigInteger hash(final BigInteger number) {
		return number.mod(BigInteger.valueOf(8379));
	}

	public BigInteger sign(final BigInteger number, final KeyPair kp) {
		return hash(number).modPow(kp.privateKey, kp.publicKey);
	}

	public boolean verify(final BigInteger message, final BigInteger signature, final BigInteger publicKey) {
		return hash(message).equals(signature.modPow(E, publicKey));
	}

	public String generateSerial(final KeyPair kp, final Random r) {
		int nTries = 0;
		do {
			final BigInteger message = BigInteger.probablePrime(28, r);
			if (message.toString(36).length() != 6) {
				continue;
			}

			final BigInteger sign = sign(message, kp);
			if (sign.toString(36).length() != 10) {
				continue;
			}

			return (message.toString(36) + sign.toString(36)).toUpperCase();
		} while (nTries++ < 100);

		throw new RuntimeException("Konnte keine Seriennnummer generieren");
	}

	public boolean verify(final String serial, final BigInteger publicKey) {
		return verify(new BigInteger(serial.substring(0, 6), 36), new BigInteger(serial.substring(7, 16), 36), publicKey);
	}

	public static void main(String[] args) {

		final KeyHandler s = new KeyHandler();

		final KeyPair kp = s.genKeyPair();
		System.out.println(String.format("Ein Schlüsselpaar...\n%1$s:%2$s\n%3$s:%4$s", "private", kp.privateKey, "public", kp.publicKey));

		final Random r = new Random();

		System.out.println("\nEin paar Seriennummern...");
		for (int i = 0; i < 20; i++) {
			final String serial = s.generateSerial(kp, r);

			System.out.println(serial + (s.verify(serial, kp.publicKey) ? " (ok)" : " NOK!"));
		}
		System.out.println();

		char[] str = "44S0-PVMW-GXQ9-ACT5".toCharArray();
		String newStr = "";

		for (int i = 0; i < str.length; i++) {
			if (i != 0) {
				if (str[i] == '-') {
					i++;
					System.out.print("-");
				}
			}
			newStr += str[i];
			System.out.print(newStr);
		}

		// System.out.println(str.activationCode.substring(0, 4) + "-" +
		// str.substring(4, 8) +
		// "-" + str.substring(8, 12) + "-" + str.substring(12, 16));
		// String key = str.replace("-", "");
		// System.out.println(key2T50GH7YYNNE1PVW);

		// System.out.println(s.verify("2EHMX56IXJT3FX4S",
		// BigInteger.valueOf(40094985638641183L)));
		/*
		 * private:24373031750372747 public:36559548014518483
		 * Ein paar Seriennummern... 44S0PVMWGXQ9ACT5 (ok) 4BXRTJGQ5M53UQSH (ok)
		 * 3VSM3VHWDYKECJYY (ok) 2PMRZDVR30QS2I71 (ok) 49OMSHITBHIPQ2DC (ok)
		 * 343NQHI7858CLM1Q (ok) 2HETMP8A25C4XA9N (ok) 2NAOKN5DW7584USL (ok)
		 * 2Q8D4Z4L269L4LE0 (ok) 49ZJVTIJMU9I2JWE (ok) 4E2YNPASKM5U57QO (ok)
		 * 2V98HJ8T5ZJ44YZ5 (ok) 3OLY1NB7E8I8WPM3 (ok) 2T50GH7YYNNE1PVW (ok)
		 * 3GDFIT5RV4VKUZTJ (ok) 2IUW3V3VEEPSDLDE (ok) 40AW11U9FZQ75FMP (ok)
		 * 3CVWU5TQMGR0P2L2 (ok) 2YJGZPSFJE2P65QA (ok) 31MHYJE3QJZ6ZNHV (ok)
		 */
	}
}
