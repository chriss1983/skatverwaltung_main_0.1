package com.backend.errorhandling;

public class EmptyStingException extends Exception {
	private static final long serialVersionUID = 1L;

	public EmptyStingException(String message) {
		super(message);
	}
}