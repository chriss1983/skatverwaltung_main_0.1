package com.backend.database;

import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.backend.database.utils.ReflectionUtils;
import com.backend.jsonmapper.JSONProperties;

public class HibernateConfigUtil {
	final static Logger					logger	= Logger.getLogger(HibernateConfigUtil.class);
	private static SessionFactory		sessionFactory;
	private static ServiceRegistry		serviceRegistry;
	private static HibernateConfigUtil	instance;

	private SessionFactory buildSessionConfigFactory() {
		try {
			Configuration configuration = new Configuration();
			Properties props = new Properties();

			props.put("hibernate.dialect", "org.hibernate.dialect." + JSONProperties.getJSonProperty().getDatabase().getDialect());
			props.put("hibernate.connection.driver_class", JSONProperties.getJSonProperty().getDatabase().getDriver());
			props.put("hibernate.connection.url", JSONProperties.getJSonProperty().getDatabase().getDatabaseURL());
			props.put("hibernate.connection.username", JSONProperties.getJSonProperty().getDatabase().getUsername());
			props.put("hibernate.connection.password", JSONProperties.getJSonProperty().getDatabase().getPassword());
			props.put("hibernate.current_session_context_class", "thread");
			props.put("hibernate.hbm2ddl.auto", "update");

			configuration.setProperties(props);
			logger.info("Hibernate Configuration loaded");

			@SuppressWarnings("rawtypes")
			Set<Class> allClasses = ReflectionUtils.getAllClasses("com.backend.pojos");
			if (allClasses != null) {
				for (Class<?> clazz : allClasses) {
					configuration.addAnnotatedClass(clazz);
					System.out.println("Add Class -> " + clazz.getName());
				}
			} else {
				System.out.println("ERROR CLASSES");
			}

			logger.debug("Hibernate Java Config serviceRegistry created");

			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
			SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);

			return sessionFactory;
		} catch (Throwable ex) {
			logger.error("Initial SessionFactory creation failed." + ex.getMessage());
			throw new ExceptionInInitializerError(ex);
		}
	}

	public SessionFactory getSessionConfigFactory() {
		logger.info("getSessionConfigFactory()");
		if (sessionFactory == null) {
			sessionFactory = buildSessionConfigFactory();
			sessionFactory.openSession();

		}
		return sessionFactory;
	}

	public void close() throws Exception {
		if (serviceRegistry != null) {
			StandardServiceRegistryBuilder.destroy(serviceRegistry);
		}
	}

	public static HibernateConfigUtil getInstance() {
		if (instance == null) {
			instance = new HibernateConfigUtil();
		}
		return instance;
	}
}
