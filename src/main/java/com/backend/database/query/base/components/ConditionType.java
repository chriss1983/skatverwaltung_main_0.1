package com.backend.database.query.base.components;

@Deprecated
public enum ConditionType {
	LESS_THAN(" < "), GRATER_THAN(" > "), EQUAL(" = "), NOT_EQUAL(" != "), LESS_THAN_EQUAL(" <= "), GREATER_THAN_EQUAL(" <= "), BETWEEN(" <> "), LIKE(" %% ");
	private String conditionType;

	ConditionType(String conditionType) {
		this.conditionType = conditionType;
	}

	public String getConditionType() {
		return conditionType;
	}
}