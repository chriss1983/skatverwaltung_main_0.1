package com.backend.database.query.base.components;

public class Column {
	private Class<?>	entityClass;
	private String		variableName;

	public Class<?> getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(Class<?> entityClass) {
		this.entityClass = entityClass;
	}

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String columnName) {
		this.variableName = columnName;
	}

}
