package com.backend.database;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.backend.database.manager.DatabaseManager;
import com.backend.database.query.base.components.GroupBy;
import com.backend.database.query.base.components.OrderBy;
import com.backend.database.query.base.components.Pagination;
import com.backend.jsonmapper.JSONProperties;
import com.backend.pojos.Player;
import com.backend.pojos.Team;
import com.gui.dialogs.ExceptionDialog;

// @Repository
public class RedeyedCoreManager {
	final static Logger					logger	= Logger.getLogger(RedeyedCoreManager.class);
	private DatabaseManager				databaseManager;
	private static RedeyedCoreManager	dataBaseUtil;

	private SessionFactory				sessionFactory;

	private Session						session;

	private RedeyedCoreManager() {
		logger.info("Init DateBaseUtil");
		this.databaseManager = new DatabaseManager();
		this.sessionFactory = HibernateConfigUtil.getInstance().getSessionConfigFactory();

		session = sessionFactory.openSession();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getAllEntities(Class<T> clazz, List<String> conditions, OrderBy orderBy, Pagination pagination, GroupBy groupBy) {
		databaseManager.onGetAllEntities();
		try {
			String queryString = "FROM " + clazz.getSimpleName() + getConditions(conditions) + getGroupBy(groupBy) + getOrderBy(orderBy);

			if (!session.isOpen()) {
				session = sessionFactory.openSession();
			}
			Query q = session.createQuery(queryString);
			setPagination(q, pagination, queryString);
			return q.list();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ArrayList<T>();
		}
	}

	private String getGroupBy(GroupBy groupBy) {
		if (groupBy != null) {
			return " GROUP BY " + groupBy.getVariableName() + " ";
		} else {
			return "";
		}
	}

	private void setPagination(Query query, Pagination pagination, String queryString) {
		if (pagination != null && pagination.getPageNumber() != null && pagination.getPageSize() != null) {
			query.setFirstResult((pagination.getPageNumber() - 1) * pagination.getPageSize());
			query.setMaxResults(pagination.getPageSize());
			pagination.setTotalRows(getRowCount(queryString));
		}
	}

	private String getOrderBy(OrderBy orderBy) {
		if (orderBy != null) {
			return " ORDER BY " + orderBy.getVariableName() + " " + (orderBy.isSortAsc() == true ? "ASC" : "DESC");
		} else {
			return "";
		}
	}

	private String getConditions(List<String> conditions) {
		if (conditions != null) {
			StringBuilder conditionsStr = new StringBuilder(" WHERE ");
			for (String condition : conditions)
				conditionsStr.append(condition).append(" AND ");
			return conditionsStr.substring(0, conditionsStr.length() - 5);
		}
		return "";
	}

	public boolean save(Object object) {
		databaseManager.onSaveOrUpdate(object);
		try {
			session.getTransaction().begin();
			// session.merge(object);
			session.save(object);

			// session.saveOrUpdate(object);
			session.getTransaction().commit();
		} catch (Exception ex) {
			// logger.error(ex.getCause().getMessage(), ex);
		}
		return true;
	}

	public void update(Object object) {
		// databaseManager.onUpdatePlayer(playerData);
		try {
			if (!session.isOpen()) {
				session = sessionFactory.openSession();
			}
			session.merge(object);

		} catch (HibernateException e) {
			if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
				ExceptionDialog.showException(e);
			}

		} finally {
			session.close();
		}
	}

	public void updateTeam(Team teamData, Integer teamID) {
		databaseManager.onUpdatePlayer(teamData, teamID);
		Transaction tx = null;
		try {
			tx = session.beginTransaction();

			Team team = getEntity(Team.class, teamID);

			logger.debug("update entity from Database: " + teamData.toString());

			// team.setId(teamData.getId());
			// team.setPointsSeriesOne(teamData.getPointsSeriesOne());
			// team.setPointsSeriesTwo(teamData.getPointsSeriesTwo());
			// team.setTotalScore(teamData.getTotalScore());
			// team.setTeamName(teamData.getTeamName());
			// team.setNewPlayerList(teamData.getPlayers());

			session.merge(team);
			tx.commit();

		} catch (HibernateException e) {

			if (tx != null)

				tx.rollback();

			e.printStackTrace();

			if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
				ExceptionDialog.showException(e);
			}

		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public <T> T getEntity(Class<T> clazz, int entityId) {
		databaseManager.onGetEntity(clazz, entityId);
		try {
			session.getTransaction().begin();
			session.get(clazz, entityId);
			return (T) session.get(clazz, entityId);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return null;
		}
	}

	public boolean deleteEntity(Class<?> clazz, int entityId) {
		databaseManager.onDeleteEntity(clazz, entityId);
		try {
			session.getTransaction().begin();
			session.delete(getEntity(clazz, entityId));
			session.getTransaction().commit();
			return true;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return false;
		}
	}

	@Transactional
	public boolean deleteEntity(Object object) {
		databaseManager.onDeleteEntity(object);
		try {
			if (!session.isOpen()) {
				session = sessionFactory.openSession();
			}
			session.getTransaction().begin();
			session.delete(object);
			session.getTransaction().commit();
			return true;
		} catch (Exception ex) {
			// Log the exception here
			session.getTransaction().rollback();
			return false;
		} finally {
			try {
				HibernateConfigUtil.getInstance().close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public Integer insertEntity(Object entity) {

		databaseManager.onInsertEntity(entity);
		logger.debug("Insert new Entity to Database: " + entity.toString());
		session.getTransaction().begin();
		Integer id = (Integer) session.save(entity);
		session.getTransaction().commit();
		return id;
	}

	private <T> int getRowCount(String queryString) {
		String query = "SELECT COUNT(*) " + queryString;
		return ((Long) this.sessionFactory.getCurrentSession().createQuery(query).uniqueResult()).intValue();
	}

	public <T> int getRowCount(Class<T> clazz) {
		return getRowCount("FROM " + clazz.getSimpleName());
	}

	public Boolean checkIfPlayerExists(Player p) {
		List<Player> players = RedeyedCoreManager.getDataBaseUtil().getAllEntities(Player.class, null, null, null, null);

		for (Player player : players) {
			if (p.getId() == player.getId()) {
				return true;
			}
		}

		return false;
	}

	public static RedeyedCoreManager getDataBaseUtil() {
		if (dataBaseUtil == null) {
			dataBaseUtil = new RedeyedCoreManager();
		}
		return dataBaseUtil;
	}

	public void shutdown() throws SQLException {
		sessionFactory.close(); // if there are no other open connection
	}

	public void closeSession() {

	}
}
