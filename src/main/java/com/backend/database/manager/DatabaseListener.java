package com.backend.database.manager;

import com.backend.pojos.Player;
import com.backend.pojos.Team;

public interface DatabaseListener {

	public void onSaveOrUpdate(Object entityObject);

	public void onUpdatePlayer(Player playerData, Integer playerID);

	public void onInsertEntity(Object entity);

	public void onDeleteEntity(Object object);

	public void onDeleteEntity(Class<?> clazz, int entityId);

	public void onGetAllEntities();

	public <T> void onGetEntity(Class<T> clazz, int entityId);

	public void onUpdateTeam(Team teamData, Integer teamID);
}
