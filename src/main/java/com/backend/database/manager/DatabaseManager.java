package com.backend.database.manager;

import java.util.ArrayList;
import java.util.List;

import com.backend.pojos.Player;
import com.backend.pojos.Team;

public class DatabaseManager {
	List<DatabaseListener> databaseListenerList;

	public DatabaseManager() {
		this.databaseListenerList = new ArrayList<DatabaseListener>();
	}

	public void addDatabaseListener(DatabaseListener databaseListener) {
		databaseListenerList.add(databaseListener);
	}

	public void removeDatabaseListener(DatabaseListener databaseListener) {
		databaseListenerList.remove(databaseListener);
	}

	public void onSaveOrUpdate(Object entityObject) {
		for (DatabaseListener databaseListener : databaseListenerList) {
			databaseListener.onSaveOrUpdate(entityObject);
		}
	}

	public void onUpdatePlayer(Player playerData, Integer playerID) {
		for (DatabaseListener databaseListener : databaseListenerList) {
			databaseListener.onUpdatePlayer(playerData, playerID);
		}
	}

	public void onInsertEntity(Object entity) {
		for (DatabaseListener databaseListener : databaseListenerList) {
			databaseListener.onInsertEntity(entity);
		}
	}

	public void onDeleteEntity(Object object) {
		for (DatabaseListener databaseListener : databaseListenerList) {
			databaseListener.onDeleteEntity(object);
		}
	}

	public void onDeleteEntity(Class<?> clazz, int entityId) {
		for (DatabaseListener databaseListener : databaseListenerList) {
			databaseListener.onDeleteEntity(clazz, entityId);
		}
	}

	public void onGetAllEntities() {
		for (DatabaseListener databaseListener : databaseListenerList) {
			databaseListener.onGetAllEntities();
		}
	}

	public <T> void onGetEntity(Class<T> clazz, int entityId) {
		for (DatabaseListener databaseListener : databaseListenerList) {
			databaseListener.onGetEntity(clazz, entityId);
		}
	}

	public void onUpdatePlayer(Team teamData, Integer teamID) {
		for (DatabaseListener databaseListener : databaseListenerList) {
			databaseListener.onUpdateTeam(teamData, teamID);
		}
	}

}
