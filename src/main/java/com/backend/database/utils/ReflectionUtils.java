package com.backend.database.utils;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.backend.jsonmapper.JSONProperties;
import com.gui.dialogs.ExceptionDialog;

public class ReflectionUtils {
	final static Logger logger = Logger.getLogger(ReflectionUtils.class);

	@SuppressWarnings("rawtypes")
	public static Set<Class> getAllClasses(String pckgname) {
		try {
			Set<Class> classes = new HashSet<Class>();
			File directory = null;
			try {
				directory = new File(Thread.currentThread().getContextClassLoader().getResource(pckgname.replace('.', '/')).getFile());
			} catch (NullPointerException e) {
				logger.error("Get Nullpointer " + e.getMessage());
				if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
					ExceptionDialog.showException(e);
				}
				throw new ClassNotFoundException(pckgname + " does not appear to be a valid package");
			}
			if (directory.exists()) {
				String[] files = directory.list();
				for (int i = 0; i < files.length; i++) {
					if (files[i].endsWith(".class")) {
						classes.add(Class.forName(pckgname + '.' + files[i].substring(0, files[i].length() - 6)));
					}
				}
			} else {
				logger.debug("Package does not exist");
				throw new ClassNotFoundException(pckgname + " does not appear to be a valid package");
			}

			return classes;

		} catch (Exception e) {
			e.printStackTrace();
			if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
				ExceptionDialog.showException(e);
			}
		}
		return null;
	}
}
