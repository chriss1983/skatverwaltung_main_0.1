package com.backend.syntetica;

import java.security.MessageDigest;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.backend.jsonmapper.JSONProperties;
import com.gui.dialogs.ExceptionDialog;

public class SyntheticaLicense {

	public static void main(String[] args) {
		try {
			String owner = "Christian Richter";
			String product = "Synthetica";
			System.out.println("Generating license for Product " + product + " owned to user " + owner);
			String licenseKey = generateLicense(owner, product);
			System.out.println("License generated: " + licenseKey);
		} catch (Exception e) {
			System.out.println("An error has occurred while generating your license key");
			if (JSONProperties.getJSonProperty().getDebugging().isShowErrorMessages()) {
				ExceptionDialog.showException(e);
			}
		}
	}

	private static String generateLicense(String owner, String product) throws Exception {
		String[] licenseInformation = { "Licensee=" + owner, "LicenseRegistrationNumber=------", "Product=" + product, "LicenseType=Non Commercial", "ExpireDate=--.--.----", "MaxVersion=999.999.999" };

		TreeMap<String, String> licenseTreeMap = new TreeMap<String, String>();
		for (String licenceEntry : licenseInformation) {
			String[] licenceEntryValues = licenceEntry.split("=");
			licenseTreeMap.put(licenceEntryValues[0], licenceEntryValues[1]);
		}

		return GenerateKey(licenseTreeMap);
	}

	private static String GenerateKey(TreeMap<String, String> licenseTreeMap) throws Exception {
		StringBuilder licenseInformationString = new StringBuilder();
		for (Iterator<Entry<String, String>> licenceEntryValues = licenseTreeMap.entrySet().iterator(); licenceEntryValues.hasNext();) {
			Entry<String, String> licenseEntry = licenceEntryValues.next();
			licenseInformationString.append(licenseEntry).append("\n");
		}
		MessageDigest cryptor = MessageDigest.getInstance("SHA-1");
		cryptor.reset();
		cryptor.update(licenseInformationString.toString().getBytes("UTF-8"));
		byte[] encryptedBytes = cryptor.digest();
		StringBuilder keyString = new StringBuilder(encryptedBytes.length * 2);

		for (int i = 0; i < encryptedBytes.length; i++) {
			int j = encryptedBytes[i] & 0xFF;
			if (j < 16) {
				keyString.append('0');
			}
			keyString.append(Integer.toHexString(j));
			if ((i % 4 == 3) && (i < encryptedBytes.length - 1)) {
				keyString.append("-");
			}
		}
		return keyString.toString().toUpperCase();
	}
}