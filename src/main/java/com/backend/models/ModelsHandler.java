package com.backend.models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import com.backend.controller.contextmenu.AddPlayerToGroupController;
import com.backend.controller.table.TableController;
import com.backend.database.RedeyedCoreManager;
import com.backend.pojos.Player;
import com.backend.pojos.TableModelHandler;
import com.backend.pojos.Team;
import com.gui.dialogs.InfoDialog;
import com.gui.view.splitpane.tables.AbstractListPanel;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

public class ModelsHandler {

	private final static Logger		logger					= Logger.getLogger(ModelsHandler.class);

	private static ModelsHandler	modelsHandler			= new ModelsHandler();

	// Player
	private ObservableList<Player>	playerList;

	// Team
	private ObservableList<Team>	teamList;

	// SelectedPlayer in Table
	private List<Player>			multibleSelectedPlayer	= new ArrayList<>();

	// SelectedTeam in Table
	private List<Team>				multibleSelectedTeam	= new ArrayList<>();

	private List<Team>				toDeleteList			= new ArrayList<>();

	// Constructor
	private ModelsHandler() {
		List<Player> players = RedeyedCoreManager.getDataBaseUtil().getAllEntities(Player.class, null, null, null, null);

		if (players != null) {
			for (Player player : players) {
				TableModelHandler.getInstance().addRowOnlyToPlayerTable(player);
			}

			playerList = FXCollections.observableList(players);
		}

		List<Team> teams = RedeyedCoreManager.getDataBaseUtil().getAllEntities(Team.class, null, null, null, null);

		if (teams != null) {
			for (Team team : teams) {
				TableModelHandler.getInstance().addRowOnlyToTeamTable(team);
			}

			teamList = FXCollections.observableList(teams);
		}
	}

	// Getter and Setter for Player
	public List<Player> getPlayers() {
		return this.playerList;
	}

	public void addPlayer(Player player) {
		RedeyedCoreManager.getDataBaseUtil().save(player);
		playerList.add(player);
		TableModelHandler.getInstance().addRowOnlyToPlayerTable(player);
	}

	public void updatePlayer(Player player) {
		Player newPlayer = null;
		Player toRemove = null;
		for (Player origPlayer : playerList) {
			if (origPlayer.getId() == player.getId()) {
				toRemove = origPlayer;
				newPlayer = new Player();

				newPlayer.setId(player.getId());
				newPlayer.setFirstName(player.getFirstName());
				newPlayer.setLastName(player.getLastName());
				newPlayer.setStreet(player.getStreet());
				newPlayer.setHouseNumber(player.getHouseNumber());
				newPlayer.setZipCode(player.getZipCode());
				newPlayer.setCity(player.getCity());
				newPlayer.setMobilePhoneNumber(player.getMobilePhoneNumber());
				newPlayer.setPhone(player.getPhone());
				newPlayer.setEMail(player.getEMail());
				newPlayer.setPaid(player.isPaid());
				newPlayer.setPlayerInfo(player.getPlayerInfo());

				System.out.println("SELECTED ROW = " + TableModelHandler.getInstance().getSelectedRowCount());
				break;
			}
		}

		String teamName = "";
		Team team = getPlayerTeamWhenExists(player);
		if (team != null) {
			teamName = team.getTeamName();
		}

		multibleSelectedTeam.add(team);
		removePlayerFromTeam(toRemove);

		if (!teamName.equals("") && newPlayer != null) {
			removeTeam(team);
			playerList.remove(toRemove);
		}

		toDeleteList.add(team);
		playerList.add(newPlayer);
	}

	public void addPlayerList(List<Player> players) {
		for (Player player : players) {
			logger.debug("Add Player with ID/NAME " + player.getId() + " " + player.getFirstName() + " " + player.getLastName() + " to playerList");
			playerList.add(player);
		}
	}

	public void removePlayer(Player player) {
		logger.debug("remove Player with ID/NAME " + player.getId() + " " + player.getFirstName() + " " + player.getLastName() + " from playerList");
		playerList.remove(player);
	}

	public Team getTeam(String teamName) {
		for (Team tmpList : teamList) {
			logger.debug("Search for " + teamName + " in " + tmpList.getTeamName());
			if (tmpList.getTeamName().equals(teamName)) {
				logger.debug("found " + teamName + " in Team " + tmpList.getTeamName() + " with ID " + tmpList.getId());
				return tmpList;
			}
		}
		return null;
	}

	public List<Team> getTeamList() {
		return this.teamList;
	}

	public void addTeam(Team team) {
		if (team.getTeamName().equals("")) {
			throw new IllegalArgumentException("teamname cannot empty");
		}
		logger.debug("Add Team with ID/NAME " + team.getId() + " " + team.getTeamName() + " to teamList");
		this.teamList.add(team);
		createAvaibleTeamContextMenu(team);
		// TableController.getInstance().onAddRowToTeam(team);
	}

	private void createAvaibleTeamContextMenu(final Team team) {
		boolean exists = false;
		for (int index = 0; index < AbstractListPanel.toTeamSub.getItemCount() && !exists; index++) {
			if (team.getTeamName().equals(AbstractListPanel.toTeamSub.getItem(index).getText())) {
				exists = true;
			}
		}

		if (!exists) {
			final JMenuItem tmpItem = new JMenuItem(team.getTeamName());
			logger.debug("Add Team " + team.getTeamName() + " to context menu");

			tmpItem.addActionListener(new AddPlayerToGroupController(tmpItem));

			AbstractListPanel.toTeamSub.add(tmpItem);
			AbstractListPanel.toTeamSub.repaint();
			AbstractListPanel.toTeamSub.revalidate();
		} else {
			logger.debug("Team with ID '" + team.getId() + "' already exists in Contextmenu");
		}
	}

	public void addTeamList(List<Team> teams) {
		for (Team team : teams) {
			logger.debug("Add Team with ID/NAME " + team.getId() + " " + team.getTeamName() + " to teamList");
			this.teamList.add(team);
		}
	}

	public void addPlayersToTeam(List<Player> players, Team team) {
		TableController.getInstance().onAddPlayersToTeam(players, team);
	}

	public boolean removePlayerFromTeam(Player player) {
		Player selectedPlayer = null;
		Team selectedTeam = null;

		if (!multibleSelectedPlayer.isEmpty() && multibleSelectedPlayer.get(0) != null) {
			for (Team team : teamList) {

				System.out.println(this.getClass().getSimpleName() + " delete Team " + team);
				for (Player p : team.getPlayers())
					if (p.getId() == multibleSelectedPlayer.get(0).getId()) {
						selectedPlayer = p;
						selectedTeam = team;
					}
				break;
			}
			if (selectedPlayer != null) {
				if (removeTeam(selectedTeam)) {
					logger.debug("Add Player with ID/NAME " + player.getId() + " " + player.getFirstName() + " " + player.getLastName() + " to Team " + selectedTeam.getId() + " " + selectedTeam.getTeamName());
					selectedTeam.getPlayers().remove(selectedPlayer);
					return true;
				} else {
					return false;
				}

			} else {
				InfoDialog.showInfoDialog("Es ist ein Fehler beim entfernen des Players aufgetretten", JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}
		return false;
	}

	public boolean removeTeam(Team team) {
		Team teamToRemove = null;
		for (Team tmpTeam : teamList) {
			if (tmpTeam.getId() == team.getId()) {
				teamToRemove = team;
				break;
			}
		}
		if (teamToRemove != null && teamList.remove(teamToRemove)) {
			logger.debug("Remove Team with ID/NAME " + teamToRemove.getId() + " " + teamToRemove.getTeamName());
			// TableController.getInstance().onRemoveTeamRow(teamToRemove);
			return true;
		} else {
			return false;
		}
	}

	public Team getPlayerTeamWhenExists(Player player) {
		for (Team team : teamList) {
			for (Player playerTmp : team.getPlayers()) {
				if (playerTmp.getId() == player.getId()) {
					logger.debug("Found Player with ID/NAME " + playerTmp.getId() + " " + playerTmp.getFirstName() + " " + playerTmp.getLastName() + " in Team " + team.getId() + " " + team.getTeamName());
					return team;
				}
			}
		}
		return null;
	}

	public boolean isPlayerInTeam(Player player) {
		for (Team team : teamList) {
			for (Player playerTmp : team.getPlayers()) {
				if (playerTmp.getId() == player.getId()) {
					logger.debug("Found Player with ID/NAME " + playerTmp.getId() + " " + playerTmp.getFirstName() + " " + playerTmp.getLastName() + " in Team " + team.getId() + " " + team.getTeamName());
					return true;
				}
			}
		}
		return false;
	}

	public boolean isTeamExistsInList(Team team) {

		for (Team teamInList : teamList) {
			if (teamInList.getId() == team.getId()) {
				return true;
			}
		}
		return false;
	}

	// public boolean isPlayerExistsInList(Player player) {
	// boolean isPlayerExists = false;
	// for (Player playerTmp : playerList) {
	// if (player.getId() == playerTmp.getId()) {
	// isPlayerExists = true;
	// }
	// }
	// return isPlayerExists;
	// }

	// Setter for Listener
	public void addMapChangeListenerToTeam(ListChangeListener<Team> teamListChangeListener) {
		this.teamList.addListener(teamListChangeListener);
	}

	public void addMapChangeListenerToPlayer(ListChangeListener<Player> playerListChangeListener) {
		this.playerList.addListener(playerListChangeListener);
	}

	// Getter and setter
	public static ModelsHandler getInstance() {
		return modelsHandler;
	}

	public void addSelectedPlayer(Player player) {
		this.multibleSelectedPlayer.add(player);
	}

	public List<Player> getSelectedPlayerList() {
		return this.multibleSelectedPlayer;
	}

	public void cleanSelectedPlayer() {
		multibleSelectedPlayer.clear();
	}

	public void addSelectedTeam(Team team) {
		this.multibleSelectedTeam.add(team);
	}

	public List<Team> getSelectedTeams() {
		return multibleSelectedTeam;
	}

	public void cleanSelectedTeam() {
		multibleSelectedTeam.clear();
	}

	public List<Team> getToDeleteTeams() {
		return toDeleteList;
	}
}
