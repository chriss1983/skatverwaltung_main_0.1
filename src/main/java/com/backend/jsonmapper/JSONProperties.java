package com.backend.jsonmapper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JSONProperties {
	final static Logger				logger	= Logger.getLogger(JSONProperties.class);
	private Database				database;
	private Debugging				debugging;
	private Security				security;
	private Global					global;
	private Table					table;
	private Team					team;

	private static JSONProperties	model;

	private static Gson				GSON	= new GsonBuilder().create();

	public static JSONProperties getJSonProperty() {
		if (model == null) {
			model = new JSONProperties();

			ClassLoader classLoader = new JSONProperties().getClass().getClassLoader();
			File file = new File(classLoader.getResource("properties.json").getFile());

			logger.debug("File Found : " + file.exists());

			String content = null;
			try {
				content = new String(Files.readAllBytes(file.toPath()));
			} catch (IOException e) {
				e.printStackTrace();
			}
			model = GSON.fromJson(content, JSONProperties.class);
		}
		return model;
	}

	public static void saveProperties() throws JsonProcessingException, IOException {
		logger.debug("Saved in JSON File");
		JsonFactory jsonFactory;
		FileOutputStream file;
		JsonGenerator jsonGen;

		jsonFactory = new JsonFactory();
		file = new FileOutputStream(new File("resources/properties.json"));
		jsonGen = jsonFactory.createJsonGenerator(file, JsonEncoding.UTF8);
		jsonGen.setCodec(new ObjectMapper());
		jsonGen.writeObject(JSONProperties.getJSonProperty());
	}
	
	public static void main(String[] args) {
		
		//map.forEach( (k,v) -> System.out.println("Key: " + k + ": Value: " + v));
		
		Map<String, Boolean> selects = JSONProperties.getJSonProperty().getTable().getPlayerHeader();

		for(Map.Entry<String, Boolean> entry : selects.entrySet()) {
		    String key = entry.getKey();
		    Boolean value = entry.getValue();
		    
		    System.out.println("Key = " + key + " : value = " + value);
		}
	}

	private JSONProperties() {
		this.database = new Database();
		this.debugging = new Debugging();
		this.security = new Security();
		this.global = new Global();
		this.table = new Table();
		this.team = new Team();
	}

	public Database getDatabase() {
		return database;
	}

	public void setDatabase(Database database) {
		this.database = database;
	}

	public Debugging getDebugging() {
		return debugging;
	}

	public void setDebugging(Debugging debugging) {
		this.debugging = debugging;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public Global getGlobal() {
		return global;
	}

	public void setGlobal(Global global) {
		this.global = global;
	}

	public Table getTable() {
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	@Override
	public String toString() {
		return "JSONProperties: \n DATABASE_CLASS=" + database.toString() + ",\n DEBUGGING_CLASS=" + debugging.toString() + ",\n SECURITY_CLASS=" + security.toString() + ",\n GLOBAL_CLASS=" + global.toString();
	}

	public class Database {
		private String	username;
		private String	password;
		private String	databaseURL;
		private String	dialect;
		private String	driver;

		public Database() {
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String userName) {
			this.username = userName;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getDatabaseURL() {
			return databaseURL;
		}

		public void setDatabaseURL(String databaseURL) {
			this.databaseURL = databaseURL;
		}

		public String getDialect() {
			return dialect;
		}

		public void setDialect(String dialect) {
			this.dialect = dialect;
		}

		public String getDriver() {
			return driver;
		}

		public void setDriver(String driver) {
			this.driver = driver;
		}

		@Override
		public String toString() {
			return "Database [username=" + username + ", password=" + password + ", databaseURL=" + databaseURL + ", dialect=" + dialect + ", driver=" + driver + "]";
		}
	}

	public class Debugging {
		private Boolean showErrorMessages;

		public Debugging() {
			// TODO Auto-generated constructor stub
		}

		public Boolean isShowErrorMessages() {
			return showErrorMessages;
		}

		public void setShowErrorMessages(Boolean showErrorMessages) {
			this.showErrorMessages = showErrorMessages;
		}

		@Override
		public String toString() {
			return "Debugging [showErrorMessages=" + showErrorMessages + "]";
		}
	}

	public class Security {

		private Boolean	activationEnable;
		private String	publicKey;
		private String	activationState;
		private String	activationKey;
		private String	secureKey;

		public Boolean getActivationEnable() {
			return activationEnable;
		}

		public void setActivationEnable(Boolean activationEnable) {
			this.activationEnable = activationEnable;
		}

		public String getPublicKey() {
			return publicKey;
		}

		public void setPublicKey(String publicKey) {
			this.publicKey = publicKey;
		}

		public String getActivationState() {
			return activationState;
		}

		public void setActivationState(String activationState) {
			this.activationState = activationState;
		}

		public String getActivationKey() {
			return activationKey;
		}

		public void setActivationKey(String activationKey) {
			this.activationKey = activationKey;
		}

		public String getSecureKey() {
			return secureKey;
		}

		public void setSecureKey(String secureKey) {
			this.secureKey = secureKey;
		}

		@Override
		public String toString() {
			return "Security [activationEnable=" + activationEnable + ", publicKey=" + publicKey + ", activationState=" + activationState + ", activationKey=" + activationKey + ", secureKey=" + secureKey + "]";
		}
	}

	public class Global {

		private String	appName;
		private String	version;
		private String	width;
		private String	height;
		private Boolean	fullScreen;
		private String	theme;
		private String	lang;

		public String getAppName() {
			return appName;
		}

		public void setAppName(String appName) {
			this.appName = appName;
		}

		public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}

		public String getWidth() {
			return width;
		}

		public void setWidth(String width) {
			this.width = width;
		}

		public String getHeight() {
			return height;
		}

		public void setHeight(String height) {
			this.height = height;
		}

		public Boolean getFullScreen() {
			return fullScreen;
		}

		public void setFullScreen(Boolean fullScreen) {
			this.fullScreen = fullScreen;
		}

		public String getTheme() {
			return theme;
		}

		public void setTheme(String theme) {
			this.theme = theme;
		}

		public String getLang() {
			return lang;
		}

		public void setLang(String lang) {
			this.lang = lang;
		}

		@Override
		public String toString() {
			return "Global [appName=" + appName + ", version=" + version + ", width=" + width + ", height=" + height + ", fullScreen=" + fullScreen + ", theme=" + theme + "]";
		}
	}

	public class Table {
		private Boolean enableColumnMenu;
		
		Map<String, Boolean> playerHeader;
		
		private Boolean id;
		private Boolean firstname;
		private Boolean lastname;
		private Boolean street;
		private Boolean housenr;
		private Boolean zipcode;
		private Boolean city;
		private Boolean paid;
		private Boolean email;
		private Boolean mobile;
		private Boolean phone;
		private Boolean seriesOne;
		private Boolean seriesTwo;
		private Boolean total;
		private Boolean playerInfo;

		public Map<String, Boolean> getPlayerHeader() {
			return playerHeader;
		}
		
		public Boolean getEnableColumnMenu() {
			return enableColumnMenu;
		}

		public void setEnableColumnMenu(Boolean enableColumnMenu) {
			this.enableColumnMenu = enableColumnMenu;
		}

		public Boolean getId() {
			return id;
		}

		public void setId(Boolean id) {
			this.id = id;
		}

		public Boolean isFirstname() {
			return firstname;
		}

		public void setFirstname(Boolean firstname) {
			this.firstname = firstname;
		}

		public Boolean isLastname() {
			return lastname;
		}

		public void setLastname(Boolean lastname) {
			this.lastname = lastname;
		}

		public Boolean isStreet() {
			return street;
		}

		public void setStreet(Boolean street) {
			this.street = street;
		}

		public Boolean isHousenr() {
			return housenr;
		}

		public void setHousenr(Boolean housenr) {
			this.housenr = housenr;
		}

		public Boolean isZipcode() {
			return zipcode;
		}

		public void setZipcode(Boolean zipcode) {
			this.zipcode = zipcode;
		}

		public Boolean isCity() {
			return city;
		}

		public void setCity(Boolean city) {
			this.city = city;
		}

		public Boolean isPaid() {
			return paid;
		}

		public void setPaid(Boolean paid) {
			this.paid = paid;
		}

		public Boolean isEmail() {
			return email;
		}

		public void setEmail(Boolean email) {
			this.email = email;
		}

		public Boolean isMobile() {
			return mobile;
		}

		public void setMobile(Boolean mobile) {
			this.mobile = mobile;
		}

		public Boolean isPhone() {
			return phone;
		}

		public void setPhone(Boolean phone) {
			this.phone = phone;
		}

		public Boolean isSeriesOne() {
			return seriesOne;
		}

		public void setSeriesOne(Boolean seriesOne) {
			this.seriesOne = seriesOne;
		}

		public Boolean isSeriesTwo() {
			return seriesTwo;
		}

		public void setSeriesTwo(Boolean seriesTwo) {
			this.seriesTwo = seriesTwo;
		}

		public Boolean isTotal() {
			return total;
		}

		public void setTotal(Boolean total) {
			this.total = total;
		}

		public Boolean PlayerInfo() {
			return playerInfo;
		}

		public void setPlayerInfo(Boolean playerInfo) {
			this.playerInfo = playerInfo;
		}
	}

	public class Team {
		int maxPlayerCount;

		public int getMaxPlayerCount() {
			return maxPlayerCount;
		}

		public void setMaxPlayerCount(int maxPlayerCount) {
			this.maxPlayerCount = maxPlayerCount;
		}

	}
}
