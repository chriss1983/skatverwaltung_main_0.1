package com.backend.jsonmapper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JSONLanguageManager {
	private Table						table;
	private Menubar						menubar;
	private Activator					activator;
	private Context						context;
	private StringValues				stringValues;

	private final static Logger			logger		= Logger.getLogger(JSONLanguageManager.class);
	private static JSONLanguageManager	instance	= null;

	private static Gson					GSON		= new GsonBuilder().create();

	public JSONLanguageManager() {
		table = new Table();
		menubar = new Menubar();
		activator = new Activator();
		context = new Context();
		stringValues = new StringValues();
	}

	public static JSONLanguageManager getLanguageProperty() {
		logger.debug("getLanguageProperty");
		if (instance == null) {
			instance = new JSONLanguageManager();

			ClassLoader classLoader = new JSONLanguageManager().getClass().getClassLoader();
			File file = new File(classLoader.getResource("lang/" + Locale.getDefault().getLanguage() + ".json").getFile());

			System.out.println("File Found : " + file.exists());

			String content = null;
			try {
				content = new String(Files.readAllBytes(file.toPath()));
			} catch (IOException e) {
				e.printStackTrace();
			}
			instance = GSON.fromJson(content, JSONLanguageManager.class);
		}
		return instance;
	}

	public static void saveProperties() throws JsonProcessingException, IOException {
		JsonFactory jsonFactory;
		FileOutputStream file;
		JsonGenerator jsonGen;

		jsonFactory = new JsonFactory();
		file = new FileOutputStream(new File("resources/lang/" + Locale.getDefault().getLanguage() + ".json"));
		jsonGen = jsonFactory.createJsonGenerator(file, JsonEncoding.UTF8);
		jsonGen.setCodec(new ObjectMapper());
		jsonGen.writeObject(JSONProperties.getJSonProperty());
	}

	public Table getTable() {
		return table;
	}

	public Menubar getMenubar() {
		return menubar;
	}

	public Activator getActivator() {
		return activator;
	}

	public Context getContext() {
		return context;
	}

	public StringValues getStringValues() {
		return stringValues;
	}

	/**
	 * * @param query
	 * - select statement
	 * 
	 * @return List of Map objects
	 *         Here is a Smal example how to use it:
	 *         <blockquote>
	 * 
	 *         <pre>
	 *         {
	 *         	&#64;code
	 *         	String template = "Hi ${name}! Your number is ${number}";
	 *
	 *         	Map<String, String> data = new HashMap<String, String>();
	 *         	data.put("name", "John");
	 *         	data.put("number", "1");
	 *         	String formattedString = StrSubstitutor.replace(template, data);
	 *         }
	 *         </pre>
	 * 
	 *         </blockquote>
	 */
	public void setStringValues(StringValues stringValues) {
		this.stringValues = stringValues;
	}

	public static void main(String[] args) {
		System.out.println(JSONLanguageManager.getLanguageProperty().getStringValues().getValues().get("firstName"));
	}

	public class Menubar {
		private String	fileMenu;
		private String	fileNew;
		private String	addUser;
		private String	addGroup;
		private String	save;
		private String	settings;
		private String	exit;

		private String	editMenu;
		private String	cut;
		private String	copy;
		private String	paste;

		private String	helpMenu;
		private String	help;
		private String	about;

		public String getFileMenu() {
			return fileMenu;
		}

		public String getFileNew() {
			return fileNew;
		}

		public String getAddUser() {
			return addUser;
		}

		public String getAddGroup() {
			return addGroup;
		}

		public String getSave() {
			return save;
		}

		public String getSettings() {
			return settings;
		}

		public String getExit() {
			return exit;
		}

		public String getEditMenu() {
			return editMenu;
		}

		public String getCut() {
			return cut;
		}

		public String getCopy() {
			return copy;
		}

		public String getPaste() {
			return paste;
		}

		public String getHelpMenu() {
			return helpMenu;
		}

		public String getHelp() {
			return help;
		}

		public String getAbout() {
			return about;
		}

	}

	public class Activator {
		private String	infotext;
		private String	buttonSave;

		public String getInfotext() {
			return infotext;
		}

		public String getButtonSave() {
			return buttonSave;
		}

	}

	public class Table {
		private Map<String, String>	playerHeaderLabels;
		private Map<String, String>	teamHeaderLabels;

		public Map<String, String> getPlayerHeaderLabels() {
			return playerHeaderLabels;
		}

		public Map<String, String> getTeamHeaderLabels() {
			return teamHeaderLabels;
		}

		public List<String> getPlayerHeadersNamesAsList() {
			return new ArrayList<String>(playerHeaderLabels.values());
		}

		public List<String> getTeamHeadersNamesAsList() {
			return new ArrayList<String>(teamHeaderLabels.values());
		}
	}

	public class StringValues {
		private Map<String, String> values;

		public Map<String, String> getValues() {
			return values;
		}
	}

	public class Context {
		private String	contextmenu;
		private String	addplayer;
		private String	addplayerto;
		private String	editplayer;
		private String	removefromteam;
		private String	deleteplayer;
		private String	toteam;
		private String	newteam;

		public String getContextmenu() {
			return contextmenu;
		}

		public void setContextmenu(String contextmenu) {
			this.contextmenu = contextmenu;
		}

		public String getAddplayer() {
			return addplayer;
		}

		public void setAddplayer(String addplayer) {
			this.addplayer = addplayer;
		}

		public String getAddplayerto() {
			return addplayerto;
		}

		public void setAddplayerto(String addplayerto) {
			this.addplayerto = addplayerto;
		}

		public String getEditplayer() {
			return editplayer;
		}

		public void setEditplayer(String editplayer) {
			this.editplayer = editplayer;
		}

		public String getRemovefromteam() {
			return removefromteam;
		}

		public void setRemovefromteam(String removefromteam) {
			this.removefromteam = removefromteam;
		}

		public String getDeleteplayer() {
			return deleteplayer;
		}

		public void setDeleteplayer(String deleteplayer) {
			this.deleteplayer = deleteplayer;
		}

		public String getToteam() {
			return toteam;
		}

		public void setToteam(String toteam) {
			this.toteam = toteam;
		}

		public String getNewteam() {
			return newteam;
		}

		public void setNewteam(String newteam) {
			this.newteam = newteam;
		}
	}
}